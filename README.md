# TokoPhincon
![](https://i.imgur.com/I60UJuJ.png)

[![Android Studio](https://img.shields.io/badge/Android%20Studio%20Flamingo-2022.2.1%20Patch2-green.svg?)](https://developer.android.com/studio/preview)

[![Kotlin Version](https://img.shields.io/badge/kotlin-1.8.10-blue.svg?)](http://kotlinlang.org/)
[![Gradle](https://img.shields.io/badge/gradle-8.0.0-blue.svg?)](https://developer.android.com/build/releases/gradle-plugin)
[![API](https://img.shields.io/badge/support%20min%20API-24%20[Android%207.0]-blue.svg?)](https://github.com/AndroidSDKSources/android-sdk-sources-list)
[![Target SDK](https://img.shields.io/badge/Target%20API%20Level-33%20[Android%2013]-blue.svg?)](https://developer.android.com/about/versions/13)

![GitHub last commit](https://img.shields.io/gitlab/last-commit/fortoszone/ecommerce?color=black)

## Tech Stack
- Kotlin, [Coroutine](https://github.com/Kotlin/kotlinx.coroutines) with [LiveData](https://developer.android.com/topic/libraries/architecture/livedata) and [Flow](https://developer.android.com/kotlin/flow)
- Jetpack
    - [Lifecycle](https://developer.android.com/jetpack/androidx/releases/lifecycle): Build lifecycle-aware components that can adjust behavior based on the current lifecycle state of an activity or fragment.
    - [ViewModel](https://developer.android.com/topic/libraries/architecture/viewmodel): Business logic or screen level state holder.
    - [ViewBinding](https://developer.android.com/topic/libraries/view-binding)
    - [Hilt](https://developer.android.com/training/dependency-injection/hilt-android): Dependency Injection
    - [Room](https://developer.android.com/training/data-storage/room): Create, store, and manage persistent data backed by a SQLite database.
    - [Fragment](https://developer.android.com/jetpack/androidx/releases/fragment): Segment your app into multiple, independent screens that are hosted within an Activity.
    - [Compose](https://developer.android.com/jetpack/androidx/releases/compose): Define your UI programmatically with composable functions that describe its shape and data dependencies. Build with [Material 3](https://developer.android.com/jetpack/androidx/releases/compose-material3)
- Architecture
    - MVVM (Model - View - ViewModel)
- [Glide](https://github.com/bumptech/glide): Image loading from network
- [SharedPreference](https://developer.android.com/training/data-storage/shared-preferences): Storing simple data

- [Retrofit](https://github.com/square/retrofit) and [OkHttp](https://square.github.io/okhttp/): REST API Client
- [Gson](https://github.com/google/gson): JSON Library for Kotlin
- [Chucker](https://github.com/ChuckerTeam/chucker): HTTP request and response inspector

- Firebase
    - [Crashlytic](https://firebase.google.com/products/crashlytics): Realtime crash reporting tool
    - [Analytics](https://firebase.google.com/docs/analytics): Measure application-level event
    - [Remote Config](https://firebase.google.com/docs/remote-config): Change app behavior and appearance without an app update
    - [Cloud Messaging](https://firebase.google.com/docs/cloud-messaging): Send app notification to user
    
- [Detekt](https://github.com/detekt/detekt): Static code analysis for Kotlin
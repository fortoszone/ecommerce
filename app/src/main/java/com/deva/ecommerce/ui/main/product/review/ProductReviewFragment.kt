package com.deva.ecommerce.ui.main.product.review

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Star
import androidx.compose.material.icons.outlined.Star
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.Divider
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.material3.darkColorScheme
import androidx.compose.material3.lightColorScheme
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.unit.dp
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.hilt.navigation.compose.hiltViewModel
import com.bumptech.glide.integration.compose.ExperimentalGlideComposeApi
import com.bumptech.glide.integration.compose.GlideImage
import com.deva.ecommerce.R
import com.deva.ecommerce.databinding.FragmentProductReviewBinding
import com.deva.ecommerce.model.remote.response.ProductReviewData
import com.deva.ecommerce.model.remote.response.ProductReviewResponse
import com.deva.ecommerce.ui.theme.EcommerceTheme
import com.deva.ecommerce.ui.theme.ErrorLayout
import dagger.hilt.android.AndroidEntryPoint
import kotlin.math.ceil
import kotlin.math.floor

@AndroidEntryPoint
class ProductReviewFragment : Fragment() {
    private var _binding: FragmentProductReviewBinding? = null
    private val viewModel: ProductReviewViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentProductReviewBinding.inflate(inflater, container, false)

        val id = arguments?.getString("id") ?: ""
        viewModel.getProductReview(id)

        return ComposeView(requireContext()).apply {
            setContent {
                val productReviewState by viewModel.productReviewState.observeAsState()

                EcommerceTheme {
                    productReviewState.let {
                        when (it) {
                            is ProductReviewState.Loading -> {
                                Box(
                                    contentAlignment = Alignment.Center
                                ) {
                                    CircularProgressIndicator()
                                }
                            }

                            is ProductReviewState.Success -> {
                                SetReviewList(
                                    data = it.data,
                                    viewModel = viewModel
                                )
                            }

                            is ProductReviewState.Error -> {
                                ErrorLayout(onRefreshClick = {
                                    viewModel.getProductReview(id)
                                })
                            }

                            else -> {}
                        }
                    }
                }
            }
        }
    }
}

@OptIn(ExperimentalGlideComposeApi::class)
@Composable
fun ReviewItem(review: ProductReviewData) {
    Column(
        modifier = Modifier
            .fillMaxWidth()
            .padding(vertical = 16.dp)
            .padding(horizontal = 16.dp)
    ) {
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .padding(bottom = 6.dp),
            verticalAlignment = Alignment.CenterVertically
        ) {
            GlideImage(
                model = review.userImage,
                contentDescription = null,
                modifier = Modifier
                    .size(40.dp)
                    .clip(
                        CircleShape
                    )
            ) {
                it.load(review.userImage).error(R.drawable.thumbnail)
            }

            Column(
                modifier = Modifier
                    .weight(1f)
                    .padding(start = 8.dp)
            ) {
                Text(
                    text = review.userName,
                    style = MaterialTheme.typography.titleMedium,
                    color = if (isSystemInDarkTheme()) darkColorScheme().onBackground else lightColorScheme().onBackground,
                )

                RatingBar(
                    rating = review.userRating.toDouble(),
                )
            }
        }

        Text(
            text = review.userReview,
            style = MaterialTheme.typography.bodyLarge,
            color = if (isSystemInDarkTheme()) darkColorScheme().onBackground else lightColorScheme().onBackground,
        )
    }

    Divider(
        color = Color.LightGray,
        thickness = 1.dp,
        modifier = Modifier.fillMaxWidth()
    )
}

@Composable
fun SetReviewList(
    data: ProductReviewResponse,
    viewModel: ProductReviewViewModel = hiltViewModel()
) {
    val isLoading by viewModel.isLoading.observeAsState()

    if (isLoading == true) {
        Box(modifier = Modifier.fillMaxWidth(), contentAlignment = Alignment.Center) {
            CircularProgressIndicator()
        }
    } else {
        LazyColumn(
            modifier = Modifier
                .fillMaxSize()
                .padding(top = 64.dp)
        ) {
            items(data.data) { review ->
                ReviewItem(review)
            }
        }
    }
}

@Composable
fun RatingBar(
    rating: Double,
    stars: Int = 5,
    starsColor: Color = if (isSystemInDarkTheme()) darkColorScheme().onBackground else lightColorScheme().onBackground,
) {
    val filledStars = floor(rating).toInt()
    val unfilledStars = (stars - ceil(rating)).toInt()
    Row {
        repeat(filledStars) {
            Icon(
                imageVector = Icons.Default.Star,
                contentDescription = null,
                tint = starsColor,
                modifier = Modifier.size(12.dp)
            )
        }
        repeat(unfilledStars) {
            Icon(
                imageVector = Icons.Outlined.Star,
                contentDescription = null,
                tint = starsColor,
                modifier = Modifier.size(12.dp)
            )
        }
    }
}

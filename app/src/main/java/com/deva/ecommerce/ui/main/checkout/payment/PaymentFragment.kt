package com.deva.ecommerce.ui.main.checkout.payment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.deva.ecommerce.databinding.FragmentPaymentBinding
import com.deva.ecommerce.model.remote.response.PaymentTitle
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import com.google.firebase.remoteconfig.ktx.get
import com.google.gson.GsonBuilder
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class PaymentFragment : Fragment() {
    private var _binding: FragmentPaymentBinding? = null
    private val binding get() = _binding!!

    @Inject
    lateinit var remoteConfig: FirebaseRemoteConfig

    @Inject
    lateinit var analytics: FirebaseAnalytics

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentPaymentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setPaymentList()
    }

    private fun setPaymentList() {
        val paymentList = GsonBuilder().create().fromJson(
            remoteConfig[PAYMENT].asString(),
            Array<PaymentTitle>::class.java
        ).toList()

        val paymentAdapter = PaymentTitleAdapter(analytics)
        binding.rvPaymentList.adapter = paymentAdapter
        paymentAdapter.setData(paymentList)
        binding.rvPaymentList.layoutManager = LinearLayoutManager(
            requireContext(),
            LinearLayoutManager.VERTICAL,
            false
        )
    }

    companion object {
        private const val PAYMENT = "payment"
    }
}

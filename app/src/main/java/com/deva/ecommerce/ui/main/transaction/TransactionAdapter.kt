package com.deva.ecommerce.ui.main.transaction

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.deva.ecommerce.R
import com.deva.ecommerce.databinding.RowTransactionListBinding
import com.deva.ecommerce.model.remote.response.FulfillmentDetail
import com.deva.ecommerce.model.remote.response.TransactionData
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.logEvent
import java.text.NumberFormat
import java.util.Locale

class TransactionAdapter :
    RecyclerView.Adapter<TransactionAdapter.TransactionViewHolder>() {
    private var transactionList = emptyList<TransactionData>()

    inner class TransactionViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val binding = RowTransactionListBinding.bind(itemView)
        private val locale = Locale("id", "ID")
        private val formatter: NumberFormat = NumberFormat.getCurrencyInstance(locale).apply {
            maximumFractionDigits = 0
        }

        @SuppressLint("SetTextI18n")
        fun bind(transaction: TransactionData) {
            with(binding) {
                tvDate.text = transaction.date
                tvProductName.text = transaction.name
                tvTotalPrice.text = formatter.format(transaction.total)
                tvQuantity.text =
                    "${transaction.items.size} ${itemView.context.getString(R.string.barang)}"

                Glide.with(itemView.context)
                    .load(transaction.image)
                    .into(ivProduct)
                if (transaction.review.isNullOrBlank() || transaction.rating == null) {
                    btnReview.visibility = View.VISIBLE
                } else {
                    btnReview.visibility = View.GONE
                }

                binding.btnReview.setOnClickListener {
                    val transactionDetail = FulfillmentDetail(
                        id = transaction.id,
                        date = transaction.date,
                        payment = transaction.payment,
                        total = transaction.total,
                        time = transaction.time,
                        status = transaction.status,
                        review = transaction.review,
                        rating = if (transaction.rating == null) null else transaction.rating.toInt(),
                    )

                    val transactionBundle = bundleOf(
                        "transactionItem" to transactionDetail
                    )

                    Navigation.findNavController(it)
                        .navigate(
                            R.id.action_navigation_transaction_to_navigation_fulfillment,
                            transactionBundle
                        )

                    val analytics = FirebaseAnalytics.getInstance(itemView.context)
                    analytics.logEvent("btn_add_review_clicked") {
                        param("transaction_id", transaction.id)
                    }
                }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TransactionViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(
            R.layout.row_transaction_list,
            parent,
            false
        )

        return TransactionViewHolder(view)
    }

    override fun getItemCount(): Int {
        return transactionList.size
    }

    override fun onBindViewHolder(holder: TransactionViewHolder, position: Int) {
        holder.bind(transactionList[position])
    }

    fun setData(transaction: List<TransactionData>) {
        transaction.isEmpty()
        this.transactionList = transaction
    }
}

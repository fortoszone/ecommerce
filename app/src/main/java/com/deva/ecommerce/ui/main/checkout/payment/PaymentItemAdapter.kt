package com.deva.ecommerce.ui.main.checkout.payment

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.deva.ecommerce.R
import com.deva.ecommerce.databinding.RowPaymentItemBinding
import com.deva.ecommerce.model.remote.response.PaymentItems

class PaymentItemAdapter(
    private val paymentItems: List<PaymentItems>,
    private var onItemClicked: (PaymentItems) -> Unit
) :
    RecyclerView.Adapter<PaymentItemAdapter.DataViewHolder>() {

    inner class DataViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val binding = RowPaymentItemBinding.bind(itemView)
        fun bind(paymentItems: PaymentItems) {
            binding.tvPaymentName.text = paymentItems.label
            Glide.with(itemView.context)
                .load(paymentItems.image)
                .into(binding.ivPaymentLogo)

            if (!paymentItems.status) {
                binding.paymentItem.apply {
                    isEnabled = false
                    isClickable = false
                    alpha = 0.3f
                }
            } else {
                binding.paymentItem.apply {
                    isEnabled = true
                    isClickable = true
                    alpha = 1f
                }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = DataViewHolder(
        LayoutInflater.from(parent.context).inflate(
            R.layout.row_payment_item,
            parent,
            false
        )
    )

    override fun onBindViewHolder(holder: DataViewHolder, position: Int) {
        holder.bind(paymentItems[position])
        holder.itemView.setOnClickListener {
            onItemClicked(paymentItems[position])
        }
    }

    override fun getItemCount(): Int = paymentItems.size
}

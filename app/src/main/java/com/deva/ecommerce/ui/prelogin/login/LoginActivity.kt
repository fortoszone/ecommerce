package com.deva.ecommerce.ui.prelogin.login

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.widget.doOnTextChanged
import com.deva.ecommerce.R
import com.deva.ecommerce.databinding.ActivityLoginBinding
import com.deva.ecommerce.preference.AuthPreference
import com.deva.ecommerce.ui.MainActivity
import com.deva.ecommerce.ui.prelogin.register.RegisterActivity
import com.google.android.material.badge.ExperimentalBadgeUtils
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.logEvent
import com.google.firebase.messaging.FirebaseMessaging
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@ExperimentalBadgeUtils
@AndroidEntryPoint
class LoginActivity : AppCompatActivity() {
    private lateinit var binding: ActivityLoginBinding

    private val authPreference: AuthPreference by lazy {
        AuthPreference(this)
    }

    private val viewModel: LoginViewModel by viewModels()

    @Inject
    lateinit var analytics: FirebaseAnalytics

    @Inject
    lateinit var messaging: FirebaseMessaging

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)

        viewModel.isLoading.observe(this) { isLoading ->
            if (isLoading) {
                binding.progressBar.visibility = android.view.View.VISIBLE
            } else {
                binding.progressBar.visibility = android.view.View.GONE
            }
        }

        messaging.token.addOnCompleteListener { task ->
            if (!task.isSuccessful) {
                Toast.makeText(
                    this,
                    "Fetching FCM registration token failed",
                    Toast.LENGTH_SHORT
                )
                    .show()
                return@addOnCompleteListener
            }
            val token = task.result
            authPreference.saveFirebaseToken(token)
        }

        binding.btnLogin.setOnClickListener {
            val email = binding.email.text.toString().trim()
            val password = binding.pwd.text.toString().trim()
            viewModel.login(email, password, authPreference.fetchFirebaseToken()!!)

            viewModel.data.observe(this) {
                if (it != null) {
                    analytics.logEvent(FirebaseAnalytics.Event.LOGIN) {
                        param("email", email)
                    }

                    startActivity(Intent(this, MainActivity::class.java))
                    finish()
                }
            }

            viewModel.error.observe(this) {
                if (it != null) {
                    Toast.makeText(this, it, Toast.LENGTH_SHORT).show()
                }
            }
        }

        binding.btnToRegister.setOnClickListener {
            analytics.logEvent("btn_to_register_clicked", null)
            startActivity(Intent(this, RegisterActivity::class.java))
            finish()
        }

        binding.email.doOnTextChanged { text, _, _, _ ->
            viewModel.checkEmail(text.toString())
        }

        binding.pwd.doOnTextChanged { text, _, _, _ ->
            viewModel.checkPassword(text.toString())
        }

        viewModel.emailUser.observe(this@LoginActivity) { emailValid ->
            if (emailValid) {
                binding.tilEmail.error = null
            } else {
                binding.tilEmail.error = getString(R.string.email_not_valid)
            }
        }

        viewModel.passwordUser.observe(this@LoginActivity) { passwordValid ->
            if (passwordValid) {
                binding.tilPwd.error = null
            } else {
                binding.tilPwd.error = getString(R.string.invalid_pwd)
            }
        }

        binding.btnLogin.isEnabled = false
        viewModel.isFormValid.observe(this@LoginActivity) { formValid ->
            binding.btnLogin.isEnabled = formValid
        }
    }
}

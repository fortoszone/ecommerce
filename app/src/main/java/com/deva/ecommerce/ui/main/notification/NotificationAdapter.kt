package com.deva.ecommerce.ui.main.notification

import android.annotation.SuppressLint
import android.content.res.Configuration
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.deva.ecommerce.R
import com.deva.ecommerce.databinding.RowNotificationListBinding
import com.deva.ecommerce.model.local.entity.Notification
import com.google.android.material.color.MaterialColors

class NotificationAdapter(private val callback: NotificationClickListener) :
    RecyclerView.Adapter<NotificationAdapter.NotificationViewHolder>() {
    private var notifications = emptyList<Notification>()

    inner class NotificationViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val binding = RowNotificationListBinding.bind(itemView)

        @SuppressLint("ResourceType")
        fun bind(notification: Notification) {
            with(binding) {
                tvTitle.text = notification.title
                tvBody.text = notification.body
                tvDate.text = notification.date
                tvType.text = notification.type

                Glide.with(itemView.context)
                    .load(notification.image)
                    .into(binding.ivNotification)
            }

            if (notification.isRead) {
                binding.notificationListItem.setBackgroundColor(
                    MaterialColors.getColor(
                        itemView,
                        android.R.attr.colorBackground
                    )
                )
            } else {
                when (itemView.resources.configuration.uiMode and Configuration.UI_MODE_NIGHT_MASK) {
                    Configuration.UI_MODE_NIGHT_YES ->
                        binding.notificationListItem.setBackgroundColor(
                            itemView.resources.getColor(
                                R.color.purple,
                                null
                            )
                        )

                    else -> binding.notificationListItem.setBackgroundColor(
                        itemView.resources.getColor(
                            R.color.notification_not_read_yet,
                            null
                        )
                    )
                }
            }

            binding.notificationListItem.setOnClickListener {
                callback.onNotificationClick(notification)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NotificationViewHolder {
        return NotificationViewHolder(
            RowNotificationListBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            ).root
        )
    }

    override fun getItemCount(): Int {
        return notifications.size
    }

    override fun onBindViewHolder(holder: NotificationViewHolder, position: Int) {
        holder.bind(notifications[position])
    }

    fun setData(notification: List<Notification>) {
        notification.isEmpty()
        this.notifications = notification
    }
}

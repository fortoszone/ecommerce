package com.deva.ecommerce.ui.main.home

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatDelegate
import androidx.core.os.LocaleListCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.deva.ecommerce.databinding.FragmentHomeBinding
import com.deva.ecommerce.preference.AuthPreference
import com.deva.ecommerce.preference.SessionManager
import com.deva.ecommerce.ui.prelogin.login.LoginActivity
import com.google.android.material.badge.ExperimentalBadgeUtils
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
@ExperimentalBadgeUtils
class HomeFragment : Fragment() {
    private var _binding: FragmentHomeBinding? = null
    private val binding get() = _binding!!
    private val sessionManager: SessionManager by lazy {
        SessionManager(requireContext())
    }

    private val authPreference: AuthPreference by lazy {
        AuthPreference(requireContext())
    }

    private val viewModel: HomeViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.logout.setOnClickListener {
            sessionManager.clearSession()
            authPreference.deleteStatus()
            viewModel.deleteAllNotification()
            viewModel.deleteAllCart()
            viewModel.deleteAllWishlist()
            startActivity(Intent(requireContext(), LoginActivity::class.java))
            requireActivity().finish()
        }

        binding.switchLanguage.isChecked =
            when (resources.configuration.locales[0].language) {
                "in" -> true
                else -> false
            }

        binding.switchLanguage.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                val appLocale: LocaleListCompat = LocaleListCompat.forLanguageTags("in")
                AppCompatDelegate.setApplicationLocales(appLocale)
            } else {
                val appLocale: LocaleListCompat = LocaleListCompat.forLanguageTags("en")
                AppCompatDelegate.setApplicationLocales(appLocale)
            }
        }

        binding.switchTheme.isChecked = sessionManager.isDarkMode()

        binding.switchTheme.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES)
            } else {
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
            }
            sessionManager.saveTheme(isChecked)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}

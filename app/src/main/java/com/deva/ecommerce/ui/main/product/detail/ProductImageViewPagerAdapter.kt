package com.deva.ecommerce.ui.main.product.detail

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.deva.ecommerce.R
import com.deva.ecommerce.databinding.RowProductImageBinding

class ProductImageViewPagerAdapter(private val image: List<String>, private val context: Context) :
    RecyclerView.Adapter<ProductImageViewPagerAdapter.PagerViewHolder>() {

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): PagerViewHolder {
        val view: View =
            LayoutInflater.from(context).inflate(R.layout.row_product_image, parent, false)
        return PagerViewHolder(view)
    }

    override fun onBindViewHolder(
        holder: PagerViewHolder,
        position: Int
    ) {
        holder.bind(image[position])
    }

    override fun getItemCount(): Int {
        return image.size
    }

    inner class PagerViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val binding = RowProductImageBinding.bind(itemView)

        fun bind(data: String) {
            with(binding) {
                Glide.with(itemView.context)
                    .load(data)
                    .into(ivProductImage)
            }
        }
    }
}

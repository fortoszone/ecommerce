package com.deva.ecommerce.ui.main.store

import androidx.annotation.StringRes
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.cachedIn
import com.deva.ecommerce.R
import com.deva.ecommerce.model.remote.ApiService
import com.deva.ecommerce.ui.main.store.paging.ProductPagingSource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.flatMapLatest
import javax.inject.Inject

@HiltViewModel
class StoreViewModel @Inject constructor(
    private val apiService: ApiService
) : ViewModel() {
    private var _isGrid = MutableLiveData(false)
    val isGrid: LiveData<Boolean> get() = _isGrid

    private val _searchedProductName: MutableStateFlow<String> = MutableStateFlow("")
    val searchedProductName: LiveData<String> get() = _searchedProductName.asLiveData()

    private var _filterSelectedState: MutableStateFlow<FilterState?> = MutableStateFlow(null)
    val filterSelectedState: LiveData<FilterState?> get() = _filterSelectedState.asLiveData()

    private val productNameAndFilterPair = combine(
        _searchedProductName,
        _filterSelectedState
    ) { productName, filter ->
        Pair(productName, filter)
    }

    @OptIn(ExperimentalCoroutinesApi::class)
    val pager = productNameAndFilterPair.flatMapLatest { (productName, filter) ->
        Pager(
            PagingConfig(
                pageSize = 10,
                initialLoadSize = 10,
                prefetchDistance = 1
            )
        ) {
            ProductPagingSource(
                apiService,
                search = productName,
                brand = filter?.category,
                lowest = filter?.lowestPrice,
                highest = filter?.highestPrice,
                sort = filter?.sort
            )
        }.flow
    }.cachedIn(viewModelScope)

    fun toggleLayout() {
        _isGrid.value = !(_isGrid.value ?: false)
    }

    fun searchItems(productName: String) {
        _searchedProductName.value = productName
    }

    fun setFilter(
        sortCategory: SortCategory?,
        brand: String?,
        lowestPrice: Int?,
        highestPrice: Int?
    ) {
        _filterSelectedState.value = FilterState(
            sortCategory,
            brand,
            lowestPrice,
            highestPrice
        )
    }

    fun resetFilter() {
        _filterSelectedState.value = null
    }
}

data class FilterState(
    val sort: SortCategory? = null,
    val category: String? = null,
    val lowestPrice: Int? = null,
    val highestPrice: Int? = null,
)

enum class SortCategory(
    @StringRes val displayedName: Int,
    val value: String
) {
    Review(R.string.review, "rating"),
    Sale(R.string.sold, "sale"),
    LowestPrice(R.string.lowest_price, "lowest"),
    HighestPrice(R.string.highest_price, "highest")
}

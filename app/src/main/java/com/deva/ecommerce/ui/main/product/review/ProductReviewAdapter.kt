package com.deva.ecommerce.ui.main.product.review

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.deva.ecommerce.R
import com.deva.ecommerce.databinding.RowProductReviewListBinding
import com.deva.ecommerce.model.remote.response.ProductReviewData

class ProductReviewAdapter(
    private val review: List<ProductReviewData>,
    private val context: Context
) :
    RecyclerView.Adapter<ProductReviewAdapter.ProductReviewViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductReviewViewHolder {
        val view: View =
            LayoutInflater.from(context).inflate(R.layout.row_product_review_list, parent, false)
        return ProductReviewViewHolder(view)
    }

    class ProductReviewViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val binding = RowProductReviewListBinding.bind(view)
        fun bind(review: ProductReviewData) {
            Glide.with(itemView.context).load(review.userImage)
                .error(R.drawable.thumbnail).circleCrop()
                .into(binding.ivUser)
            review.userName.let { binding.tvUserName.text = it }
            review.userReview.let { binding.tvReview.text = it }
            review.userRating.let { binding.ratingBar.rating = it.toFloat() }
        }
    }

    override fun getItemCount(): Int {
        return review.size
    }

    override fun onBindViewHolder(holder: ProductReviewViewHolder, position: Int) {
        holder.bind(review[position])
    }
}

package com.deva.ecommerce.ui.main.home

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.deva.ecommerce.model.local.room.CartDao
import com.deva.ecommerce.model.local.room.NotificationDao
import com.deva.ecommerce.model.local.room.WishlistDao
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor(
    private val cartDao: CartDao,
    private val wishlistDao: WishlistDao,
    private val notificationDao: NotificationDao
) : ViewModel() {
    fun deleteAllCart() {
        viewModelScope.launch {
            cartDao.deleteAllCart()
        }
    }

    fun deleteAllWishlist() {
        viewModelScope.launch {
            wishlistDao.deleteAllWishlist()
        }
    }

    fun deleteAllNotification() {
        viewModelScope.launch {
            notificationDao.deleteAllNotification()
        }
    }
}

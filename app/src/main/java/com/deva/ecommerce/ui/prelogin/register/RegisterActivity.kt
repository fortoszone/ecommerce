package com.deva.ecommerce.ui.prelogin.register

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.widget.addTextChangedListener
import androidx.core.widget.doOnTextChanged
import com.deva.ecommerce.R
import com.deva.ecommerce.databinding.ActivityRegisterBinding
import com.deva.ecommerce.preference.AuthPreference
import com.deva.ecommerce.preference.SessionManager
import com.deva.ecommerce.ui.prelogin.login.LoginActivity
import com.deva.ecommerce.ui.prelogin.profile.ProfileActivity
import com.google.android.material.badge.ExperimentalBadgeUtils
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.logEvent
import com.google.firebase.messaging.FirebaseMessaging
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@ExperimentalBadgeUtils
@AndroidEntryPoint
class RegisterActivity : AppCompatActivity() {
    private lateinit var binding: ActivityRegisterBinding

    @Inject
    lateinit var analytics: FirebaseAnalytics

    @Inject
    lateinit var messaging: FirebaseMessaging

    @Inject
    lateinit var authPreference: AuthPreference

    @Inject
    lateinit var sessionManager: SessionManager

    private val viewModel: RegisterViewModel by viewModels()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityRegisterBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.btnToLogin.setOnClickListener {
            analytics.logEvent("btn_to_login_clicked", null)
            startActivity(Intent(this, LoginActivity::class.java))
            finish()
        }

        viewModel.isLoading.observe(this) { isLoading ->
            if (isLoading) {
                binding.progressBar.visibility = android.view.View.VISIBLE
            } else {
                binding.progressBar.visibility = android.view.View.GONE
            }
        }

        messaging.token.addOnCompleteListener { task ->
            if (!task.isSuccessful) {
                Toast.makeText(
                    this,
                    "Fetching FCM registration token failed",
                    Toast.LENGTH_SHORT
                )
                    .show()
                return@addOnCompleteListener
            }
            val token = task.result
            authPreference.saveFirebaseToken(token)
        }

        binding.btnRegister.setOnClickListener {
            val email = binding.email.text.toString().trim()
            val password = binding.pwd.text.toString().trim()
            viewModel.register(email, password, authPreference.fetchFirebaseToken()!!)

            viewModel.data.observe(this) {
                if (it != null) {
                    startActivity(Intent(this, ProfileActivity::class.java))
                    finish()
                }
            }

            viewModel.error.observe(this) {
                if (!it.isNullOrEmpty()) {
                    Toast.makeText(this, it, Toast.LENGTH_SHORT).show()
                }
            }

            analytics.logEvent(FirebaseAnalytics.Event.SIGN_UP) {
                param("email", email)
            }
        }

        binding.email.addTextChangedListener {
            binding.email.error = null
        }

        binding.email.doOnTextChanged { text, _, _, _ ->
            viewModel.checkEmail(text.toString())
        }

        binding.pwd.doOnTextChanged { text, _, _, _ ->
            viewModel.checkPassword(text.toString())
        }

        viewModel.emailUser.observe(this@RegisterActivity) { emailValid ->
            if (emailValid) {
                binding.tilEmail.error = null
            } else {
                binding.tilEmail.error = getString(R.string.email_not_valid)
            }
        }

        viewModel.passwordUser.observe(this@RegisterActivity) { passwordValid ->
            if (passwordValid) {
                binding.tilPwd.error = null
            } else {
                binding.tilPwd.error = getString(R.string.invalid_pwd)
            }
        }

        binding.btnRegister.isEnabled = false
        viewModel.isFormValid.observe(this@RegisterActivity) { formValid ->
            binding.btnRegister.isEnabled = formValid
        }
    }
}

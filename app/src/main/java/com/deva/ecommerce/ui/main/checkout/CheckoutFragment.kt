package com.deva.ecommerce.ui.main.checkout

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.Navigation
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.deva.ecommerce.R
import com.deva.ecommerce.databinding.FragmentCheckoutBinding
import com.deva.ecommerce.model.local.entity.CartItem
import com.deva.ecommerce.model.remote.request.FulfillmentProduct
import com.deva.ecommerce.model.remote.request.FulfillmentRequest
import com.deva.ecommerce.model.remote.response.PaymentItems
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.logEvent
import dagger.hilt.android.AndroidEntryPoint
import java.text.NumberFormat
import java.util.Locale
import javax.inject.Inject

@AndroidEntryPoint
class CheckoutFragment : Fragment() {
    private var _binding: FragmentCheckoutBinding? = null
    private val binding get() = _binding!!
    private val productList = mutableListOf<CartItem>()
    private val locale = Locale("id", "ID")
    private val formatter: NumberFormat = NumberFormat.getCurrencyInstance(locale).apply {
        maximumFractionDigits = 0
    }
    private val viewModel: CheckoutViewModel by viewModels()
    private var selectedPayment: String? = null

    private val checkedOutProductList = mutableListOf<FulfillmentProduct>()

    @Inject
    lateinit var analytics: FirebaseAnalytics

    private var onCheckoutClickListener = object : CheckoutClickListener {
        override fun onTotalPriceChanged(totalPriceFromChecked: Int) {
            binding.tvTotalPriceValue.text = formatter.format(totalPriceFromChecked)
        }

        override fun onAddQtyClick(cartItem: CartItem) {
            if (cartItem.quantity + 1 > cartItem.stock) {
                Snackbar.make(
                    requireView(),
                    R.string.insufficient_stock,
                    Snackbar.LENGTH_SHORT
                ).show()
            } else {
                cartItem.quantity++
                viewModel.updateCartQuantity(cartItem.id, cartItem.quantity)
                checkoutAnalytics(cartItem, "btn_checkout_add_qty_clicked")
            }
        }

        override fun onRemoveQtyClick(cartItem: CartItem) {
            if (cartItem.quantity > 1) {
                cartItem.quantity--
                viewModel.updateCartQuantity(cartItem.id, cartItem.quantity)
                checkoutAnalytics(cartItem, "btn_checkout_remove_qty_clicked")
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentCheckoutBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setItem()
        setSelectedPayment()
        setLoadingState()

        binding.cvPayment.setOnClickListener {
            Navigation.findNavController(requireView())
                .navigate(R.id.action_navigation_checkout_to_navigation_payment)

            analytics.logEvent("btn_checkout_payment_clicked") {
                param("payment", selectedPayment.toString())
            }
        }

        viewModel.setIsPaymentMethodSelected(false)
        viewModel.isPaymentMethodSelected.observe(viewLifecycleOwner) {
            binding.btnPay.isEnabled = it
        }

        binding.btnPay.setOnClickListener {
            getFulfillment()
            analytics.logEvent("btn_checkout_pay_clicked") {
                param("payment", selectedPayment.toString())
            }
        }
    }

    private fun setLoadingState() {
        viewModel.isLoading.observe(viewLifecycleOwner) {
            binding.progressBar.isVisible = it
        }
    }

    private fun getFulfillment() {
        val selectedPayment = binding.tvPaymentName.text.toString()
        if (productList.isNotEmpty()) {
            for (product in productList) {
                checkedOutProductList.add(
                    FulfillmentProduct(
                        id = product.id,
                        quantity = product.quantity,
                        variant = product.variant
                    )
                )

                viewModel.deleteCartItem(product.id)
            }

            viewModel.getFulfillment(
                FulfillmentRequest(
                    payment = selectedPayment,
                    items = checkedOutProductList as ArrayList<FulfillmentProduct>
                )
            )

            viewModel.data.observe(viewLifecycleOwner) {
                if (it != null) {
                    val bundle = bundleOf("fulfillment" to it.data)
                    findNavController().navigate(
                        R.id.action_navigation_checkout_to_navigation_fulfillment,
                        bundle
                    )

                    analytics.logEvent(FirebaseAnalytics.Event.PURCHASE) {
                        param(FirebaseAnalytics.Param.TRANSACTION_ID, it.data.id)
                        param(FirebaseAnalytics.Param.PAYMENT_TYPE, it.data.payment)
                        param("Time", it.data.time)
                        param("Date", it.data.date)
                        param("Status", it.data.status.toString())
                        param("Total price", it.data.total.toString())
                    }
                }
            }
        }
    }

    private fun setSelectedPayment() {
        val navigation = findNavController()
        navigation.currentBackStackEntry?.savedStateHandle?.getLiveData<PaymentItems>("payment")
            ?.observe(viewLifecycleOwner) {
                if (it != null) {
                    viewModel.setIsPaymentMethodSelected(true)
                    selectedPayment = it.label
                    binding.tvPaymentName.text = selectedPayment
                    Glide.with(requireContext())
                        .load(it.image)
                        .into(binding.ivPaymentLogo)
                }
            }
    }

    private fun setItem() {
        val checkoutAdapter =
            CheckoutAdapter(onCheckoutClickListener, productList as ArrayList<CartItem>)
        val item = arguments?.getParcelableArrayList<CartItem>("product")
        if (item != null) {
            item.forEach {
                val bundle = Bundle().apply {
                    putString(FirebaseAnalytics.Param.ITEM_ID, it.id)
                    putString(FirebaseAnalytics.Param.ITEM_NAME, it.name)
                    putString(FirebaseAnalytics.Param.ITEM_VARIANT, it.variant)
                    putInt(FirebaseAnalytics.Param.PRICE, it.price)
                    putInt(FirebaseAnalytics.Param.QUANTITY, it.quantity)
                }

                analytics.logEvent(FirebaseAnalytics.Event.BEGIN_CHECKOUT) {
                    param(FirebaseAnalytics.Param.ITEMS, arrayOf(bundle))
                }
            }

            productList.clear()
            productList.addAll(item)
            binding.rvCheckout.apply {
                adapter = checkoutAdapter
                checkoutAdapter.updateTotalPrice()
                layoutManager = LinearLayoutManager(requireContext())
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        productList.clear()
        _binding = null
    }

    private fun checkoutAnalytics(cartItem: CartItem, event: String) {
        val item = Bundle().apply {
            putString(FirebaseAnalytics.Param.ITEM_ID, cartItem.id)
            putString(FirebaseAnalytics.Param.ITEM_NAME, cartItem.name)
            putString(FirebaseAnalytics.Param.ITEM_VARIANT, cartItem.variant)
            putInt(FirebaseAnalytics.Param.PRICE, cartItem.price)
            putInt(FirebaseAnalytics.Param.QUANTITY, cartItem.quantity)
        }

        analytics.logEvent(event) {
            param(FirebaseAnalytics.Param.ITEMS, arrayOf(item))
        }
    }
}

package com.deva.ecommerce.ui.main.checkout.payment

import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.deva.ecommerce.databinding.RowPaymentListBinding
import com.deva.ecommerce.model.remote.response.PaymentItems
import com.deva.ecommerce.model.remote.response.PaymentTitle
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.logEvent

class PaymentTitleAdapter(private val analytics: FirebaseAnalytics) :
    RecyclerView.Adapter<PaymentTitleAdapter.ViewHolder>() {
    private var titles = ArrayList<PaymentTitle>()

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val binding = RowPaymentListBinding.bind(view)

        fun bind(paymentTitle: PaymentTitle) {
            binding.tvPaymentType.text = paymentTitle.title
            val childAdapter = PaymentItemAdapter(paymentTitle.items) { childItem ->
                val selectedPayment = PaymentItems(
                    childItem.label,
                    childItem.image,
                    childItem.status
                )

                analytics.logEvent(FirebaseAnalytics.Event.ADD_PAYMENT_INFO) {
                    param(FirebaseAnalytics.Param.PAYMENT_TYPE, childItem.label)
                }

                val navController = findNavController(itemView)
                navController.previousBackStackEntry?.savedStateHandle?.set(
                    "payment",
                    selectedPayment
                )

                navController.popBackStack()
            }

            binding.rvPaymentItemList.layoutManager = LinearLayoutManager(itemView.context)
            binding.rvPaymentItemList.adapter = childAdapter
        }
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): PaymentTitleAdapter.ViewHolder {
        val view = View.inflate(parent.context, com.deva.ecommerce.R.layout.row_payment_list, null)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: PaymentTitleAdapter.ViewHolder, position: Int) {
        holder.bind(titles[position])
    }

    override fun getItemCount(): Int {
        return titles.size
    }

    fun setData(it: List<PaymentTitle>) {
        for (i in it.indices) {
            titles.add(it[i])
        }

        notifyDataSetChanged()
    }
}

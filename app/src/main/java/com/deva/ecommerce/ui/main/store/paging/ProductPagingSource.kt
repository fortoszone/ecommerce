package com.deva.ecommerce.ui.main.store.paging

import android.util.Log
import androidx.annotation.Keep
import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.bumptech.glide.load.HttpException
import com.deva.ecommerce.model.remote.ApiService
import com.deva.ecommerce.model.remote.response.ProductItems
import com.deva.ecommerce.model.remote.response.ProductResponse
import com.deva.ecommerce.ui.main.store.SortCategory
import com.google.gson.Gson
import kotlinx.coroutines.delay
import java.io.IOException

@Keep
class ProductPagingSource(
    private val apiService: ApiService,
    private val search: String?,
    private val brand: String?,
    private val lowest: Int?,
    private val highest: Int?,
    private val sort: SortCategory?,
) :
    PagingSource<Int, ProductItems>() {
    override fun getRefreshKey(state: PagingState<Int, ProductItems>): Int? {
        return state.anchorPosition?.let { anchorPosition ->
            val anchorPage = state.closestPageToPosition(anchorPosition)
            anchorPage?.prevKey?.plus(1) ?: anchorPage?.nextKey?.minus(1)
        }
    }

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, ProductItems> {
        delay(1000)
        try {
            val page = params.key ?: 1
            val response = apiService.products(
                search = search,
                brand = brand,
                sort = sort,
                highest = highest,
                lowest = lowest,
                page = page,
                limit = params.loadSize
            )

            val data = response.body()?.data?.items
            val prevKey = if (page == 1) null else page - 1
            val nextKey = if (page == response.body()?.data?.totalPages) null else page + 1

            return if (response.isSuccessful) {
                LoadResult.Page(
                    data = data ?: emptyList(),
                    prevKey = prevKey,
                    nextKey = nextKey
                )
            } else {
                val errResponse =
                    Gson().fromJson(response.errorBody()?.string(), ProductResponse::class.java)
                LoadResult.Error(Throwable(errResponse.message))
            }
        } catch (e: IOException) {
            Log.d("TAGGG", "IOException: $e")
            return LoadResult.Error(e)
        } catch (e: HttpException) {
            Log.d("TAGGG", "HTTPException: $e")
            return LoadResult.Error(e)
        } catch (e: Exception) {
            return LoadResult.Error(e)
        }
    }
}

package com.deva.ecommerce.ui.prelogin.profile

import android.graphics.Bitmap
import android.media.ThumbnailUtils
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.deva.ecommerce.model.remote.ApiService
import com.deva.ecommerce.model.remote.response.ProfileResponse
import com.deva.ecommerce.preference.AuthPreference
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody.Companion.asRequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.HttpException
import retrofit2.Response
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.IOException
import javax.inject.Inject

@HiltViewModel
class ProfileViewModel @Inject constructor(
    private val apiService: ApiService,
    private val authPreference: AuthPreference
) : ViewModel() {
    private var _nameUser = MediatorLiveData<Boolean>()
    val nameUser: LiveData<Boolean> get() = _nameUser

    private var _data = MutableLiveData<ProfileResponse>()
    val data: LiveData<ProfileResponse> get() = _data

    private var _error = MutableLiveData<ProfileResponse>()
    val error: LiveData<ProfileResponse> get() = _error

    fun isNameUserFilled(nameUser: String) {
        _nameUser.value = nameUser.isNotEmpty()
    }

    fun addProfile(userName: String, userImage: File?) {
        val name = MultipartBody.Part.createFormData("userName", userName)

        val fileImage = if (userImage != null) {
            MultipartBody.Part.createFormData(
                "userImage",
                userImage.name,
                userImage.asRequestBody("image/*".toMediaTypeOrNull())
            )
        } else {
            null
        }

        apiService.addProfileInfo(name, fileImage)
            .enqueue(object : Callback<ProfileResponse> {
                override fun onResponse(
                    call: Call<ProfileResponse>,
                    response: Response<ProfileResponse>
                ) {
                    if (response.isSuccessful) {
                        _data.value = response.body()

                        authPreference.saveLoginStatus(true)
                    } else {
                        _error.value = response.errorBody()?.let {
                            ProfileResponse(
                                message = it.string(),
                                code = response.code(),
                                data = null
                            )
                        }
                    }
                }

                override fun onFailure(call: Call<ProfileResponse>, t: Throwable) {
                    when (t) {
                        is IOException -> _error.value = ProfileResponse(
                            message = "Network Error",
                            code = 500,
                            data = null
                        )

                        is HttpException -> {
                            val code = t.code()
                            val errorResponse = t.message()
                            _error.value = ProfileResponse(
                                message = errorResponse,
                                code = code,
                                data = null
                            )
                        }

                        else -> _error.value = ProfileResponse(
                            message = t.message.toString(),
                            code = 500,
                            data = null
                        )
                    }
                }
            })
    }

    suspend fun processImage(image: Bitmap, file: File): Bitmap =
        withContext(Dispatchers.IO) {
            val byteArrayOutputStream = ByteArrayOutputStream()
            val cropped = ThumbnailUtils.extractThumbnail(image, 512, 512)

            cropped.compress(Bitmap.CompressFormat.PNG, 0, byteArrayOutputStream)
            val byteArray = byteArrayOutputStream.toByteArray()
            file.outputStream().use { output ->
                output.write(byteArray)
                output.flush()
                output.close()
            }
            cropped
        }
}

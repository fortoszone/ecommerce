package com.deva.ecommerce.ui.main.cart

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.deva.ecommerce.R
import com.deva.ecommerce.databinding.RowCartListBinding
import com.deva.ecommerce.model.local.entity.CartItem
import java.text.NumberFormat
import java.util.Locale

class CartAdapter(private val callback: CartClickListener) :
    RecyclerView.Adapter<CartAdapter.CartViewHolder>() {
    private var cartItems = emptyList<CartItem>()

    inner class CartViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val binding = RowCartListBinding.bind(view)
        private val locale = Locale("id", "ID")
        private val formatter: NumberFormat = NumberFormat.getCurrencyInstance(locale).apply {
            maximumFractionDigits = 0
        }

        @SuppressLint("SetTextI18n")
        fun bind(cartItem: CartItem) {
            binding.cbCheckProduct.setOnClickListener {
                callback.onCheckClick(cartItem)
            }

            binding.cbCheckProduct.isChecked = cartItem.isSelected
            binding.quantity.text = cartItem.quantity.toString()
            binding.tvProductName.text = cartItem.name
            binding.tvProductPrice.text = formatter.format(cartItem.price)
            binding.tvProductStock.apply {
                if (cartItem.stock != 0) {
                    if (cartItem.stock > 9) {
                        text = if (Locale.getDefault().language == "en") {
                            itemView.context.getString(R.string.stock) + " " + cartItem.stock
                        } else {
                            itemView.context.getString(R.string.stock) + " " + cartItem.stock
                        }
                    } else {
                        text = if (Locale.getDefault().language == "en") {
                            cartItem.stock.toString() + " " + itemView.context.getString(R.string.remaining)
                        } else {
                            itemView.context.getString(R.string.remaining) + " " + cartItem.stock
                        }
                        setTextColor(resources.getColor(R.color.red, null))
                    }
                }
            }

            binding.tvProductVariant.text = cartItem.variant
            Glide.with(itemView.context)
                .load(cartItem.image)
                .into(binding.ivProduct)

            binding.cartItem.setOnClickListener {
                val id = bundleOf("id" to cartItem.id)
                Navigation.findNavController(it)
                    .navigate(
                        R.id.action_navigation_cart_to_navigation_product_detail,
                        id
                    )
            }

            binding.btnDelete.setOnClickListener {
                callback.onRemoveClick(cartItem)
            }

            binding.btnCounter.addOnButtonCheckedListener { group, _, _ ->
                group.clearChecked()
            }

            binding.btnPlus.setOnClickListener {
                callback.onAddQtyClick(cartItem)
                notifyDataSetChanged()
            }

            binding.btnMinus.setOnClickListener {
                callback.onRemoveQtyClick(cartItem)
                notifyDataSetChanged()
            }

            binding.quantity.isClickable = false
        }
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): CartViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(
            R.layout.row_cart_list,
            parent,
            false
        )

        return CartViewHolder(view)
    }

    override fun onBindViewHolder(holder: CartViewHolder, position: Int) {
        holder.bind(cartItems[position])
    }

    override fun getItemCount(): Int {
        return cartItems.size
    }

    fun setData(cartItem: List<CartItem>) {
        cartItem.isEmpty()
        this.cartItems = cartItem
    }

    fun updatePrice() {
        val filteredData = cartItems.filter { it.isSelected }
        val totalPriceFromChecked = filteredData.sumOf { it.price * it.quantity }
        callback.onTotalPriceChanged(totalPriceFromChecked)
    }
}

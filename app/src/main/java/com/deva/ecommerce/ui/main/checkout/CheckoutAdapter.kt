package com.deva.ecommerce.ui.main.checkout

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.deva.ecommerce.R
import com.deva.ecommerce.databinding.RowCheckoutListBinding
import com.deva.ecommerce.model.local.entity.CartItem
import java.text.NumberFormat
import java.util.Locale

class CheckoutAdapter(
    private val callback: CheckoutClickListener,
    private val products: ArrayList<CartItem>
) :
    RecyclerView.Adapter<CheckoutAdapter.CheckoutViewHolder>() {
    inner class CheckoutViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val binding = RowCheckoutListBinding.bind(itemView)
        private val locale = Locale("id", "ID")
        private val formatter: NumberFormat = NumberFormat.getCurrencyInstance(locale).apply {
            maximumFractionDigits = 0
        }

        @SuppressLint("SetTextI18n")
        fun bind(cartItem: CartItem) {
            with(binding) {
                tvProductName.text = cartItem.name
                tvProductPrice.text = formatter.format(cartItem.price)

                binding.tvProductStock.apply {
                    if (cartItem.stock != 0) {
                        if (cartItem.stock > 9) {
                            text = if (Locale.getDefault().language == "en") {
                                itemView.context.getString(R.string.stock) + " " + cartItem.stock
                            } else {
                                itemView.context.getString(R.string.stock) + " " + cartItem.stock
                            }
                        } else {
                            text = if (Locale.getDefault().language == "en") {
                                cartItem.stock.toString() + " " + itemView.context.getString(R.string.remaining)
                            } else {
                                itemView.context.getString(R.string.remaining) + " " + cartItem.stock
                            }
                            setTextColor(resources.getColor(R.color.red, null))
                        }
                    }
                }

                tvProductVariant.text = cartItem.variant
                quantity.text = cartItem.quantity.toString()

                Glide.with(itemView.context)
                    .load(cartItem.image)
                    .into(ivProduct)

                binding.buttonCounter.addOnButtonCheckedListener { group, _, _ ->
                    group.clearChecked()
                }

                binding.btnPlus.setOnClickListener {
                    callback.onAddQtyClick(cartItem)
                    notifyDataSetChanged()
                }

                binding.btnMinus.setOnClickListener {
                    callback.onRemoveQtyClick(cartItem)
                    notifyDataSetChanged()
                }

                binding.quantity.isClickable = false
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CheckoutViewHolder {
        return CheckoutViewHolder(
            RowCheckoutListBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            ).root
        )
    }

    override fun getItemCount(): Int {
        return products.size
    }

    override fun onBindViewHolder(holder: CheckoutViewHolder, position: Int) {
        holder.bind(products[position])
    }

    fun updateTotalPrice() {
        val totalPriceFromChecked = products.sumOf { it.price * it.quantity }
        callback.onTotalPriceChanged(totalPriceFromChecked)
        notifyDataSetChanged()
    }
}

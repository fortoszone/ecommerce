package com.deva.ecommerce.ui.main.notification

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.deva.ecommerce.model.local.entity.Notification
import com.deva.ecommerce.model.local.room.NotificationDao
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class NotificationViewModel @Inject constructor(private val notificationDao: NotificationDao) :
    ViewModel() {

    fun getNotificationList() = notificationDao.getNotification()

    fun updateNotification(notification: Notification) {
        viewModelScope.launch {
            notificationDao.updateNotification(notification)
        }
    }
}

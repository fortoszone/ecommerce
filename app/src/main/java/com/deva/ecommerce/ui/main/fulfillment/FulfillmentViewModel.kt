package com.deva.ecommerce.ui.main.fulfillment

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.deva.ecommerce.model.remote.ApiService
import com.deva.ecommerce.model.remote.request.RatingRequest
import com.deva.ecommerce.model.remote.response.RatingResponse
import dagger.hilt.android.lifecycle.HiltViewModel
import retrofit2.Call
import retrofit2.Callback
import retrofit2.HttpException
import retrofit2.Response
import java.io.IOException
import javax.inject.Inject

@HiltViewModel
class FulfillmentViewModel @Inject constructor(private val apiService: ApiService) : ViewModel() {
    private val _message = MutableLiveData<RatingResponse>()
    val message: LiveData<RatingResponse> = _message

    private val _dataError = MutableLiveData<String>()

    fun rating(id: String, rating: Int, review: String) {
        apiService.rating(RatingRequest(id, rating, review))
            .enqueue(object : Callback<RatingResponse> {
                override fun onResponse(
                    call: Call<RatingResponse>,
                    response: Response<RatingResponse>
                ) {
                    if (response.isSuccessful) {
                        _message.value = response.body()
                    } else {
                        _dataError.value = response.body()?.message
                    }
                }

                override fun onFailure(call: Call<RatingResponse>, t: Throwable) {
                    when (t) {
                        is IOException -> _dataError.value = "Network Failure"
                        is HttpException -> {
                            val code = t.code()
                            val errorResponse = t.message()
                            _dataError.value = "Error $code $errorResponse"
                        }

                        else -> _dataError.value = "Unknown Error"
                    }
                }
            })
    }
}

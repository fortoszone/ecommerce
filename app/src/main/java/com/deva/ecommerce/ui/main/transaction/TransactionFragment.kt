package com.deva.ecommerce.ui.main.transaction

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.deva.ecommerce.R
import com.deva.ecommerce.databinding.FragmentTransactionBinding
import com.deva.ecommerce.model.remote.response.TransactionData
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class TransactionFragment : Fragment() {
    private var _binding: FragmentTransactionBinding? = null
    private val binding get() = _binding!!
    private val viewModel: TransactionViewModel by viewModels()
    private val transactionList = mutableListOf<TransactionData>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentTransactionBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setTransactionList()
    }

    private fun setTransactionList() {
        viewModel.isLoading.observe(viewLifecycleOwner) {
            binding.progressBar.isVisible = it
        }

        viewModel.getTransactionList()
        viewModel.data.observe(viewLifecycleOwner) {
            transactionList.clear()
            if (it.data.isEmpty() || it.code == 404) {
                binding.error.error.visibility = View.VISIBLE
                binding.rvTransaction.visibility = View.GONE
                binding.error.btnRefresh.visibility = View.GONE
            } else {
                for (i in it.data) {
                    transactionList.add(i)
                }

                binding.error.error.visibility = View.GONE
                binding.rvTransaction.visibility = View.VISIBLE

                val transactionAdapter = TransactionAdapter()
                transactionAdapter.setData(transactionList)
                binding.rvTransaction.adapter = transactionAdapter
                val layoutManager =
                    LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
                layoutManager.reverseLayout = true
                layoutManager.stackFromEnd = true
                binding.rvTransaction.layoutManager = layoutManager
            }
        }

        viewModel.error.observe(viewLifecycleOwner) {
            if (it != null) {
                binding.rvTransaction.isVisible = false

                binding.error.error.isVisible = true
                binding.error.tvErrorDesc.text =
                    getString(R.string.your_requested_data_is_unavailable)
                binding.error.tvErrorTitle.text = getString(R.string.empty)

                binding.error.btnRefresh.isVisible = true
                binding.error.btnRefresh.setOnClickListener {
                    binding.error.error.visibility = View.GONE
                    setTransactionList()
                }
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}

package com.deva.ecommerce.ui.main.product.detail

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.defaultMinSize
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.foundation.layout.wrapContentSize
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.foundation.pager.HorizontalPager
import androidx.compose.foundation.pager.rememberPagerState
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Favorite
import androidx.compose.material.icons.filled.FavoriteBorder
import androidx.compose.material.icons.filled.Share
import androidx.compose.material.ripple.LocalRippleTheme
import androidx.compose.material.ripple.RippleAlpha
import androidx.compose.material.ripple.RippleTheme
import androidx.compose.material3.Button
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.Divider
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.IconToggleButton
import androidx.compose.material3.InputChip
import androidx.compose.material3.LocalTextStyle
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedButton
import androidx.compose.material3.Scaffold
import androidx.compose.material3.SnackbarDuration
import androidx.compose.material3.SnackbarHost
import androidx.compose.material3.SnackbarHostState
import androidx.compose.material3.SuggestionChip
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.material3.darkColorScheme
import androidx.compose.material3.lightColorScheme
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.composed
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.PlatformTextStyle
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.withStyle
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.constraintlayout.compose.ConstraintLayout
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.findNavController
import com.bumptech.glide.integration.compose.ExperimentalGlideComposeApi
import com.bumptech.glide.integration.compose.GlideImage
import com.deva.ecommerce.R
import com.deva.ecommerce.databinding.FragmentProductDetailBinding
import com.deva.ecommerce.model.local.entity.CartItem
import com.deva.ecommerce.model.local.entity.Product
import com.deva.ecommerce.model.remote.response.ProductDetailData
import com.deva.ecommerce.ui.theme.EcommerceTheme
import com.deva.ecommerce.ui.theme.ErrorLayout
import com.google.firebase.analytics.FirebaseAnalytics
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch
import java.text.NumberFormat
import java.util.Locale
import javax.inject.Inject

@AndroidEntryPoint
class ProductDetailFragment : Fragment() {
    private var _binding: FragmentProductDetailBinding? = null
    private val binding get() = _binding!!
    private val viewModel: ProductDetailViewModel by viewModels()

    private var productDetail: ProductDetailData? = null

    @Inject
    lateinit var analytics: FirebaseAnalytics

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentProductDetailBinding.inflate(inflater, container, false)

        val view = binding.root

        val id = arguments?.getString("id")
        if (!id.isNullOrBlank()) {
            viewModel.checkIsWishlished(id)
            viewModel.getProduct(id)
        }

        binding.composeView.setContent {
            val productDetailState by viewModel.productDetailState.observeAsState()

            EcommerceTheme {
                productDetailState.let {
                    when (it) {
                        is ProductDetailState.Loading -> {
                            Box(
                                contentAlignment = Alignment.Center
                            ) {
                                CircularProgressIndicator()
                            }
                        }

                        is ProductDetailState.Success -> {
                            ProductDetailScreen(
                                product = productDetail
                                    ?: (productDetailState as ProductDetailState.Success).data.data,
                                view = view,
                            )
                        }

                        is ProductDetailState.Error -> {
                            ErrorLayout(
                                onRefreshClick = {
                                    if (!id.isNullOrBlank()) {
                                        viewModel.checkIsWishlished(id)
                                        viewModel.getProduct(id)
                                    }
                                },
                            )
                        }

                        else -> {}
                    }
                }
            }
        }

        return view
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}

@OptIn(
    ExperimentalFoundationApi::class,
    ExperimentalMaterial3Api::class,
    ExperimentalGlideComposeApi::class
)
@Composable
fun ProductDetailScreen(product: ProductDetailData, view: View) {
    val viewModel: ProductDetailViewModel = hiltViewModel()
    val locale = Locale("id", "ID")
    val formatter: NumberFormat = NumberFormat.getCurrencyInstance(locale).apply {
        maximumFractionDigits = 0
    }

    val pagerState = rememberPagerState()
    val pageCount = product.image.size

    val selectedChip = remember { mutableStateOf(0) }
    var isFavorite = viewModel.isLoved.observeAsState().value ?: false

    val snackbarHostState = remember { SnackbarHostState() }

    var selectedProductPrice by remember { mutableStateOf(product.productPrice) }

    var selectedVariantName by remember { mutableStateOf(product.productVariant[0].variantName) }

    val context = LocalContext.current

    val scope = rememberCoroutineScope()

    val productEntity = Product(
        id = product.productId,
        name = product.productName,
        price = product.productPrice + product.productVariant[0].variantPrice,
        rating = product.productRating,
        reviewCount = product.totalReview,
        ratingCount = product.totalRating,
        totalSatisfaction = product.totalSatisfaction,
        sold = product.sale,
        description = product.description,
        image = product.image[0],
        seller = product.store,
        variant = product.productVariant[0].variantName,
    )

    Scaffold(
        snackbarHost = { SnackbarHost(snackbarHostState) },
        bottomBar = {
            BottomBar(
                viewModel,
                product,
                selectedProductPrice,
                selectedVariantName,
                view,
                snackbarHostState
            )
        }
    ) {
        Column(
            modifier = Modifier
                .fillMaxSize()
                .verticalScroll(rememberScrollState())
                .padding(top = 1.dp, bottom = it.calculateBottomPadding())
        ) {
            ConstraintLayout {
                val (viewPager, pageIndicator) = createRefs()

                HorizontalPager(
                    pageCount = pageCount,
                    state = pagerState,
                    modifier = Modifier
                        .constrainAs(viewPager) {
                            top.linkTo(parent.top)
                            start.linkTo(parent.start)
                            end.linkTo(parent.end)
                        }
                        .wrapContentHeight()
                        .fillMaxWidth()
                ) { index ->
                    GlideImage(
                        model = "",
                        contentDescription = null,
                        modifier = Modifier
                            .fillMaxSize(),
                        contentScale = ContentScale.Crop,
                    ) { imageState ->
                        imageState.load(product.image[index])
                    }
                }

                Row(
                    Modifier
                        .height(50.dp)
                        .fillMaxWidth()
                        .constrainAs(pageIndicator) {
                            bottom.linkTo(viewPager.bottom)
                        },
                    horizontalArrangement = Arrangement.Center
                ) {
                    repeat(pageCount) { iteration ->
                        val color =
                            if (pagerState.currentPage == iteration) pagerSelectedColor() else borderColor()
                        Box(
                            modifier = Modifier
                                .padding(8.dp)
                                .clip(CircleShape)
                                .background(color)
                                .size(8.dp)
                        )
                    }
                }
            }

            Column(
                modifier = Modifier
                    .fillMaxWidth()
            ) {
                Row(
                    modifier = Modifier.fillMaxWidth(),
                    verticalAlignment = Alignment.CenterVertically
                ) {
                    Text(
                        text = formatter.format(selectedProductPrice),
                        fontSize = 20.sp,
                        modifier = Modifier.padding(start = 16.dp),
                        style = MaterialTheme.typography.titleMedium
                    )

                    Spacer(
                        modifier = Modifier
                            .weight(1f)
                    )

                    IconButton(onClick = {
                        val shareIntent: Intent = Intent().apply {
                            action = Intent.ACTION_SEND
                            putExtra(
                                Intent.EXTRA_TEXT,
                                "Product: ${product.productName}\n" +
                                    "Price: ${formatter.format(selectedProductPrice)}\n" +
                                    "Seller: ${product.store}\n" +
                                    "Link: http://ecommerce.deva.com/product/${product.productId}"
                            )
                            type = "text/plain"
                        }

                        context.startActivity(
                            Intent.createChooser(
                                shareIntent,
                                context.getString(R.string.share_product)
                            )
                        )
                    }) {
                        Icon(
                            imageVector = Icons.Default.Share,
                            contentDescription = null
                        )
                    }

                    IconToggleButton(
                        checked = isFavorite,
                        onCheckedChange = { value ->
                            isFavorite = value

                            if (isFavorite) {
                                viewModel.addToWishlist(productEntity)
                                scope.launch {
                                    snackbarHostState.showSnackbar(
                                        message = context.getString(R.string.added_to_wishlist),
                                        duration = SnackbarDuration.Short,
                                    )
                                }
                            } else {
                                viewModel.removeFromWishlist(productEntity)
                                scope.launch {
                                    snackbarHostState.showSnackbar(
                                        message = context.getString(R.string.removed_from_wishlist),
                                        duration = SnackbarDuration.Short
                                    )
                                }
                            }
                        },
                    ) {
                        Icon(
                            imageVector = if (isFavorite) {
                                Icons.Filled.Favorite
                            } else {
                                Icons.Default.FavoriteBorder
                            },
                            contentDescription = null
                        )
                    }
                }

                Text(
                    text = product.productName,
                    modifier = Modifier.padding(horizontal = 16.dp),
                    style = MaterialTheme.typography.bodyLarge
                )

                Row(
                    modifier = Modifier
                        .fillMaxWidth(),
                    verticalAlignment = Alignment.CenterVertically
                ) {
                    Text(
                        text = stringResource(id = R.string.sold) + " " + product.sale,
                        fontSize = 12.sp,
                        modifier = Modifier
                            .padding(start = 16.dp, end = 10.dp)
                            .wrapContentSize(Alignment.Center),

                        style = MaterialTheme.typography.bodyLarge
                    )

                    CompositionLocalProvider(LocalRippleTheme provides NoRippleTheme) {
                        SuggestionChip(
                            onClick = {
                            },
                            modifier = Modifier
                                .defaultMinSize(minHeight = 0.dp, minWidth = 0.dp)
                                .noRippleClickable {}
                                .clickable(enabled = false) {},
                            label = { RatingChipText(text = "${product.productRating} (${product.sale})") },
                            icon = {
                                Icon(
                                    painter = painterResource(R.drawable.baseline_star_24),
                                    contentDescription = null,
                                    modifier = Modifier
                                        .width(18.dp)
                                        .height(18.dp)
                                        .wrapContentHeight(Alignment.CenterVertically),
                                )
                            },
                        )
                    }
                }

                Spacer(
                    modifier = Modifier
                        .fillMaxWidth()
                        .height(1.dp)
                        .background(borderColor())
                )

                Text(
                    text = stringResource(R.string.choose_variant),
                    fontSize = 16.sp,
                    modifier = Modifier.padding(start = 16.dp, top = 12.dp),
                    style = MaterialTheme.typography.titleSmall
                )

                LazyRow(
                    modifier = Modifier
                        .padding(start = 16.dp, end = 16.dp, bottom = 8.dp)
                        .defaultMinSize(minHeight = 0.dp, minWidth = 0.dp),
                    horizontalArrangement = Arrangement.spacedBy(8.dp)
                ) {
                    itemsIndexed(product.productVariant) { index, item ->
                        InputChip(
                            selected = selectedChip.value == index,
                            onClick = {
                                selectedChip.value = index
                                product.apply {
                                    selectedProductPrice =
                                        product.productPrice + item.variantPrice
                                    selectedVariantName = item.variantName
                                }
                            },
                            label = { VariantChipText(text = item.variantName) }
                        )
                    }
                }

                Spacer(
                    modifier = Modifier
                        .fillMaxWidth()
                        .height(1.dp)
                        .background(borderColor())
                )

                Text(
                    text = stringResource(R.string.product_desc),
                    fontSize = 16.sp,
                    modifier = Modifier.padding(
                        start = 16.dp,
                        bottom = 8.dp,
                        end = 16.dp,
                        top = 12.dp
                    ),
                    style = MaterialTheme.typography.titleSmall
                )

                Text(
                    text = product.description,
                    modifier = Modifier.padding(start = 16.dp, end = 16.dp, bottom = 12.dp),
                    style = MaterialTheme.typography.bodyLarge
                )

                Spacer(
                    modifier = Modifier
                        .fillMaxWidth()
                        .height(1.dp)
                        .background(borderColor())
                        .padding(vertical = 12.dp)
                )

                Row(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(start = 16.dp, end = 16.dp),
                    verticalAlignment = Alignment.CenterVertically
                ) {
                    Text(
                        text = stringResource(R.string.product_review),
                        fontSize = 16.sp,
                        style = MaterialTheme.typography.titleSmall
                    )

                    Spacer(
                        Modifier
                            .weight(1f)
                            .background(borderColor())
                    )

                    TextButton(
                        onClick = {
                            val idBundle = bundleOf("id" to product.productId)
                            view.findNavController().navigate(
                                R.id.navigation_product_review,
                                idBundle
                            )
                        },
                        modifier = Modifier
                            .defaultMinSize(minHeight = 0.dp, minWidth = 0.dp),
                        shape = MaterialTheme.shapes.small
                    ) {
                        Text(
                            text = stringResource(R.string.see_all),
                            fontSize = 12.sp,
                            style = textStyleDisableFontPadding()
                        )
                    }
                }

                Row(
                    verticalAlignment = Alignment.CenterVertically,
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(start = 16.dp, end = 16.dp, bottom = 18.dp)
                ) {
                    Icon(
                        modifier = Modifier
                            .size(20.dp),
                        painter = painterResource(R.drawable.baseline_star_24),
                        contentDescription = null,
                    )
                    Spacer(modifier = Modifier.width(4.dp))
                    Text(
                        buildAnnotatedString {
                            withStyle(
                                style = SpanStyle(
                                    fontSize = 20.sp,
                                    fontFamily = FontFamily(Font(R.font.poppins_semibold)),
                                )
                            ) {
                                append(product.productRating.toString())
                            }
                            withStyle(
                                style = SpanStyle(
                                    fontSize = 10.sp,
                                    fontFamily = FontFamily(Font(R.font.poppins_regular)),
                                )
                            ) {
                                append("/5.0")
                            }
                        },
                        style = textStyleDisableFontPadding()
                    )
                    Spacer(modifier = Modifier.width(32.dp))
                    Column {
                        Text(
                            text = product.totalSatisfaction.toString() + stringResource(R.string.buyer_feels_satisfied),
                            fontSize = 12.sp,
                            style = MaterialTheme.typography.titleMedium
                        )
                        Text(
                            text = product.totalReview.toString() +
                                " " +
                                stringResource(R.string.rating_count) +
                                " " +
                                product.totalRating.toString() +
                                " " +
                                stringResource(R.string.review_count),
                            fontSize = 12.sp,
                            style = MaterialTheme.typography.bodyLarge
                        )
                    }
                }
            }
        }
    }
}

@SuppressLint("CoroutineCreationDuringComposition")
@Composable
fun BottomBar(
    viewModel: ProductDetailViewModel,
    product: ProductDetailData,
    selectedProductPrice: Int,
    selectedVariantName: String,
    view: View,
    snackbarHostState: SnackbarHostState

) {
    val checkIfProductExist =
        viewModel.checkIfItemExistInCart(product.productId).observeAsState().value

    val cartEntity = CartItem(
        id = product.productId,
        price = selectedProductPrice,
        image = product.image[0],
        stock = product.stock,
        variant = selectedVariantName,
        name = product.productName,
        quantity = 1,
        isSelected = false
    )

    rememberCoroutineScope().launch {
        cartEntity.quantity =
            if (checkIfProductExist == true) viewModel.getQtyById(cartEntity.id) else 1
    }

    val context = LocalContext.current
    val scope = rememberCoroutineScope()

    EcommerceTheme {
        Column(
            modifier = Modifier
                .fillMaxWidth()
                .background(if (isSystemInDarkTheme()) darkColorScheme().background else lightColorScheme().background)
        ) {
            Divider(
                modifier = Modifier
                    .fillMaxWidth()
                    .height(1.dp),
                color = borderColor(),
                thickness = 1.dp,
            )

            Row(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(horizontal = 16.dp, vertical = 8.dp),
                horizontalArrangement = Arrangement.SpaceBetween,
                verticalAlignment = Alignment.CenterVertically
            ) {
                OutlinedButton(
                    onClick = {
                        val checkoutList = mutableListOf<CartItem>()
                        checkoutList.add(cartEntity)

                        val bundle = bundleOf("product" to checkoutList)
                        view.findNavController().navigate(
                            R.id.action_navigation_product_detail_to_navigation_checkout,
                            bundle
                        )
                    },
                    modifier = Modifier
                        .weight(1f)
                        .defaultMinSize(minHeight = 0.dp, minWidth = 0.dp),

                ) {
                    Text(text = stringResource(R.string.beli_langsung))
                }

                Spacer(
                    modifier = Modifier
                        .width(16.dp)
                        .background(borderColor())
                )

                Button(
                    onClick = {
                        checkIfProductExist.let {
                            if (it == true) {
                                if (cartEntity.quantity < product.stock) {
                                    cartEntity.quantity++
                                    viewModel.updateCartQuantity(
                                        cartEntity.id,
                                        cartEntity.quantity
                                    )
                                    Log.d("TAG", "BottomBar: ${cartEntity.quantity}")
                                } else
                                    scope.launch {
                                        snackbarHostState.showSnackbar(
                                            message = context.getString(R.string.insufficient_stock),
                                            duration = SnackbarDuration.Short
                                        )
                                    }
                            } else {
                                viewModel.addToCart(cartEntity)
                                scope.launch {
                                    snackbarHostState.showSnackbar(
                                        message = context.getString(R.string.added_to_cart),
                                        duration = SnackbarDuration.Short
                                    )
                                }
                            }
                        }
                    },
                    modifier = Modifier
                        .weight(1f)
                        .defaultMinSize(minHeight = 0.dp, minWidth = 0.dp),
                ) {
                    Icon(
                        painter = painterResource(R.drawable.baseline_add_24),
                        contentDescription = null,
                        modifier = Modifier.padding(end = 4.dp)
                    )
                    Text(text = stringResource(R.string.keranjang))
                }
            }
        }
    }
}

@Composable
fun RatingChipText(text: String) {
    Text(
        text = text,
        fontSize = 12.sp,
        style = MaterialTheme.typography.bodyLarge
    )
}

@Composable
fun VariantChipText(text: String) {
    Text(
        text = text,
        fontSize = 14.sp,
        style = MaterialTheme.typography.titleSmall
    )
}

@Composable
fun textStyleDisableFontPadding(): TextStyle {
    return LocalTextStyle.current.merge(
        TextStyle(
            platformStyle = PlatformTextStyle(
                includeFontPadding = false
            ),
        )
    )
}

@Composable
fun borderColor(): Color {
    return colorResource(id = R.color.light_grey)
}

@Composable
fun pagerSelectedColor(): Color {
    return colorResource(id = R.color.purple)
}

fun Modifier.noRippleClickable(onClick: () -> Unit): Modifier = composed {
    clickable(
        indication = null,
        interactionSource = remember { MutableInteractionSource() }
    ) {
        onClick()
    }
}

private object NoRippleTheme : RippleTheme {
    @Composable
    override fun defaultColor() = Color.Unspecified

    @Composable
    override fun rippleAlpha(): RippleAlpha = RippleAlpha(0.0f, 0.0f, 0.0f, 0.0f)
}

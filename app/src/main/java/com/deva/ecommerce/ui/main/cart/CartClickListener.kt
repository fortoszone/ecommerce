package com.deva.ecommerce.ui.main.cart

import com.deva.ecommerce.model.local.entity.CartItem

interface CartClickListener {
    fun onRemoveClick(cartItem: CartItem)
    fun onAddQtyClick(cartItem: CartItem)
    fun onRemoveQtyClick(cartItem: CartItem)
    fun onCheckClick(cartItem: CartItem)
    fun onTotalPriceChanged(totalPriceFromChecked: Int)
}

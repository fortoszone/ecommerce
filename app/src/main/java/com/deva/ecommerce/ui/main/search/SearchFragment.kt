package com.deva.ecommerce.ui.main.search

import android.os.Bundle
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import com.deva.ecommerce.R
import com.deva.ecommerce.databinding.FragmentSearchBinding
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.logEvent
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class SearchFragment : Fragment() {
    private var _binding: FragmentSearchBinding? = null
    private val binding get() = _binding!!
    private val viewModel: SearchViewModel by viewModels()

    @Inject
    lateinit var analytics: FirebaseAnalytics

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentSearchBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setSearchQuery()

        binding.search.requestFocus()

        binding.search.doOnTextChanged { text, _, _, _ ->
            viewModel.isLoading.observe(viewLifecycleOwner) { isLoading ->
                if (isLoading) {
                    binding.rvSearch.visibility = View.GONE
                    binding.progressBar.visibility = View.VISIBLE
                } else {
                    binding.rvSearch.visibility = View.VISIBLE
                    binding.progressBar.visibility = View.GONE
                }
            }

            viewModel.getQueries(text.toString()).observe(viewLifecycleOwner) {
                binding.rvSearch.apply {
                    adapter = SearchAdapter(it)
                    layoutManager = LinearLayoutManager(requireContext())
                }

                if (it != null) {
                    analytics.logEvent(FirebaseAnalytics.Event.SEARCH) {
                        param(FirebaseAnalytics.Param.SEARCH_TERM, text.toString())
                    }
                }
            }
        }

        binding.search.setOnKeyListener(
            View.OnKeyListener { v, keyCode, event ->
                val query = binding.search.text.toString().trim()
                if (keyCode == KeyEvent.KEYCODE_ENTER && event.action == KeyEvent.ACTION_UP) {
                    val queryText = bundleOf("query" to query)
                    Navigation.findNavController(v)
                        .navigate(
                            R.id.action_navigation_search_to_navigation_store,
                            queryText
                        )
                    return@OnKeyListener true
                }
                false
            }
        )

        viewModel.error.observe(viewLifecycleOwner) {
            Toast.makeText(requireContext(), it, Toast.LENGTH_SHORT).show()
        }
    }

    private fun setSearchQuery() {
        val searchQuery = arguments?.getString("query")
        if (searchQuery != null) {
            binding.search.setText(searchQuery)
        }
    }
}

package com.deva.ecommerce.ui.main.search

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.deva.ecommerce.model.local.entity.Query
import com.deva.ecommerce.model.remote.ApiService
import com.deva.ecommerce.model.remote.response.SearchResponse
import dagger.hilt.android.lifecycle.HiltViewModel
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject

@HiltViewModel
class SearchViewModel @Inject constructor(
    private val apiService: ApiService,
) : ViewModel() {

    private val _queriesLiveData = MutableLiveData<List<Query>>()
    val queriesLiveData: LiveData<List<Query>> get() = _queriesLiveData

    private val _isLoading = MutableLiveData(false)
    val isLoading: LiveData<Boolean> get() = _isLoading

    private val _error = MutableLiveData<String>()
    val error: LiveData<String> get() = _error

    private val _data = MutableLiveData<List<String>>()
    val data: LiveData<List<String>> get() = _data

    fun getQueries(text: String): LiveData<List<Query>> {
        setIsLoading(true)
        apiService.search(text).enqueue(object : Callback<SearchResponse> {
            override fun onResponse(
                call: Call<SearchResponse>,
                response: Response<SearchResponse>
            ) {
                if (response.isSuccessful) {
                    Thread.sleep(1000)
                    val queries = mutableListOf<Query>()
                    response.body()!!.data.forEach {
                        val query = Query(null)
                        query.query = it
                        queries.add(query)
                        _queriesLiveData.value = queries
                        _data.value = response.body()!!.data
                    }
                    setIsLoading(false)
                }
            }

            override fun onFailure(call: Call<SearchResponse>, t: Throwable) {
                _error.value = t.message
                setIsLoading(false)
            }
        })

        return queriesLiveData
    }

    fun setIsLoading(isLoading: Boolean) {
        _isLoading.value = isLoading
    }
}

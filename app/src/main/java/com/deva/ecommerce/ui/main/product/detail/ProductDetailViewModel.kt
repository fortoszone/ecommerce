package com.deva.ecommerce.ui.main.product.detail

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.deva.ecommerce.model.local.entity.CartItem
import com.deva.ecommerce.model.local.entity.Product
import com.deva.ecommerce.model.local.room.CartDao
import com.deva.ecommerce.model.local.room.WishlistDao
import com.deva.ecommerce.model.remote.ApiService
import com.deva.ecommerce.model.remote.response.ProductDetailResponse
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import retrofit2.HttpException
import java.io.IOException
import javax.inject.Inject

@HiltViewModel
class ProductDetailViewModel @Inject constructor(
    private val apiService: ApiService,
    private val wishlistDao: WishlistDao,
    private val cartDao: CartDao
) : ViewModel() {
    private val _data = MutableLiveData<ProductDetailResponse>()
    val data: MutableLiveData<ProductDetailResponse> = _data

    private val _isLoved = MutableLiveData<Boolean>()
    val isLoved: LiveData<Boolean> = _isLoved

    private fun setIsLoved(isLoved: Boolean) {
        _isLoved.value = isLoved
    }

    private val _productDetailState = MutableLiveData<ProductDetailState>()
    val productDetailState: LiveData<ProductDetailState> = _productDetailState

    fun getProduct(id: String) {
        viewModelScope.launch {
            _productDetailState.value = ProductDetailState.Loading
            delay(1000)
            try {
                val response = apiService.getProductDetail(id)
                if (response.isSuccessful) {
                    _productDetailState.value = ProductDetailState.Success(response.body()!!)
                    _data.value = response.body()
                } else {
                    _productDetailState.value = ProductDetailState.Error(
                        response.message()
                    )
                }
            } catch (e: HttpException) {
                _productDetailState.value = ProductDetailState.Error(
                    e.message ?: ""
                )
            } catch (e: IOException) {
                _productDetailState.value = ProductDetailState.Error(errorMessage = e.message ?: "")
            }
        }
    }

    fun addToWishlist(product: Product) {
        viewModelScope.launch(Dispatchers.Default) {
            wishlistDao.addToWishlist(product)
        }
        setIsLoved(true)
    }

    fun removeFromWishlist(product: Product) {
        viewModelScope.launch(Dispatchers.Default) {
            wishlistDao.removeFromWishlist(product)
        }
        setIsLoved(false)
    }

    fun checkIsWishlished(productId: String) {
        viewModelScope.launch(Dispatchers.Default) {
            _isLoved.postValue(wishlistDao.checkWishlistIsFavorite(productId))
        }
    }

    fun addToCart(productEntity: CartItem) {
        viewModelScope.launch(Dispatchers.Default) {
            cartDao.insertCartItem(productEntity)
        }
    }

    fun checkIfItemExistInCart(id: String): LiveData<Boolean> {
        return cartDao.checkCartId(id)
    }

    fun updateCartQuantity(id: String, quantity: Int) {
        viewModelScope.launch {
            cartDao.updateCartItemQuantity(id, quantity)
        }
    }

    suspend fun getQtyById(id: String): Int {
        return cartDao.getQuantityById(id)
    }
}

sealed class ProductDetailState {
    object Loading : ProductDetailState()
    data class Success(val data: ProductDetailResponse) : ProductDetailState()
    data class Error(val errorMessage: String) : ProductDetailState()
}

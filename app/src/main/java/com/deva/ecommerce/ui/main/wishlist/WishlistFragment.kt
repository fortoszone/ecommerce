package com.deva.ecommerce.ui.main.wishlist

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.GridLayoutManager
import com.deva.ecommerce.R
import com.deva.ecommerce.databinding.FragmentWishlistBinding
import com.deva.ecommerce.model.local.entity.Product
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.analytics.FirebaseAnalytics
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class WishlistFragment : Fragment() {
    private var _binding: FragmentWishlistBinding? = null
    private val binding get() = _binding!!
    private val viewModel: WishlistViewModel by viewModels()

    @Inject
    lateinit var analytics: FirebaseAnalytics

    private var onWishlistClickListener = object : OnWishlistClickListener {
        override fun onRemoveClick(product: Product) {
            viewModel.deleteFromWishlist(product)
            Snackbar.make(
                requireView(),
                R.string.removed_from_wishlist,
                Snackbar.LENGTH_SHORT
            ).show()

            wishlistAnalytics(product, "btn_wishlist_remove_clicked")
        }

        override fun onAddToCart(product: Product) {
            viewModel.addToCart(product)
            Snackbar.make(
                requireView(),
                R.string.added_to_cart,
                Snackbar.LENGTH_SHORT
            ).show()
            wishlistAnalytics(product, "btn_wishlist_add_to_cart_clicked")
        }
    }

    private fun wishlistAnalytics(product: Product, s: String) {
        val bundle = Bundle().apply {
            putString("product_id", product.id)
            putString("product_name", product.name)
            putString("product_price", product.price.toString())
        }

        analytics.logEvent(s, bundle)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentWishlistBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setWishlist()

        binding.toggleProduct.setOnClickListener {
            viewModel.toggleLayout()
            analytics.logEvent("btn_wishlist_toggle_layout_clicked", null)
        }
    }

    @SuppressLint("SetTextI18n")
    private fun setWishlist() {
        viewModel.getWishlistedProduct().observe(viewLifecycleOwner) { products ->
            if (products.isNotEmpty()) {
                binding.rvWishlistProduct.visibility = View.VISIBLE
                binding.llWishlistHeader.visibility = View.VISIBLE
                binding.error.error.visibility = View.GONE

                binding.wishlistCounter.text = "${products.size} barang"

                viewModel.isGrid.observe(viewLifecycleOwner) {
                    binding.rvWishlistProduct.apply {
                        if (it) {
                            val gridAdapter = WishlistAdapterGrid(onWishlistClickListener)
                            adapter = gridAdapter
                            gridAdapter.setData(products)
                            layoutManager = GridLayoutManager(requireContext(), 2)
                            binding.toggleProduct.setImageResource(R.drawable.baseline_format_list_bulleted_24)
                        } else {
                            val listAdapter = WishlistAdapter(onWishlistClickListener)
                            adapter = listAdapter
                            listAdapter.setData(products)
                            layoutManager = GridLayoutManager(requireContext(), 1)
                            binding.toggleProduct.setImageResource(R.drawable.baseline_grid_view_24)
                        }
                    }
                }
            } else {
                binding.rvWishlistProduct.visibility = View.GONE
                binding.llWishlistHeader.visibility = View.GONE
                binding.error.error.visibility = View.VISIBLE
                binding.error.btnRefresh.visibility = View.GONE
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}

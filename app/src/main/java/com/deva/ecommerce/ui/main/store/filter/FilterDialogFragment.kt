package com.deva.ecommerce.ui.main.store.filter

import android.content.res.Configuration
import android.os.Bundle
import android.view.ContextThemeWrapper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.res.ResourcesCompat
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import com.deva.ecommerce.R
import com.deva.ecommerce.databinding.BottomSheetFilterBinding
import com.deva.ecommerce.ui.main.store.FilterState
import com.deva.ecommerce.ui.main.store.SortCategory
import com.deva.ecommerce.ui.main.store.StoreViewModel
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.google.android.material.elevation.SurfaceColors
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class FilterDialogFragment : BottomSheetDialogFragment() {
    private var _binding: BottomSheetFilterBinding? = null
    private val binding get() = _binding!!
    private val viewModel: StoreViewModel by viewModels({ requireParentFragment() })

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val contextThemeWrapper =
            ContextThemeWrapper(requireActivity(), R.style.BottomSheetDialogTheme)
        _binding = BottomSheetFilterBinding.inflate(
            inflater.cloneInContext(contextThemeWrapper),
            container,
            false
        )

        when (resources.configuration.uiMode and Configuration.UI_MODE_NIGHT_MASK) {
            Configuration.UI_MODE_NIGHT_YES ->
                requireActivity().window.navigationBarColor =
                    SurfaceColors.SURFACE_2.getColor(requireContext())

            else -> requireActivity().window.navigationBarColor = ResourcesCompat.getColor(
                resources,
                R.color.white,
                null
            )
        }

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setInitState()

        binding.btnReset.setOnClickListener {
            resetFilter()
            viewModel.resetFilter()
        }

        binding.btnFinishFilter.setOnClickListener {
            setProductFilter()
        }
    }

    private fun resetFilter() {
        binding.chipGroupCategory.clearCheck()
        binding.chipGroupSortBy.clearCheck()

        binding.apply {
            chipGroupSortBy.clearCheck()
            chipGroupCategory.clearCheck()

            lowestPrice.clearFocus()
            lowestPrice.text = null

            highestPrice.clearFocus()
            highestPrice.text = null
        }
    }

    private fun setInitState() {
        viewModel.filterSelectedState.observe(viewLifecycleOwner) { filterState: FilterState? ->
            binding.btnReset.isVisible = filterState != null

            filterState?.let { state ->
                state.sort?.let {
                    when (it) {
                        SortCategory.Review -> binding.chipReview.isChecked = false
                        SortCategory.Sale -> binding.chipSold.isChecked = false
                        SortCategory.LowestPrice -> binding.chipLowestPrice.isChecked = false
                        SortCategory.HighestPrice -> binding.chipHighestPrice.isChecked = false
                    }
                }

                state.category?.let {
                    when (it) {
                        "Asus" -> binding.chipAsus.isChecked = false
                        "Lenovo" -> binding.chipLenovo.isChecked = false
                        "Apple" -> binding.chipApple.isChecked = false
                        "Dell" -> binding.chipDell.isChecked = false
                    }
                }

                state.lowestPrice?.let {
                    binding.lowestPrice.setText(it.toString())
                }

                state.highestPrice?.let {
                    binding.highestPrice.setText(it.toString())
                }
            }
        }
    }

    private fun setProductFilter() {
        val chosenSortCategory: SortCategory? = when (binding.chipGroupSortBy.checkedChipId) {
            binding.chipReview.id -> SortCategory.Review
            binding.chipSold.id -> SortCategory.Sale
            binding.chipLowestPrice.id -> SortCategory.LowestPrice
            binding.chipHighestPrice.id -> SortCategory.HighestPrice
            else -> null
        }

        val chosenBrand: String? = when (binding.chipGroupCategory.checkedChipId) {
            binding.chipAsus.id -> "Asus"
            binding.chipLenovo.id -> "Lenovo"
            binding.chipApple.id -> "Apple"
            binding.chipDell.id -> "Dell"
            else -> null
        }

        val lowestPrice: Int? = if (binding.lowestPrice.text.toString().isBlank()) {
            null
        } else {
            binding.lowestPrice.text.toString().toInt()
        }

        val highestPrice: Int? = if (binding.highestPrice.text.toString().isBlank()) {
            null
        } else {
            binding.highestPrice.text.toString().toInt()
        }

        viewModel.setFilter(
            chosenSortCategory,
            chosenBrand,
            lowestPrice,
            highestPrice
        )
        dismiss()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    companion object {
        const val TAG = "ModalBottomSheet"
    }
}

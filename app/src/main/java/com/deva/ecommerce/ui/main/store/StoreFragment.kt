package com.deva.ecommerce.ui.main.store

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.Navigation
import androidx.paging.CombinedLoadStates
import androidx.paging.LoadState
import androidx.recyclerview.widget.GridLayoutManager
import com.deva.ecommerce.R
import com.deva.ecommerce.databinding.FragmentStoreBinding
import com.deva.ecommerce.ui.main.store.filter.FilterDialogFragment
import com.deva.ecommerce.ui.main.store.paging.LoadStateAdapter
import com.deva.ecommerce.ui.main.store.paging.LoadStateAdapterGrid
import com.deva.ecommerce.ui.main.store.paging.ProductPagingAdapter
import com.deva.ecommerce.ui.main.store.paging.ProductPagingAdapterGrid
import com.google.android.material.chip.Chip
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.logEvent
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import retrofit2.HttpException
import java.io.IOException
import javax.inject.Inject

@AndroidEntryPoint
class StoreFragment : Fragment() {
    @Inject
    lateinit var analytics: FirebaseAnalytics
    private var _binding: FragmentStoreBinding? = null
    private val binding: FragmentStoreBinding
        get() = _binding!!

    private val viewModel: StoreViewModel by viewModels()
    private val pagingAdapter =
        ProductPagingAdapter(ProductPagingAdapter.diffCallBack)
    private val pagingAdapterGrid =
        ProductPagingAdapterGrid(ProductPagingAdapterGrid.diffCallBack)
    private val loadStateAdapter = LoadStateAdapter(pagingAdapter)
    private val loadStateAdapterGrid = LoadStateAdapterGrid(pagingAdapterGrid)
    private val concatAdapterGrid = pagingAdapterGrid.withLoadStateHeaderAndFooter(
        header = loadStateAdapterGrid,
        footer = loadStateAdapterGrid
    )

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentStoreBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setProduct()
        setSearchQuery()
        setFilterState()
        setLoadState()

        binding.toggleProduct.setOnClickListener {
            viewModel.toggleLayout()
        }

        binding.btnChipFilter.setOnClickListener {
            val modalBottomSheet = FilterDialogFragment()
            modalBottomSheet.show(childFragmentManager, FilterDialogFragment.TAG)
        }

        binding.search.onFocusChangeListener = View.OnFocusChangeListener { _, hasFocus ->
            if (hasFocus) {
                val query = bundleOf("query" to binding.search.text.toString())
                if (!query.isEmpty) {
                    Navigation.findNavController(view)
                        .navigate(R.id.action_navigation_store_to_navigation_search, query)
                } else
                    Navigation.findNavController(view)
                        .navigate(R.id.action_navigation_store_to_navigation_search)
            }
        }

        binding.swipeRefresh.apply {
            setOnRefreshListener {
                viewModel.isGrid.observe(viewLifecycleOwner) { isGrid ->
                    if (isGrid) {
                        pagingAdapterGrid.refresh()
                    } else {
                        pagingAdapter.refresh()
                    }
                }
                isRefreshing = false
                binding.error.error.isVisible = false
            }
        }
    }

    private fun setLoadState() {
        viewModel.isGrid.observe(viewLifecycleOwner) { isGrid ->
            viewLifecycleOwner.lifecycleScope.launch {
                if (isGrid) {
                    pagingAdapterGrid.loadStateFlow.collectLatest { loadState: CombinedLoadStates ->
                        binding.listSkeleton.listSkeleton.visibility = View.GONE
                        binding.gridSkeleton.gridSkeleton.isVisible =
                            loadState.refresh is LoadState.Loading

                        binding.rvProduct.isVisible = loadState.refresh is LoadState.NotLoading
                        binding.llFilter.isVisible = loadState.refresh is LoadState.NotLoading

                        if (loadState.refresh is LoadState.Error) {
                            val errorState = loadState.refresh as LoadState.Error
                            val error = errorState.error
                            handleError(error)
                        }
                    }
                } else {
                    pagingAdapter.loadStateFlow.collectLatest { loadState: CombinedLoadStates ->
                        binding.gridSkeleton.gridSkeleton.visibility = View.GONE
                        binding.listSkeleton.listSkeleton.isVisible =
                            loadState.refresh is LoadState.Loading

                        binding.rvProduct.isVisible = loadState.refresh is LoadState.NotLoading
                        binding.llFilter.isVisible = loadState.refresh is LoadState.NotLoading

                        if (loadState.refresh is LoadState.Error) {
                            val errorState = loadState.refresh as LoadState.Error
                            val error = errorState.error
                            handleError(error)
                        }
                    }
                }
            }
        }
    }

    private fun setFilterState() {
        viewModel.filterSelectedState.observe(viewLifecycleOwner) { filterState: FilterState? ->
            binding.filterChipGroup.removeAllViews()
            filterState?.let { state ->
                state.sort?.let {
                    val newChip = addChip(getString(it.displayedName))
                    binding.filterChipGroup.addView(newChip)
                }

                state.category?.let {
                    val newChip = addChip(it)
                    binding.filterChipGroup.addView(newChip)
                }

                state.lowestPrice?.let {
                    val newChip = addChip("< $it")
                    binding.filterChipGroup.addView(newChip)
                }

                state.highestPrice?.let {
                    val newChip = addChip("> $it")
                    binding.filterChipGroup.addView(newChip)
                }
            }
        }
    }

    private fun setProduct() {
        viewModel.isGrid.observe(viewLifecycleOwner) { value ->
            if (value) {
                binding.toggleProduct.setImageResource(R.drawable.baseline_format_list_bulleted_24)
                binding.rvProduct.apply {
                    val gridLayoutManager = GridLayoutManager(requireContext(), 2)
                    layoutManager = gridLayoutManager
                    gridLayoutManager.scrollToPosition(0)

                    gridLayoutManager.spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
                        override fun getSpanSize(position: Int): Int {
                            return if (
                                position == concatAdapterGrid.itemCount - 1 &&
                                loadStateAdapterGrid.itemCount > 0
                            ) {
                                2
                            } else {
                                1
                            }
                        }
                    }

                    adapter = pagingAdapterGrid.withLoadStateFooter(loadStateAdapterGrid)

                    lifecycleScope.launch {
                        viewModel.pager.collectLatest { pagingData ->
                            pagingAdapterGrid.submitData(pagingData)
                        }
                    }

                    binding.rvProduct.scrollToPosition(0)

                    val bundle = Bundle().apply {
                        pagingAdapterGrid.snapshot().items.forEach {
                            putString(FirebaseAnalytics.Param.ITEM_ID, it.id)
                            putString(FirebaseAnalytics.Param.ITEM_NAME, it.productName)
                            putInt(FirebaseAnalytics.Param.PRICE, it.productPrice)
                            putString(FirebaseAnalytics.Param.ITEM_BRAND, it.brand)
                        }
                    }

                    analytics.logEvent(FirebaseAnalytics.Event.VIEW_ITEM_LIST) {
                        param(FirebaseAnalytics.Param.ITEMS, arrayOf(bundle))
                    }
                }
            } else {
                binding.toggleProduct.setImageResource(R.drawable.baseline_grid_view_24)
                binding.rvProduct.apply {
                    val gridLayoutManager = GridLayoutManager(requireContext(), 1)
                    gridLayoutManager.scrollToPosition(0)
                    layoutManager = gridLayoutManager
                    adapter = pagingAdapter.withLoadStateFooter(loadStateAdapter)

                    lifecycleScope.launch {
                        viewModel.pager.collectLatest { pagingData ->
                            pagingAdapter.submitData(pagingData)
                        }
                    }

                    binding.rvProduct.scrollToPosition(0)

                    val bundle = Bundle().apply {
                        pagingAdapter.snapshot().items.forEach {
                            putString(FirebaseAnalytics.Param.ITEM_ID, it.id)
                            putString(FirebaseAnalytics.Param.ITEM_NAME, it.productName)
                            putInt(FirebaseAnalytics.Param.PRICE, it.productPrice)
                            putString(FirebaseAnalytics.Param.ITEM_BRAND, it.brand)
                        }
                    }

                    analytics.logEvent(FirebaseAnalytics.Event.VIEW_ITEM_LIST) {
                        param(FirebaseAnalytics.Param.ITEMS, arrayOf(bundle))
                    }
                }
            }
        }
    }

    private fun setSearchQuery() {
        val searchQuery = arguments?.getString("query")
        if (searchQuery != null) {
            binding.search.setText(searchQuery)
            viewModel.searchItems(searchQuery)
        }

        viewModel.searchedProductName.observe(viewLifecycleOwner) {
            if (it.isNotBlank()) {
                binding.search.setText(it)
            } else {
                binding.search.text = null
                binding.tilSearch.hint = getString(R.string.search)
            }
        }
    }

    private fun addChip(chipText: String): Chip {
        val chip = Chip(requireContext())
        chip.text = chipText
        chip.isCheckable = false
        chip.isClickable = false
        return chip
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun handleError(error: Throwable) {
        binding.error.btnRefresh.setOnClickListener {
            viewModel.isGrid.observe(viewLifecycleOwner) { isGrid ->
                if (isGrid) {
                    pagingAdapterGrid.retry()
                } else {
                    pagingAdapter.retry()
                }
                binding.error.error.isVisible = false
            }
        }

        when (error) {
            is HttpException -> {
                binding.error.error.visibility = View.VISIBLE
                binding.rvProduct.visibility = View.GONE
                binding.error.btnRefresh.text = getString(R.string.refresh)
                binding.error.apply {
                    tvErrorTitle.text = error.code().toString()
                    tvErrorDesc.text = error.message()
                }
            }

            is IOException -> {
                binding.error.error.visibility = View.VISIBLE
                binding.rvProduct.visibility = View.GONE
                binding.error.btnRefresh.text = getString(R.string.refresh)
                binding.error.apply {
                    tvErrorTitle.text = getString(R.string.connection)
                    tvErrorDesc.text = getString(R.string.your_connection_is_unavailable)
                }
            }

            else -> {
                binding.error.error.visibility = View.VISIBLE
                binding.rvProduct.visibility = View.GONE
                binding.error.apply {
                    tvErrorTitle.text = getString(R.string.empty)
                    tvErrorDesc.text = getString(R.string.your_requested_data_is_unavailable)
                }

                binding.error.btnRefresh.text = getString(R.string.reset)
                binding.error.btnRefresh.setOnClickListener {
                    viewModel.resetFilter()
                    arguments?.clear()
                    binding.search.text = null
                    setProduct()
                    arguments?.remove("query")
                    setFilterState()
                    setSearchQuery()

                    Log.d("TAG", "handleError: ${viewModel.filterSelectedState.value}")

                    viewModel.isGrid.observe(viewLifecycleOwner) { isGrid ->
                        if (isGrid) {
                            pagingAdapterGrid.retry()
                        } else {
                            pagingAdapter.retry()
                        }
                        binding.error.error.isVisible = false
                    }
                }
            }
        }
    }
}

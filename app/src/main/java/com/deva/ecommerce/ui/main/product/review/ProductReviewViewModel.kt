package com.deva.ecommerce.ui.main.product.review

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.deva.ecommerce.model.remote.ApiService
import com.deva.ecommerce.model.remote.response.ProductReviewResponse
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import retrofit2.HttpException
import java.io.IOException
import javax.inject.Inject

@HiltViewModel
class ProductReviewViewModel @Inject constructor(private val apiService: ApiService) : ViewModel() {
    private val _isLoading = MutableLiveData<Boolean>()
    val isLoading: LiveData<Boolean> = _isLoading

    private val _productReviewState = MutableLiveData<ProductReviewState>()
    val productReviewState: LiveData<ProductReviewState> = _productReviewState

    private val _data = MutableLiveData<ProductReviewResponse>()
    val data: LiveData<ProductReviewResponse> = _data

    private val _error = MutableLiveData<ProductReviewResponse>()
    val error: LiveData<ProductReviewResponse> = _error

    fun getProductReview(id: String) {
        viewModelScope.launch {
            _productReviewState.value = ProductReviewState.Loading
            delay(1000)
            try {
                val response = apiService.getProductReview(id)
                if (response.isSuccessful) {
                    _productReviewState.value = ProductReviewState.Success(response.body()!!)
                    _data.value = response.body()
                } else {
                    _productReviewState.value = ProductReviewState.Error(response.message())
                }
            } catch (e: HttpException) {
                _productReviewState.value = ProductReviewState.Error(e.message ?: "")
            } catch (e: IOException) {
                _productReviewState.value = ProductReviewState.Error(e.message ?: "")
            }
        }
    }
}

sealed class ProductReviewState {
    data class Success(val data: ProductReviewResponse) : ProductReviewState()
    data class Error(val errorMessage: String) : ProductReviewState()
    object Loading : ProductReviewState()
}

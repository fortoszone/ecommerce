package com.deva.ecommerce.ui.main.notification

import com.deva.ecommerce.model.local.entity.Notification

interface NotificationClickListener {
    fun onNotificationClick(notification: Notification)
}

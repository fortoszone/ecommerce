package com.deva.ecommerce.ui.prelogin.login

import android.util.Patterns
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.deva.ecommerce.model.remote.ApiService
import com.deva.ecommerce.model.remote.request.UserRequest
import com.deva.ecommerce.model.remote.response.LoginResponse
import com.deva.ecommerce.preference.AuthPreference
import com.deva.ecommerce.preference.SessionManager
import com.google.gson.Gson
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import retrofit2.HttpException
import java.io.IOException
import javax.inject.Inject

@HiltViewModel
class LoginViewModel @Inject constructor(
    private val sessionManager: SessionManager,
    private val authPreference: AuthPreference,
    private val apiService: ApiService,
) : ViewModel() {
    private var _emailUser = MutableLiveData<Boolean>()
    val emailUser: LiveData<Boolean> get() = _emailUser

    private var _passwordUser = MutableLiveData<Boolean>()
    val passwordUser: LiveData<Boolean> get() = _passwordUser

    private var _isFromValid = MediatorLiveData<Boolean>()
    val isFormValid: LiveData<Boolean> get() = _isFromValid

    private var _data = MutableLiveData<LoginResponse>()
    val data: MutableLiveData<LoginResponse> = _data

    private val _error = MutableLiveData<String>()
    val error: LiveData<String> = _error

    private val _isLoading = MutableLiveData<Boolean>()
    val isLoading: LiveData<Boolean> = _isLoading

    private fun setIsLoading(isLoading: Boolean) {
        _isLoading.value = isLoading
    }

    init {
        _isFromValid.addSource(_emailUser) {
            _isFromValid.value = it && _passwordUser.value == true
        }
        _isFromValid.addSource(_passwordUser) {
            _isFromValid.value = it && _emailUser.value == true
        }
    }

    fun checkEmail(email: String) {
        _emailUser.value = Patterns.EMAIL_ADDRESS.matcher(email).matches() && email.isNotEmpty()
    }

    fun checkPassword(pwd: String) {
        _passwordUser.value = pwd.length >= 8 && pwd.isNotEmpty()
    }

    fun login(email: String, password: String, firebaseToken: String) {
        setIsLoading(true)
        val request = UserRequest(email, password, firebaseToken)

        viewModelScope.launch {
            try {
                val response = apiService.login(request)
                if (response.isSuccessful) {
                    _data.value = response.body()
                    sessionManager.saveAccessToken(data.value?.data?.accessToken)
                    sessionManager.saveRefreshToken(data.value?.data?.refreshToken)
                    sessionManager.saveUserName(data.value?.data?.userName)
                    authPreference.saveLoginStatus(true)
                    authPreference.saveNameAndImageFilled(true)
                } else {
                    val errorResponse = Gson().fromJson(
                        response.errorBody()?.string(),
                        LoginResponse::class.java
                    )
                    _error.value = errorResponse.message
                }
                setIsLoading(false)
            } catch (e: HttpException) {
                _error.value = e.message()
                setIsLoading(false)
            } catch (e: IOException) {
                _error.value = e.message
                setIsLoading(false)
            }
        }
    }
}

package com.deva.ecommerce.ui.main.checkout

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.deva.ecommerce.model.local.room.CartDao
import com.deva.ecommerce.model.remote.ApiService
import com.deva.ecommerce.model.remote.request.FulfillmentRequest
import com.deva.ecommerce.model.remote.response.FulfillmentResponse
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject

@HiltViewModel
class CheckoutViewModel @Inject constructor(
    private val apiService: ApiService,
    private val cartDao: CartDao,
) : ViewModel() {
    private val _data = MutableLiveData<FulfillmentResponse>()
    val data: MutableLiveData<FulfillmentResponse> = _data

    private val _dataError = MutableLiveData<FulfillmentResponse>()
    val dataError: MutableLiveData<FulfillmentResponse> = _dataError

    private val _isLoading = MutableLiveData<Boolean>()
    val isLoading: MutableLiveData<Boolean> = _isLoading

    private val _isPaymentMethodSelected = MutableLiveData<Boolean>()
    val isPaymentMethodSelected: MutableLiveData<Boolean> = _isPaymentMethodSelected

    fun setIsPaymentMethodSelected(isPaymentMethodSelected: Boolean) {
        _isPaymentMethodSelected.value = isPaymentMethodSelected
    }

    fun setIsLoading(isLoading: Boolean) {
        _isLoading.value = isLoading
    }

    fun getFulfillment(request: FulfillmentRequest) {
        setIsLoading(true)
        apiService.getFulfillment(request).enqueue(object : Callback<FulfillmentResponse> {
            override fun onResponse(
                call: Call<FulfillmentResponse>,
                response: Response<FulfillmentResponse>
            ) {
                if (response.isSuccessful) {
                    data.value = response.body()
                    setIsLoading(false)
                } else {
                    dataError.value = response.body()
                    setIsLoading(false)
                }
            }

            override fun onFailure(call: Call<FulfillmentResponse>, t: Throwable) {
                /*dataError.value = FulfillmentResponse(
                    code = 500,
                    data = null,
                    message = t.message.toString()
                )*/
                setIsLoading(false)
            }
        })
    }

    fun deleteCartItem(id: String) {
        viewModelScope.launch {
            cartDao.deleteCartById(id)
        }
    }

    fun updateCartQuantity(id: String, quantity: Int) {
        viewModelScope.launch {
            cartDao.updateCartItemQuantity(id, quantity)
        }
    }
}

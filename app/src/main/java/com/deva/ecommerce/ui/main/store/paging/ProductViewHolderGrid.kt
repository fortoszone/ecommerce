package com.deva.ecommerce.ui.main.store.paging

import androidx.core.os.bundleOf
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.deva.ecommerce.R
import com.deva.ecommerce.databinding.RowProductGridBinding
import com.deva.ecommerce.model.remote.response.ProductItems
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.logEvent
import java.text.NumberFormat
import java.util.Locale

class ProductViewHolderGrid(
    private val binding: RowProductGridBinding,
) :
    RecyclerView.ViewHolder(binding.root) {
    private val analytics: FirebaseAnalytics = FirebaseAnalytics.getInstance(itemView.context)
    private val locale = Locale("id", "ID")
    private val formatter: NumberFormat = NumberFormat.getCurrencyInstance(locale).apply {
        maximumFractionDigits = 0
    }

    fun bind(product: ProductItems) {
        Glide.with(itemView).load(product.image)
            .into(binding.ivProduct)
        binding.tvProductName.text = product.productName
        binding.tvProductPrice.text = formatter.format(product.productPrice)
        binding.tvProductSeller.text = product.store
        binding.tvProductRating.text = product.productRating.toString()
        binding.tvProductSold.text = product.sale.toString()

        binding.product.setOnClickListener {
            val id = bundleOf("id" to product.id)
            Navigation.findNavController(it)
                .navigate(
                    R.id.action_navigation_store_to_navigation_product_detail,
                    id
                )

            analytics.logEvent(FirebaseAnalytics.Event.SELECT_ITEM) {
                param(FirebaseAnalytics.Param.ITEM_ID, product.id)
                param(FirebaseAnalytics.Param.ITEM_NAME, product.productName)
                param(FirebaseAnalytics.Param.PRICE, product.productPrice.toDouble())
                param(FirebaseAnalytics.Param.ITEM_BRAND, product.brand)
            }
        }
    }
}

package com.deva.ecommerce.ui.main.wishlist

import com.deva.ecommerce.model.local.entity.Product

interface OnWishlistClickListener {
    fun onRemoveClick(product: Product)
    fun onAddToCart(product: Product)
}

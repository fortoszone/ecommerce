package com.deva.ecommerce.ui.main.fulfillment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.deva.ecommerce.R
import com.deva.ecommerce.databinding.FragmentFulfillmentBinding
import com.deva.ecommerce.model.remote.response.FulfillmentDetail
import com.google.firebase.analytics.FirebaseAnalytics
import dagger.hilt.android.AndroidEntryPoint
import java.text.NumberFormat
import java.util.Locale
import javax.inject.Inject

@AndroidEntryPoint
class FulfillmentFragment : Fragment() {
    private var _binding: FragmentFulfillmentBinding? = null
    private val binding get() = _binding!!
    private val viewModel: FulfillmentViewModel by viewModels()
    private val locale = Locale("id", "ID")
    private val formatter: NumberFormat = NumberFormat.getCurrencyInstance(locale).apply {
        maximumFractionDigits = 0
    }

    @Inject
    lateinit var analytics: FirebaseAnalytics

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentFulfillmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setTransactionDetail()
    }

    private fun setTransactionDetail() {
        val transaction = arguments?.getParcelable<FulfillmentDetail>("transactionItem").apply {
            if (this != null) {
                binding.tvTransactionId.text = this.id
                binding.tvTransactionDate.text = this.date
                binding.tvTransactionMethod.text = this.payment
                binding.tvTransactionTotal.text = formatter.format(this.total)
                binding.tvTransactionTime.text = this.time
                if (this.status) {
                    binding.tvTransactionStatus.text = getString(R.string.succeed)
                } else {
                    binding.tvTransactionStatus.text = getString(R.string.failed)
                }
                binding.ratingBar.rating = if (this.rating != null) this.rating.toFloat() else 0f
                binding.etReview.setText(this.review)
            }
        }

        arguments?.getParcelable<FulfillmentDetail>("fulfillment").apply {
            if (this != null) {
                binding.tvTransactionId.text = this.id
                binding.tvTransactionDate.text = this.date
                binding.tvTransactionMethod.text = this.payment
                binding.tvTransactionTotal.text = formatter.format(this.total)
                binding.tvTransactionTime.text = this.time
                if (this.status) {
                    binding.tvTransactionStatus.text = getString(R.string.succeed)
                } else {
                    binding.tvTransactionStatus.text = getString(R.string.failed)
                }
            }

            binding.btnFinish.setOnClickListener {
                val review = binding.etReview.text.toString().trim()
                val rating = binding.ratingBar.rating.toInt()
                if (review.isNotEmpty() || rating.toString().isNotEmpty()) {
                    if (transaction != null) {
                        viewModel.rating(transaction.id, rating, review)
                    } else {
                        viewModel.rating(this?.id.toString(), rating, review)
                    }
                    findNavController().navigateUp()
                }
                analytics.logEvent("btn_fulfillment_finish_clicked", null)
            }
        }

        viewModel.message.observe(viewLifecycleOwner) { message ->
            Toast.makeText(requireContext(), message.message, Toast.LENGTH_SHORT).show()
        }
    }
}

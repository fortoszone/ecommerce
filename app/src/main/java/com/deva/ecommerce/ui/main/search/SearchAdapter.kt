package com.deva.ecommerce.ui.main.search

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.deva.ecommerce.R
import com.deva.ecommerce.databinding.RowSearchBinding
import com.deva.ecommerce.model.local.entity.Query

class SearchAdapter(private val queries: List<Query>) :
    RecyclerView.Adapter<SearchAdapter.SearchViewHolder>() {
    class SearchViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val binding = RowSearchBinding.bind(view)
        private val queryText = binding.tvQuery

        fun bind(query: Query) {
            this.queryText.text = query.query
            binding.query.setOnClickListener {
                val queryText = bundleOf("query" to query.query)
                Navigation.findNavController(it)
                    .navigate(
                        R.id.action_navigation_search_to_navigation_store,
                        queryText
                    )
            }
        }
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): SearchViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(
            R.layout.row_search,
            parent,
            false
        )

        return SearchViewHolder(view)
    }

    override fun onBindViewHolder(holder: SearchViewHolder, position: Int) {
        holder.bind(queries[position])
    }

    override fun getItemCount(): Int {
        return queries.size
    }
}

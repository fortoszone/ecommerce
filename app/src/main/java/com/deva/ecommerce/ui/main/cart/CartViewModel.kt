package com.deva.ecommerce.ui.main.cart

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.deva.ecommerce.model.local.entity.CartItem
import com.deva.ecommerce.model.local.room.CartDao
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class CartViewModel @Inject constructor(private val cartDao: CartDao) : ViewModel() {
    val isEveryProductSelected = cartDao.checkIsEveryProductSelected()
    val isAnyProductSelected = cartDao.checkIsAnyProductSelected()
    private val _isAProductSelected = MutableLiveData<Boolean>()
    val isAProductSelected: LiveData<Boolean> = _isAProductSelected

    fun getCartItem() = cartDao.getAllCartItems()

    fun removeCartItem(cartItem: CartItem) {
        viewModelScope.launch {
            cartDao.deleteCartItem(cartItem)
        }
    }

    fun addCartItem(cartItem: CartItem) {
        viewModelScope.launch {
            cartDao.insertCartItem(cartItem)
        }
    }

    fun updateSelectedAllProduct(checkedAll: Boolean) {
        viewModelScope.launch {
            cartDao.updateAllSelectedProduct(checkedAll)
        }
    }

    fun updateCart(newCartProductState: CartItem) {
        viewModelScope.launch {
            cartDao.updateCartItem(newCartProductState)
        }
    }

    fun deleteAllSelectedProduct() {
        viewModelScope.launch {
            cartDao.deleteAllSelectedProduct()
        }
    }

    fun updateCartQuantity(id: String, quantity: Int) {
        viewModelScope.launch {
            cartDao.updateCartItemQuantity(id, quantity)
        }
    }
}

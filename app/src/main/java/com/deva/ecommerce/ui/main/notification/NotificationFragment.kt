package com.deva.ecommerce.ui.main.notification

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.deva.ecommerce.databinding.FragmentNotificationBinding
import com.deva.ecommerce.model.local.entity.Notification
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.logEvent
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class NotificationFragment : Fragment() {
    private var _binding: FragmentNotificationBinding? = null
    private val binding get() = _binding!!
    private val viewModel: NotificationViewModel by viewModels()

    @Inject
    lateinit var analytics: FirebaseAnalytics

    private var onNotificationClickListener = object : NotificationClickListener {
        override fun onNotificationClick(notification: Notification) {
            if (!notification.isRead) {
                Toast.makeText(requireContext(), "Marked as read", Toast.LENGTH_SHORT).show()
                notification.isRead = true
                viewModel.updateNotification(notification)

                analytics.logEvent("btn_notification_mark_as_read_clicked") {
                    param("notification_id", notification.id)
                    param("notification_title", notification.title)
                    param("notification_message", notification.body)
                    param("notification_is_read", notification.isRead.toString())
                }
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentNotificationBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setNotification()
    }

    private fun setNotification() {
        viewModel.getNotificationList().observe(viewLifecycleOwner) {
            if (it.isNotEmpty()) {
                binding.rvNotification.visibility = View.VISIBLE
                binding.error.error.visibility = View.GONE
                val layoutManager = LinearLayoutManager(requireContext())
                layoutManager.reverseLayout = true
                layoutManager.stackFromEnd = true
                binding.rvNotification.layoutManager = layoutManager

                val notificationAdapter = NotificationAdapter(onNotificationClickListener)
                notificationAdapter.setData(it)
                binding.rvNotification.adapter = notificationAdapter
            } else {
                binding.rvNotification.visibility = View.GONE
                binding.error.error.visibility = View.VISIBLE
                binding.error.btnRefresh.visibility = View.GONE
            }
        }
    }
}

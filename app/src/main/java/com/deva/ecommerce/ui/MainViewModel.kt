package com.deva.ecommerce.ui

import androidx.lifecycle.ViewModel
import com.deva.ecommerce.model.local.room.CartDao
import com.deva.ecommerce.model.local.room.NotificationDao
import com.deva.ecommerce.model.local.room.WishlistDao
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(
    private val wishlistDao: WishlistDao,
    private val cartDao: CartDao,
    private val notificationDao: NotificationDao
) : ViewModel() {
    fun getWishlistedProduct() = wishlistDao.getWishlistedProduct()

    fun getCartItem() = cartDao.getAllCartItems()

    fun getUnreadNotification() = notificationDao.getUnreadNotification()
}

package com.deva.ecommerce.ui.main.checkout

import com.deva.ecommerce.model.local.entity.CartItem

interface CheckoutClickListener {
    fun onTotalPriceChanged(totalPriceFromChecked: Int)
    fun onAddQtyClick(cartItem: CartItem)
    fun onRemoveQtyClick(cartItem: CartItem)
}

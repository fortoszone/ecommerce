package com.deva.ecommerce.ui.main.store.paging

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import com.deva.ecommerce.databinding.RowProductListBinding
import com.deva.ecommerce.model.remote.response.ProductItems

class ProductPagingAdapter(
    diffCallBack: DiffUtil.ItemCallback<ProductItems>
) :
    PagingDataAdapter<ProductItems, ProductViewHolder>(diffCallBack) {

    override fun onBindViewHolder(holder: ProductViewHolder, position: Int) {
        val product = getItem(position)
        if (product != null) {
            holder.bind(product)
        }
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ProductViewHolder {
        return ProductViewHolder(
            RowProductListBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    companion object {
        val diffCallBack = object : DiffUtil.ItemCallback<ProductItems>() {
            override fun areItemsTheSame(oldItem: ProductItems, newItem: ProductItems): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: ProductItems, newItem: ProductItems): Boolean {
                return oldItem == newItem
            }
        }
    }
}

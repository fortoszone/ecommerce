package com.deva.ecommerce.ui.main.cart

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import com.deva.ecommerce.R
import com.deva.ecommerce.databinding.FragmentCartBinding
import com.deva.ecommerce.model.local.entity.CartItem
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.logEvent
import dagger.hilt.android.AndroidEntryPoint
import java.text.NumberFormat
import java.util.Locale
import javax.inject.Inject

@AndroidEntryPoint
class CartFragment : Fragment() {
    private var _binding: FragmentCartBinding? = null
    private val binding get() = _binding!!
    private val viewModel: CartViewModel by viewModels()
    private val locale = Locale("id", "ID")
    private val formatter: NumberFormat = NumberFormat.getCurrencyInstance(locale).apply {
        maximumFractionDigits = 0
    }

    @Inject
    lateinit var analytics: FirebaseAnalytics

    private var onCartClickListener = object : CartClickListener {
        override fun onRemoveClick(cartItem: CartItem) {
            cartAnalytics(cartItem, "btn_cart_remove_clicked")
            viewModel.removeCartItem(cartItem)
            Snackbar.make(
                requireView(),
                R.string.removed_from_cart,
                Snackbar.LENGTH_SHORT
            ).show()
        }

        override fun onAddQtyClick(cartItem: CartItem) {
            if (cartItem.quantity + 1 > cartItem.stock) {
                Snackbar.make(
                    requireView(),
                    R.string.insufficient_stock,
                    Snackbar.LENGTH_SHORT
                ).show()
            } else {
                cartItem.quantity++
                viewModel.updateCartQuantity(cartItem.id, cartItem.quantity)
                cartAnalytics(cartItem, "btn_add_qty_clicked")
            }
        }

        override fun onRemoveQtyClick(cartItem: CartItem) {
            if (cartItem.quantity > 1) {
                cartItem.quantity--
                viewModel.updateCartQuantity(cartItem.id, cartItem.quantity)
            } else {
                viewModel.removeCartItem(cartItem)
                cartAnalytics(cartItem, "btn_remove_qty_clicked")
            }
        }

        override fun onCheckClick(cartItem: CartItem) {
            viewModel.updateCart(cartItem.copy(isSelected = !cartItem.isSelected))
        }

        override fun onTotalPriceChanged(totalPriceFromChecked: Int) {
            binding.tvTotalPriceValue.text = formatter.format(totalPriceFromChecked)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentCartBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setCartItem()

        viewModel.isEveryProductSelected.observe(viewLifecycleOwner) {
            binding.cbCheckAll.isChecked = it
        }

        viewModel.isAnyProductSelected.observe(viewLifecycleOwner) {
            binding.btnCheckout.isEnabled = it
            binding.btnDeleteAll.isVisible = it
        }

        viewModel.isAProductSelected.observe(viewLifecycleOwner) {
            binding.btnCheckout.isEnabled = it
            binding.btnDeleteAll.isVisible = it
        }

        binding.cbCheckAll.setOnClickListener {
            val isCheckedAll = binding.cbCheckAll.isChecked
            viewModel.updateSelectedAllProduct(isCheckedAll)
        }

        binding.btnDeleteAll.setOnClickListener {
            viewModel.deleteAllSelectedProduct()
            Snackbar.make(
                requireView(),
                R.string.removed_from_cart,
                Snackbar.LENGTH_SHORT
            ).show()

            analytics.logEvent("btn_cart_delete_all_clicked") {
                param(FirebaseAnalytics.Param.ITEM_ID, "all")
                param(FirebaseAnalytics.Param.ITEM_NAME, "all")
                param(FirebaseAnalytics.Param.ITEM_VARIANT, "all")
                param(FirebaseAnalytics.Param.PRICE, 0.0)
                param(FirebaseAnalytics.Param.QUANTITY, 0.0)
            }
        }
    }

    private fun setCartItem() {
        viewModel.getCartItem().observe(viewLifecycleOwner) { item ->
            binding.rvCart.layoutManager = LinearLayoutManager(requireContext())
            val adapter = CartAdapter(onCartClickListener)
            binding.rvCart.adapter = adapter
            adapter.setData(item)
            adapter.updatePrice()

            binding.apply {
                if (item.isEmpty()) {
                    rvCart.visibility = View.GONE
                    llCheckAll.visibility = View.GONE
                    error.error.visibility = View.VISIBLE
                    error.btnRefresh.visibility = View.GONE
                    vDivider.visibility = View.GONE
                } else {
                    rvCart.visibility = View.VISIBLE
                    llCheckAll.visibility = View.VISIBLE
                    error.error.visibility = View.GONE
                    vDivider.visibility = View.VISIBLE

                    item.forEach {
                        val bundle = Bundle().apply {
                            putString(FirebaseAnalytics.Param.ITEM_ID, it.id)
                            putString(FirebaseAnalytics.Param.ITEM_NAME, it.name)
                            putString(FirebaseAnalytics.Param.ITEM_VARIANT, it.variant)
                            putInt(FirebaseAnalytics.Param.PRICE, it.price)
                            putInt(FirebaseAnalytics.Param.QUANTITY, it.quantity)
                        }

                        analytics.logEvent(FirebaseAnalytics.Event.VIEW_CART) {
                            param(FirebaseAnalytics.Param.ITEMS, arrayOf(bundle))
                        }
                    }
                }
            }

            binding.btnCheckout.setOnClickListener { it ->
                val selectedItem = item.filter { it.isSelected }
                val bundle = bundleOf("product" to selectedItem)
                Navigation.findNavController(it)
                    .navigate(R.id.action_navigation_cart_to_navigation_checkout, bundle)

                val analyticsBundle = Bundle().apply {
                    selectedItem.forEach {
                        putString(FirebaseAnalytics.Param.ITEM_ID, it.id)
                        putString(FirebaseAnalytics.Param.ITEM_NAME, it.name)
                        putString(FirebaseAnalytics.Param.ITEM_VARIANT, it.variant)
                        putInt(FirebaseAnalytics.Param.PRICE, it.price)
                        putInt(FirebaseAnalytics.Param.QUANTITY, it.quantity)
                    }
                }

                analytics.logEvent("btn_checkout_clicked", analyticsBundle)
            }
        }
    }

    private fun cartAnalytics(cartItem: CartItem, event: String) {
        val item = Bundle().apply {
            putString(FirebaseAnalytics.Param.ITEM_ID, cartItem.id)
            putString(FirebaseAnalytics.Param.ITEM_NAME, cartItem.name)
            putString(FirebaseAnalytics.Param.ITEM_VARIANT, cartItem.variant)
            putInt(FirebaseAnalytics.Param.PRICE, cartItem.price)
            putInt(FirebaseAnalytics.Param.QUANTITY, cartItem.quantity)
        }

        analytics.logEvent(event) {
            param(FirebaseAnalytics.Param.ITEMS, arrayOf(item))
        }
    }
}

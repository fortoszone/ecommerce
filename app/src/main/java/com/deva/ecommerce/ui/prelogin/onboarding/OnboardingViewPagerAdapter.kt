package com.deva.ecommerce.ui.prelogin.onboarding

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.deva.ecommerce.R

class OnboardingViewPagerAdapter(private val image: List<Int>, private val context: Context) :
    RecyclerView.Adapter<OnboardingViewPagerAdapter.PagerViewHolder>() {

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): PagerViewHolder {
        val view: View =
            LayoutInflater.from(context).inflate(R.layout.row_onboarding, parent, false)
        return PagerViewHolder(view)
    }

    override fun onBindViewHolder(
        holder: PagerViewHolder,
        position: Int
    ) {
        holder.imgOnboarding.setImageResource(image[position])
    }

    override fun getItemCount(): Int {
        return image.size
    }

    inner class PagerViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val imgOnboarding: ImageView = itemView.findViewById(R.id.img_onboarding)
    }
}

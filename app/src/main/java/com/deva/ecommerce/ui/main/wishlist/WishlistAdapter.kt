package com.deva.ecommerce.ui.main.wishlist

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.navigation.Navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.deva.ecommerce.R
import com.deva.ecommerce.databinding.RowProductWishlistBinding
import com.deva.ecommerce.model.local.entity.Product
import java.text.NumberFormat
import java.util.Locale

class WishlistAdapter(private val callback: OnWishlistClickListener) :
    RecyclerView.Adapter<WishlistAdapter.WishlistViewHolder>() {
    private var products = emptyList<Product>()

    inner class WishlistViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val binding = RowProductWishlistBinding.bind(view)
        private val locale = Locale("id", "ID")
        private val formatter: NumberFormat = NumberFormat.getCurrencyInstance(locale).apply {
            maximumFractionDigits = 0
        }

        fun bind(product: Product) {
            binding.tvProductName.text = product.name
            binding.tvProductPrice.text = formatter.format(product.price)
            binding.tvProductRating.text = product.rating.toString()
            binding.tvProductSeller.text = product.seller
            binding.tvProductSold.text = product.sold.toString()
            Glide.with(itemView.context)
                .load(product.image)
                .into(binding.ivProduct)

            binding.product.setOnClickListener {
                val id = bundleOf("id" to product.id)
                findNavController(it)
                    .navigate(
                        R.id.action_navigation_wishlist_to_navigation_product_detail,
                        id
                    )
            }

            binding.btnDelete.setOnClickListener {
                callback.onRemoveClick(product)
            }

            binding.btnAddToCart.setOnClickListener {
                callback.onAddToCart(product)
            }
        }
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): WishlistViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(
            R.layout.row_product_wishlist,
            parent,
            false
        )

        return WishlistViewHolder(view)
    }

    override fun onBindViewHolder(holder: WishlistViewHolder, position: Int) {
        holder.bind(products[position])
    }

    override fun getItemCount(): Int {
        return products.size
    }

    fun setData(product: List<Product>) {
        product.isEmpty()
        this.products = product
    }
}

package com.deva.ecommerce.ui.main.transaction

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.deva.ecommerce.model.remote.ApiService
import com.deva.ecommerce.model.remote.response.TransactionResponse
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import retrofit2.HttpException
import java.io.IOException
import javax.inject.Inject

@HiltViewModel
class TransactionViewModel @Inject constructor(private val apiService: ApiService) : ViewModel() {
    private val _data = MutableLiveData<TransactionResponse>()
    val data: MutableLiveData<TransactionResponse> = _data

    private val _error = MutableLiveData<TransactionResponse>()
    val error: MutableLiveData<TransactionResponse> = _error

    private val _isLoading = MutableLiveData<Boolean>()
    val isLoading: MutableLiveData<Boolean> = _isLoading

    private fun setIsLoading(it: Boolean) {
        _isLoading.value = it
    }

    fun getTransactionList() {
        setIsLoading(true)
        viewModelScope.launch {
            delay(1000)

            try {
                val response = apiService.getTransaction()
                if (response.isSuccessful) {
                    _data.value = response.body()
                    setIsLoading(false)
                } else {
                    _error.value = TransactionResponse(
                        code = response.code(),
                        data = emptyList(),
                        message = response.message()
                    )
                    setIsLoading(false)
                }
            } catch (e: HttpException) {
                _error.value = TransactionResponse(
                    code = e.code(),
                    data = emptyList(),
                    message = e.message()
                )
            } catch (e: IOException) {
                _error.value = TransactionResponse(
                    code = 0,
                    data = emptyList(),
                    message = e.message.toString()
                )
                setIsLoading(false)
            }
        }
    }
}

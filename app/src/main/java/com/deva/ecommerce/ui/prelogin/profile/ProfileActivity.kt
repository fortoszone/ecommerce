package com.deva.ecommerce.ui.prelogin.profile

import android.annotation.SuppressLint
import android.content.ContentResolver
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Bundle
import android.provider.OpenableColumns
import android.util.Log
import android.widget.ImageView
import android.widget.Toast
import androidx.activity.result.PickVisualMediaRequest
import androidx.activity.result.contract.ActivityResultContracts
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.FileProvider
import androidx.core.widget.doOnTextChanged
import androidx.lifecycle.lifecycleScope
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.CustomTarget
import com.bumptech.glide.request.transition.Transition
import com.deva.ecommerce.R
import com.deva.ecommerce.databinding.ActivityAddProfileBinding
import com.deva.ecommerce.model.local.entity.User
import com.deva.ecommerce.preference.AuthPreference
import com.deva.ecommerce.preference.SessionManager
import com.deva.ecommerce.ui.MainActivity
import com.google.android.material.badge.ExperimentalBadgeUtils
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.firebase.analytics.FirebaseAnalytics
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch
import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream
import javax.inject.Inject

@ExperimentalBadgeUtils
@AndroidEntryPoint
class ProfileActivity : AppCompatActivity() {
    private lateinit var binding: ActivityAddProfileBinding
    private var latestTmpUri: Uri? = null
    private val viewModel: ProfileViewModel by viewModels()
    private var selectedImage: Uri? = null
    private var imageFile: File? = null

    @Inject
    lateinit var authPreference: AuthPreference

    @Inject
    lateinit var sessionManager: SessionManager

    @Inject
    lateinit var analytics: FirebaseAnalytics

    private val pickMedia =
        registerForActivityResult(ActivityResultContracts.PickVisualMedia()) { uri ->
            if (uri != null) {
                selectedImage = uri
                Log.d("PhotoPicker", "Selected URI: $uri")
                binding.userImage.setImageURI(uri)
                binding.userImage.scaleType = ImageView.ScaleType.CENTER_CROP
            } else {
                Log.d("PhotoPicker", "No media selected")
            }
        }

    private val takeImageResult =
        registerForActivityResult(ActivityResultContracts.TakePicture()) { isSuccess ->
            if (isSuccess) {
                Glide.with(this).asBitmap().load(latestTmpUri).into(object :
                    CustomTarget<Bitmap>() {
                    override fun onResourceReady(
                        resource: Bitmap,
                        transition: Transition<in Bitmap>?
                    ) {
                        lifecycleScope.launch {
                            imageFile?.let { viewModel.processImage(resource, it) }
                        }

                        binding.userImage.setImageBitmap(resource)
                        binding.userImage.scaleType = ImageView.ScaleType.CENTER_CROP
                    }

                    override fun onLoadCleared(placeholder: Drawable?) {
                        // Do nothing
                    }
                })
            }
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityAddProfileBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.userImage.setOnClickListener {
            MaterialAlertDialogBuilder(this).setTitle(resources.getString(R.string.choose_image))
                .setItems(
                    arrayOf(
                        resources.getString(R.string.camera),
                        resources.getString(R.string.gallery)
                    )
                ) { _, which ->
                    when (which) {
                        0 -> takeImage()
                        1 -> pickMedia.launch(PickVisualMediaRequest(ActivityResultContracts.PickVisualMedia.ImageOnly))
                    }
                }.show()

            analytics.logEvent("profile_image", null)
        }

        binding.btnFinish.setOnClickListener {
            val userName = binding.name.text.toString().trim()
            val userImage = selectedImage?.let { getImageFile(it) }
            if (userImage != null) {
                viewModel.addProfile(userName, userImage)
            } else
                viewModel.addProfile(userName, null)

            viewModel.data.observe(this@ProfileActivity) {
                if (it.data?.userName != null) {
                    sessionManager.saveUserName(it.data.userName)
                    val user = User()
                    user.userName = it.data.userName
                    authPreference.saveNameAndImageFilled(true)
                    startActivity(Intent(this, MainActivity::class.java))
                    finish()
                }
            }

            viewModel.error.observe(this@ProfileActivity) {
                Toast.makeText(this, it.message, Toast.LENGTH_SHORT).show()
            }

            analytics.logEvent("profile_filled", null)
        }

        binding.btnFinish.isEnabled = false
        viewModel.nameUser.observe(this@ProfileActivity) { nameUserFilled ->
            if (nameUserFilled) {
                binding.btnFinish.isEnabled = nameUserFilled
            } else
                binding.btnFinish.isEnabled = nameUserFilled
        }

        binding.name.doOnTextChanged { text, _, _, _ ->
            viewModel.isNameUserFilled(text.toString())
        }
    }

    private fun takeImage() {
        lifecycleScope.launch {
            getTmpFileUri().let { uri ->
                latestTmpUri = uri
                selectedImage = uri
                takeImageResult.launch(uri)
            }
        }
    }

    private fun getTmpFileUri(): Uri {
        val tmpFile = File.createTempFile("tmp_image_user", ".png", this.filesDir).apply {
            createNewFile()
            deleteOnExit()
        }

        return FileProvider.getUriForFile(
            this,
            "${this.packageName}.provider",
            tmpFile
        )
    }

    @SuppressLint("Recycle")
    private fun getImageFile(imageUri: Uri): File {
        val parcelFileDescriptor = applicationContext.contentResolver.openFileDescriptor(
            imageUri,
            "r",
            null
        )
        val file = File(
            applicationContext.cacheDir,
            applicationContext.contentResolver.getFileName(imageUri)
        )
        val inputStream = FileInputStream(parcelFileDescriptor?.fileDescriptor)
        val outputStream = FileOutputStream(file)
        inputStream.copyTo(outputStream)
        return file
    }

    @SuppressLint("Range")
    fun ContentResolver.getFileName(uri: Uri): String {
        var name = ""
        val cursor = query(
            uri,
            null,
            null,
            null,
            null
        )
        cursor?.use {
            it.moveToFirst()
            name = it.getString(it.getColumnIndex(OpenableColumns.DISPLAY_NAME))
        }
        return name
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }
}

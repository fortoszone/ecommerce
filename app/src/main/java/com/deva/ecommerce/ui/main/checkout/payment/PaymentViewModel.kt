package com.deva.ecommerce.ui.main.checkout.payment

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.deva.ecommerce.model.remote.response.PaymentResponse
import com.deva.ecommerce.ui.main.store.filter.FilterDialogFragment.Companion.TAG
import com.google.firebase.remoteconfig.ConfigUpdate
import com.google.firebase.remoteconfig.ConfigUpdateListener
import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import com.google.firebase.remoteconfig.FirebaseRemoteConfigException
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class PaymentViewModel @Inject constructor(private val remoteConfig: FirebaseRemoteConfig) :
    ViewModel() {
    private val _data = MutableLiveData<PaymentResponse>()
    val data: MutableLiveData<PaymentResponse> = _data

    private fun fetchRealtimePayment() {
        remoteConfig.addOnConfigUpdateListener(object : ConfigUpdateListener {
            override fun onUpdate(configUpdate: ConfigUpdate) {
                remoteConfig.activate().addOnCompleteListener {
                    Log.d(TAG, "Updated keys: " + configUpdate.updatedKeys)

                    if (configUpdate.updatedKeys.contains("payment")) {
                        remoteConfig.activate()
                    }
                }
            }

            override fun onError(error: FirebaseRemoteConfigException) {
                Log.w(TAG, "Config update error with code: " + error.code, error)
            }
        })
    }

    init {
        fetchRealtimePayment()
    }
}

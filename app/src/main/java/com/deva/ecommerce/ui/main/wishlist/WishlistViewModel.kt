package com.deva.ecommerce.ui.main.wishlist

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.deva.ecommerce.model.local.entity.CartItem
import com.deva.ecommerce.model.local.entity.Product
import com.deva.ecommerce.model.local.room.CartDao
import com.deva.ecommerce.model.local.room.WishlistDao
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class WishlistViewModel @Inject constructor(
    private val wishlistDao: WishlistDao,
    private val cartDao: CartDao
) : ViewModel() {
    private var _isGrid = MutableLiveData(false)
    val isGrid: LiveData<Boolean> get() = _isGrid

    private var _isLoading = MutableLiveData(false)
    val isLoading: LiveData<Boolean> get() = _isLoading

    fun toggleLayout() {
        _isGrid.value = !(_isGrid.value ?: false)
    }

    fun getWishlistedProduct() = wishlistDao.getWishlistedProduct()

    fun deleteFromWishlist(product: Product) {
        viewModelScope.launch(Dispatchers.Default) {
            wishlistDao.removeFromWishlist(product)
        }
    }

    fun addToCart(product: Product) {
        viewModelScope.launch(Dispatchers.Default) {
            cartDao.insertCartItem(
                CartItem(
                    id = product.id,
                    name = product.name,
                    price = product.price,
                    image = product.image,
                    stock = 1,
                    variant = product.variant,
                    quantity = 1,
                    isSelected = false
                )
            )
        }
    }
}

package com.deva.ecommerce.ui.main.wishlist

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.deva.ecommerce.R
import com.deva.ecommerce.databinding.RowProductWishlistGridBinding
import com.deva.ecommerce.model.local.entity.Product
import java.text.NumberFormat
import java.util.Locale

class WishlistAdapterGrid(private val callback: OnWishlistClickListener) :
    RecyclerView.Adapter<WishlistAdapterGrid.WishlistGridViewHolder>() {
    private var products = emptyList<Product>()

    inner class WishlistGridViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val binding = RowProductWishlistGridBinding.bind(view)
        private val locale = Locale("id", "ID")
        private val formatter: NumberFormat = NumberFormat.getCurrencyInstance(locale).apply {
            maximumFractionDigits = 0
        }

        fun bind(product: Product) {
            binding.tvProductName.text = product.name
            binding.tvProductPrice.text = formatter.format(product.price)
            binding.tvProductRating.text = product.rating.toString()
            binding.tvProductSeller.text = product.seller
            binding.tvProductSold.text = product.sold.toString()
            Glide.with(itemView.context)
                .load(product.image)
                .into(binding.ivProduct)

            binding.product.setOnClickListener {
                val id = bundleOf("id" to product.id)
                Navigation.findNavController(it)
                    .navigate(
                        R.id.action_navigation_wishlist_to_navigation_product_detail,
                        id
                    )
            }

            binding.btnDelete.setOnClickListener {
                callback.onRemoveClick(product)
            }

            binding.btnAddToCart.setOnClickListener {
                callback.onAddToCart(product)
            }
        }
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): WishlistGridViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(
            R.layout.row_product_wishlist_grid,
            parent,
            false
        )

        return WishlistGridViewHolder(view)
    }

    override fun onBindViewHolder(holder: WishlistGridViewHolder, position: Int) {
        holder.bind(products[position])
    }

    override fun getItemCount(): Int {
        return products.size
    }

    @SuppressLint("NotifyDataSetChanged")
    fun setData(movie: List<Product>) {
        movie.isEmpty()
        this.products = movie
        notifyDataSetChanged()
    }
}

package com.deva.ecommerce.ui.prelogin.onboarding

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.viewpager2.widget.ViewPager2
import com.deva.ecommerce.R
import com.deva.ecommerce.databinding.ActivityOnboardingBinding
import com.deva.ecommerce.ui.prelogin.login.LoginActivity
import com.deva.ecommerce.ui.prelogin.register.RegisterActivity
import com.google.android.material.tabs.TabLayoutMediator
import com.google.firebase.analytics.FirebaseAnalytics
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class OnboardingActivity : AppCompatActivity() {
    private lateinit var binding: ActivityOnboardingBinding
    private val imageList = mutableListOf<Int>()

    @Inject
    lateinit var analytics: FirebaseAnalytics

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityOnboardingBinding.inflate(layoutInflater)
        setContentView(binding.root)

        postToList()

        binding.viewPager.adapter = OnboardingViewPagerAdapter(imageList, this)

        TabLayoutMediator(binding.intoTabLayout, binding.viewPager) { _, _ -> }.attach()

        binding.btnJoinNow.setOnClickListener {
            analytics.logEvent("btn_join_now_clicked", null)

            startActivity(Intent(this, RegisterActivity::class.java))
            finish()
        }

        binding.btnSkip.setOnClickListener {
            analytics.logEvent("btn_skip_clicked", null)

            startActivity(Intent(this, LoginActivity::class.java))
            finish()
        }

        binding.viewPager.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
            override fun onPageSelected(position: Int) {
                if (position == 2) {
                    binding.btnNext.visibility = View.INVISIBLE
                } else
                    binding.btnNext.visibility = View.VISIBLE
                super.onPageSelected(position)
            }
        })

        binding.btnNext.setOnClickListener {
            binding.viewPager.currentItem = binding.viewPager.currentItem + 1
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        binding.viewPager.unregisterOnPageChangeCallback(object :
            ViewPager2.OnPageChangeCallback() {
        })
    }

    private fun addToList(image: Int) {
        imageList.add(image)
    }

    private fun postToList() {
        addToList(R.drawable.onboarding1)
        addToList(R.drawable.onboarding2)
        addToList(R.drawable.onboarding3)
    }
}

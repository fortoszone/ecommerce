package com.deva.ecommerce.ui

import android.content.Intent
import android.content.res.Configuration
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.view.ViewTreeObserver
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.core.splashscreen.SplashScreen.Companion.installSplashScreen
import androidx.core.view.isVisible
import androidx.navigation.NavController
import androidx.navigation.NavOptions
import androidx.navigation.findNavController
import androidx.navigation.ui.setupWithNavController
import androidx.window.layout.WindowMetricsCalculator
import com.deva.ecommerce.R
import com.deva.ecommerce.databinding.ActivityMainBinding
import com.deva.ecommerce.preference.AuthPreference
import com.deva.ecommerce.preference.SessionManager
import com.deva.ecommerce.ui.prelogin.login.LoginActivity
import com.deva.ecommerce.ui.prelogin.onboarding.OnboardingActivity
import com.deva.ecommerce.ui.prelogin.profile.ProfileActivity
import com.google.android.material.badge.BadgeDrawable
import com.google.android.material.badge.BadgeUtils
import com.google.android.material.badge.ExperimentalBadgeUtils
import com.google.android.material.elevation.SurfaceColors
import com.google.firebase.messaging.FirebaseMessaging
import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@ExperimentalBadgeUtils
@AndroidEntryPoint
class MainActivity :
    AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding

    @Inject
    lateinit var sessionManager: SessionManager

    @Inject
    lateinit var authPreference: AuthPreference

    @Inject
    lateinit var remoteConfig: FirebaseRemoteConfig

    @Inject
    lateinit var messaging: FirebaseMessaging

    private val viewModel: MainViewModel by viewModels()

    private val navController: NavController by lazy {
        findNavController(R.id.nav_host_fragment_activity_main)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        installSplashScreen()
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setToolbarAndBottomNav()

        AppCompatDelegate.setDefaultNightMode(
            if (sessionManager.isDarkMode()) AppCompatDelegate.MODE_NIGHT_YES else AppCompatDelegate.MODE_NIGHT_NO
        )

        if (!(authPreference.isOnboarded())) {
            startActivity(Intent(this, OnboardingActivity::class.java))
            authPreference.saveOnboardedStatus(true)
            finish()
        } else {
            if (!authPreference.isLoggedIn()) {
                startActivity(Intent(this, LoginActivity::class.java))
                finish()
            } else {
                if (!authPreference.isNameAndImageFilled()) {
                    startActivity(Intent(this, ProfileActivity::class.java))
                    finish()
                }
            }
        }

        val container: ViewGroup = binding.container

        container.addView(object : View(this) {
            override fun onConfigurationChanged(newConfig: Configuration?) {
                super.onConfigurationChanged(newConfig)
                computeWindowSizeClasses(navController)
            }
        })

        computeWindowSizeClasses(navController)

        observeCart()
        observeWishlist()
        observeNotification()

        fetchAndActivatePayment()

        messaging.subscribeToTopic("promo")
    }

    private fun setToolbarAndBottomNav() {
        val metrics = WindowMetricsCalculator.getOrCreate()
            .computeCurrentWindowMetrics(this)

        val widthDp = metrics.bounds.width() /
            resources.displayMetrics.density

        when {
            widthDp < 600f ->
                binding.bottomNavView?.setupWithNavController(navController)

            widthDp < 840f ->
                binding.navRailView?.setupWithNavController(navController)

            else ->
                binding.navView?.setupWithNavController(navController)
        }

        binding.topAppBar.setOnMenuItemClickListener {
            Log.d("Menu", it.title.toString())
            when (it.itemId) {
                R.id.cart -> {
                    navController.navigate(
                        R.id.navigation_cart,
                        null,
                        NavOptions.Builder()
                            .setPopEnterAnim(androidx.navigation.ui.R.anim.nav_default_pop_enter_anim)
                            .setEnterAnim(androidx.navigation.ui.R.anim.nav_default_enter_anim)
                            .setExitAnim(androidx.navigation.ui.R.anim.nav_default_exit_anim)
                            .setPopExitAnim(androidx.navigation.ui.R.anim.nav_default_pop_exit_anim)
                            .build()
                    )
                    true
                }

                R.id.notification -> {
                    navController.navigate(
                        R.id.navigation_notification,
                        null,
                        NavOptions.Builder()
                            .setPopEnterAnim(androidx.navigation.ui.R.anim.nav_default_pop_enter_anim)
                            .setEnterAnim(androidx.navigation.ui.R.anim.nav_default_enter_anim)
                            .setExitAnim(androidx.navigation.ui.R.anim.nav_default_exit_anim)
                            .setPopExitAnim(androidx.navigation.ui.R.anim.nav_default_pop_exit_anim)
                            .build()
                    )
                    true
                }

                else -> return@setOnMenuItemClickListener true
            }
        }

        navController.addOnDestinationChangedListener { _, destination, _ ->
            Log.d("Destination", destination.displayName)
            when (destination.id) {
                R.id.navigation_home, R.id.navigation_wishlist, R.id.navigation_transaction, R.id.navigation_store -> {
                    binding.topAppBar.apply {
                        isTitleCentered = false
                        isVisible = true
                        title = sessionManager.fetchUserName()
                        setNavigationIcon(R.drawable.baseline_account_circle_24)
                        setNavigationOnClickListener(null)
                    }

                    binding.topAppBar.menu.setGroupVisible(
                        R.id.toolbar_group,
                        true
                    )

                    binding.topAppBar.menu.setGroupVisible(R.id.toolbar_group, true)
                    binding.bottomNavBorder?.isVisible = true

                    when {
                        widthDp < 600f -> apply {
                            binding.navBorder?.isVisible = true
                            binding.bottomNavView?.isVisible = true
                        }

                        widthDp < 840f -> apply {
                            binding.navBorder?.isVisible = true
                            binding.navRailView?.isVisible = true
                        }

                        else -> apply {
                            binding.navBorder?.isVisible = true
                            binding.navView?.isVisible = true
                        }
                    }

                    when (resources.configuration.uiMode and Configuration.UI_MODE_NIGHT_MASK) {
                        Configuration.UI_MODE_NIGHT_YES ->
                            window.navigationBarColor =
                                SurfaceColors.SURFACE_2.getColor(this)

                        else -> window.navigationBarColor = getColor(R.color.white)
                    }
                }

                else -> {
                    binding.bottomNavBorder?.isVisible = false

                    when (resources.configuration.uiMode and Configuration.UI_MODE_NIGHT_MASK) {
                        Configuration.UI_MODE_NIGHT_YES ->
                            window.navigationBarColor =
                                SurfaceColors.SURFACE_0.getColor(this)

                        else -> window.navigationBarColor = getColor(R.color.white)
                    }

                    binding.topAppBar.apply {
                        setNavigationIcon(R.drawable.baseline_arrow_back_24)

                        isTitleCentered = false
                        setNavigationOnClickListener {
                            navController.popBackStack()
                        }
                    }

                    when {
                        widthDp < 600f -> apply {
                            binding.navBorder?.visibility = View.GONE
                            binding.bottomNavView?.visibility = View.GONE
                        }

                        widthDp < 840f -> apply {
                            binding.navBorder?.visibility = View.GONE
                            binding.navRailView?.visibility = View.GONE
                        }

                        else -> apply {
                            binding.navBorder?.visibility = View.GONE
                            binding.navView?.visibility = View.GONE
                        }
                    }

                    binding.topAppBar.menu.setGroupVisible(
                        R.id.toolbar_group,
                        false
                    )

                    when (destination.id) {
                        R.id.navigation_cart -> {
                            binding.topAppBar.title = getString(R.string.title_cart)
                        }

                        R.id.navigation_notification -> {
                            binding.topAppBar.title = getString(R.string.title_notifications)
                        }

                        R.id.navigation_product_detail -> {
                            binding.topAppBar.title = getString(R.string.product_detail)
                            binding.topAppBar.menu.setGroupVisible(
                                R.id.toolbar_group,
                                true
                            )
                        }

                        R.id.navigation_checkout -> {
                            binding.topAppBar.title = getString(R.string.checkout)
                        }

                        R.id.navigation_product_review -> {
                            binding.topAppBar.title = getString(R.string.product_review)
                        }

                        R.id.navigation_payment -> {
                            binding.topAppBar.title = getString(R.string.payment)
                        }

                        R.id.navigation_search -> {
                            binding.topAppBar.visibility = View.GONE
                        }

                        R.id.navigation_fulfillment -> {
                            binding.topAppBar.apply {
                                title = getString(R.string.status)
                                menu.setGroupVisible(R.id.toolbar_group, false)
                                navigationIcon = null
                                isTitleCentered = true
                            }
                        }
                    }
                }
            }
        }

        binding.bottomNavView?.setOnItemReselectedListener {}
        binding.navRailView?.setOnItemReselectedListener {}
    }

    private fun observeWishlist() {
        val metrics = WindowMetricsCalculator.getOrCreate()
            .computeCurrentWindowMetrics(this)

        val widthDp = metrics.bounds.width() /
            resources.displayMetrics.density

        val badge = when {
            widthDp < 600f ->
                binding.bottomNavView?.getOrCreateBadge(R.id.navigation_wishlist)

            widthDp < 840f ->
                binding.navRailView?.getOrCreateBadge(R.id.navigation_wishlist)

            else -> null
        }

        viewModel.getWishlistedProduct().observe(this) {
            if (it.isEmpty()) {
                badge?.isVisible = false
            } else {
                badge?.isVisible = true
                badge?.number = it.size
            }
        }
    }

    private fun observeCart() {
        binding.topAppBar.viewTreeObserver.addOnGlobalLayoutListener(
            (
                ViewTreeObserver.OnGlobalLayoutListener {
                    val badgeDrawable = BadgeDrawable.create(this@MainActivity)
                    viewModel.getCartItem().observe(this@MainActivity) {
                        if (it.isEmpty()) {
                            badgeDrawable.isVisible = false
                        } else {
                            badgeDrawable.isVisible = true
                            badgeDrawable.number = it.size
                        }
                    }
                    BadgeUtils.attachBadgeDrawable(
                        badgeDrawable,
                        binding.topAppBar,
                        R.id.cart
                    )
                }
                )
        )
    }

    private fun observeNotification() {
        binding.topAppBar.viewTreeObserver.addOnGlobalLayoutListener(
            (
                ViewTreeObserver.OnGlobalLayoutListener {
                    val badgeDrawable = BadgeDrawable.create(this@MainActivity)
                    viewModel.getUnreadNotification().observe(this@MainActivity) {
                        if (it.isEmpty()) {
                            badgeDrawable.isVisible = false
                        } else {
                            badgeDrawable.isVisible = true
                            badgeDrawable.number = it.size
                        }
                    }
                    BadgeUtils.attachBadgeDrawable(
                        badgeDrawable,
                        binding.topAppBar,
                        R.id.notification
                    )
                }
                )
        )
    }

    private fun computeWindowSizeClasses(navController: NavController) {
        val metrics = WindowMetricsCalculator.getOrCreate()
            .computeCurrentWindowMetrics(this)

        val widthDp = metrics.bounds.width() /
            resources.displayMetrics.density

        when {
            widthDp < 600f ->
                binding.bottomNavView?.setupWithNavController(navController)

            widthDp < 840f ->
                binding.navRailView?.setupWithNavController(navController)

            else ->
                binding.navView?.setupWithNavController(navController)
        }
    }

    private fun fetchAndActivatePayment() {
        remoteConfig.fetchAndActivate()
    }
}

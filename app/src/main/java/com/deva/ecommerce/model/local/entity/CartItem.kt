package com.deva.ecommerce.model.local.entity

import android.os.Parcelable
import androidx.annotation.Keep
import androidx.room.Entity
import kotlinx.parcelize.Parcelize

@Entity(tableName = "cart_product_table", primaryKeys = ["id"])
@Keep
@Parcelize
data class CartItem(
    val id: String,
    val name: String,
    val image: String,
    val variant: String,
    val stock: Int,
    val price: Int,
    var quantity: Int,
    var isSelected: Boolean
) : Parcelable

package com.deva.ecommerce.model.local.entity

import android.os.Parcelable
import androidx.annotation.Keep
import androidx.room.Entity
import kotlinx.parcelize.Parcelize

@Entity(tableName = "notification_product_table", primaryKeys = ["id"])
@Keep
@Parcelize
data class Notification(
    val id: String,
    val type: String,
    val date: String,
    val title: String,
    val body: String,
    val image: String,
    var isRead: Boolean
) : Parcelable

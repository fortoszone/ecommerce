package com.deva.ecommerce.model.remote.response

import android.os.Parcelable
import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

data class PaymentResponse(
    @SerializedName("code")
    val code: Int,

    @SerializedName("data")
    val data: List<PaymentTitle>,

    @SerializedName("message")
    val message: String
)

data class PaymentTitle(
    @SerializedName("title")
    val title: String,

    @SerializedName("item")
    val items: List<PaymentItems>
)

@Keep
@Parcelize
data class PaymentItems(
    @SerializedName("label")
    val label: String,

    @SerializedName("image")
    val image: String,

    @SerializedName("status")
    val status: Boolean
) : Parcelable

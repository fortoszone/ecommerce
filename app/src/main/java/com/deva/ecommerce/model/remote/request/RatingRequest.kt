package com.deva.ecommerce.model.remote.request

import com.google.gson.annotations.SerializedName

data class RatingRequest(
    @SerializedName("invoiceId")
    val id: String,
    @SerializedName("rating")
    val rating: Int,
    @SerializedName("review")
    val review: String
)

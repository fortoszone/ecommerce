package com.deva.ecommerce.model.local.entity

import android.os.Parcelable
import androidx.annotation.Keep
import androidx.room.Entity
import kotlinx.parcelize.Parcelize

@Entity(tableName = "product_table", primaryKeys = ["id"])
@Keep
@Parcelize
data class Product(
    var id: String = "",
    var image: String = "",
    var name: String,
    var price: Int,
    var seller: String,
    var rating: Float,
    var ratingCount: Int,
    var sold: Int,
    var description: String,
    var loved: Boolean? = false,
    var reviewCount: Int,
    var totalSatisfaction: Int,
    var variant: String
) : Parcelable

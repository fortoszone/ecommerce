package com.deva.ecommerce.model.remote.request

import com.google.gson.annotations.SerializedName

data class UserRequest(
    @SerializedName("email")
    val email: String,

    @SerializedName("password")
    val password: String,

    @SerializedName("firebaseToken")
    val firebaseToken: String
)

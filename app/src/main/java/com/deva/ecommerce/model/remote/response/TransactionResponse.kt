package com.deva.ecommerce.model.remote.response

import com.google.gson.annotations.SerializedName

data class TransactionResponse(
    @SerializedName("code")
    val code: Int,

    @SerializedName("data")
    val data: List<TransactionData>,

    @SerializedName("message")
    val message: String
)

data class TransactionData(
    @SerializedName("invoiceId")
    val id: String,

    @SerializedName("status")
    val status: Boolean,

    @SerializedName("date")
    val date: String,

    @SerializedName("time")
    val time: String,

    @SerializedName("payment")
    val payment: String,

    @SerializedName("total")
    val total: Int,

    @SerializedName("items")
    val items: List<TransactionItemList>,

    @SerializedName("rating")
    val rating: Float?,

    @SerializedName("review")
    val review: String?,

    @SerializedName("image")
    val image: String,

    @SerializedName("name")
    val name: String,
)

data class TransactionItemList(
    @SerializedName("productId")
    val productId: String,

    @SerializedName("variantName")
    val variantName: String,

    @SerializedName("quantity")
    val quantity: Int,
)

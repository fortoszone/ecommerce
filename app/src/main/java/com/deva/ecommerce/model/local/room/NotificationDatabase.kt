package com.deva.ecommerce.model.local.room

import androidx.room.Database
import androidx.room.RoomDatabase
import com.deva.ecommerce.model.local.entity.Notification

@Database(entities = [Notification::class], version = 4, exportSchema = true)
abstract class NotificationDatabase : RoomDatabase() {
    abstract fun notificationDao(): NotificationDao
}

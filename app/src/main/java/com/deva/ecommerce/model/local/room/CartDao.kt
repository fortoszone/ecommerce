package com.deva.ecommerce.model.local.room

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Update
import com.deva.ecommerce.model.local.entity.CartItem

@Dao
interface CartDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertCartItem(cartItem: CartItem)

    @Update
    suspend fun updateCartItem(cartItem: CartItem)

    @Delete
    suspend fun deleteCartItem(cartItem: CartItem)

    @Query("SELECT CASE WHEN COUNT(*) = SUM(isSelected) THEN 1 ELSE 0 END FROM cart_product_table")
    fun checkIsEveryProductSelected(): LiveData<Boolean>

    @Query("SELECT EXISTS(SELECT * FROM cart_product_table WHERE isSelected = 1)")
    fun checkIsAnyProductSelected(): LiveData<Boolean>

    @Query("SELECT * FROM cart_product_table")
    fun getAllCartItems(): LiveData<List<CartItem>>

    @Query("UPDATE cart_product_table SET isSelected = :flag")
    suspend fun updateAllSelectedProduct(flag: Boolean)

    @Query("DELETE FROM cart_product_table WHERE isSelected = 1")
    suspend fun deleteAllSelectedProduct()

    @Query("DELETE FROM cart_product_table WHERE id = :id")
    suspend fun deleteCartById(id: String)

    @Query("DELETE FROM cart_product_table")
    suspend fun deleteAllCart()

    @Query("UPDATE cart_product_table SET quantity = :quantity WHERE id = :id")
    suspend fun updateCartItemQuantity(id: String, quantity: Int)

    @Query("SELECT EXISTS(SELECT * FROM cart_product_table WHERE id = :id)")
    fun checkCartId(id: String): LiveData<Boolean>

    @Query("SELECT quantity FROM cart_product_table WHERE id = :id")
    suspend fun getQuantityById(id: String): Int
}

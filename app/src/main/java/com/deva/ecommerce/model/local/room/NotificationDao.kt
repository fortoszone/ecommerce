package com.deva.ecommerce.model.local.room

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Update
import com.deva.ecommerce.model.local.entity.Notification

@Dao
interface NotificationDao {
    @Query("SELECT * FROM notification_product_table")
    fun getNotification(): LiveData<List<Notification>>

    @Query("SELECT * FROM notification_product_table WHERE isRead = 0")
    fun getUnreadNotification(): LiveData<List<Notification>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addToNotification(notification: Notification)

    @Update
    suspend fun updateNotification(notification: Notification)

    @Query("DELETE FROM notification_product_table")
    suspend fun deleteAllNotification()
}

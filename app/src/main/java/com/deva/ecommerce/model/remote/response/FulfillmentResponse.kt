package com.deva.ecommerce.model.remote.response

import android.os.Parcelable
import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

data class FulfillmentResponse(
    @SerializedName("code")
    var code: Int,

    @SerializedName("data")
    val data: FulfillmentDetail,

    @SerializedName("message")
    val message: String

)

@Keep
@Parcelize
data class FulfillmentDetail(
    @SerializedName("invoiceId")
    val id: String,

    @SerializedName("status")
    val status: Boolean,

    @SerializedName("date")
    val date: String,

    @SerializedName("time")
    val time: String,

    @SerializedName("payment")
    val payment: String,

    @SerializedName("total")
    val total: Int,

    val review: String?,

    val rating: Int?
) : Parcelable

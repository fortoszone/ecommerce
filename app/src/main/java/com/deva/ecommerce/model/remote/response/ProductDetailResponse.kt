package com.deva.ecommerce.model.remote.response

import com.google.gson.annotations.SerializedName

data class ProductDetailResponse(
    @SerializedName("code")
    val code: Int,

    @SerializedName("data")
    val data: ProductDetailData,

    @SerializedName("message")
    val message: String
)

data class ProductDetailData(
    @SerializedName("productId")
    val productId: String,

    @SerializedName("productName")
    val productName: String,

    @SerializedName("productPrice")
    var productPrice: Int,

    @SerializedName("image")
    val image: List<String>,

    @SerializedName("brand")
    val brand: String,

    @SerializedName("description")
    val description: String,

    @SerializedName("store")
    val store: String,

    @SerializedName("sale")
    val sale: Int,

    @SerializedName("stock")
    val stock: Int,

    @SerializedName("totalRating")
    val totalRating: Int,

    @SerializedName("totalReview")
    val totalReview: Int,

    @SerializedName("totalSatisfaction")
    val totalSatisfaction: Int,

    @SerializedName("productRating")
    val productRating: Float,

    @SerializedName("productVariant")
    val productVariant: List<ProductVariant>
)

data class ProductVariant(
    val variantName: String,
    val variantPrice: Int
)

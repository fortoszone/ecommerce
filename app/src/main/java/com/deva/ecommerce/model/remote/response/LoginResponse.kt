package com.deva.ecommerce.model.remote.response

import com.google.gson.annotations.SerializedName

data class LoginResponse(
    @SerializedName("code")
    val code: Int,

    @SerializedName("data")
    val data: LoginData?,

    @SerializedName("message")
    val message: String
)

data class LoginData(
    @SerializedName("userName")
    val userName: String,

    @SerializedName("userImage")
    val userImage: String,

    @SerializedName("accessToken")
    val accessToken: String,

    @SerializedName("expiresAt")
    val expiresAt: Int,

    @SerializedName("refreshToken")
    val refreshToken: String
)

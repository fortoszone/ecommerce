package com.deva.ecommerce.model.remote.response

import com.google.gson.annotations.SerializedName

data class ProfileResponse(
    @SerializedName("code")
    var code: Int,

    @SerializedName("data")
    val data: ProfileData?,

    @SerializedName("message")
    val message: String
)

data class ProfileData(
    @SerializedName("userName")
    val userName: String,

    @SerializedName("userImage")
    val userImage: String?,
)

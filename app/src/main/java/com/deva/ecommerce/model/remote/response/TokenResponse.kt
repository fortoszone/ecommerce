package com.deva.ecommerce.model.remote.response

import com.google.gson.annotations.SerializedName

data class TokenResponse(

    @SerializedName("code")
    val code: Int,

    @SerializedName("data")
    val data: RegisterData,

    @SerializedName("message")
    val message: String
)

data class TokenData(
    @SerializedName("accessToken")
    val accessToken: String,

    @SerializedName("refreshToken")
    val refreshToken: String,

    @SerializedName("expiresAt")
    val expiresAt: Int

)

package com.deva.ecommerce.model.remote

import com.deva.ecommerce.model.remote.request.FulfillmentRequest
import com.deva.ecommerce.model.remote.request.RatingRequest
import com.deva.ecommerce.model.remote.request.TokenRequest
import com.deva.ecommerce.model.remote.request.UserRequest
import com.deva.ecommerce.model.remote.response.FulfillmentResponse
import com.deva.ecommerce.model.remote.response.LoginResponse
import com.deva.ecommerce.model.remote.response.PaymentResponse
import com.deva.ecommerce.model.remote.response.ProductDetailResponse
import com.deva.ecommerce.model.remote.response.ProductResponse
import com.deva.ecommerce.model.remote.response.ProductReviewResponse
import com.deva.ecommerce.model.remote.response.ProfileResponse
import com.deva.ecommerce.model.remote.response.RatingResponse
import com.deva.ecommerce.model.remote.response.RegisterResponse
import com.deva.ecommerce.model.remote.response.SearchResponse
import com.deva.ecommerce.model.remote.response.TokenResponse
import com.deva.ecommerce.model.remote.response.TransactionResponse
import com.deva.ecommerce.ui.main.store.SortCategory
import okhttp3.MultipartBody
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Multipart
import retrofit2.http.POST
import retrofit2.http.Part
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiService {
    @POST("/register")
    suspend fun register(@Body userRequest: UserRequest): Response<RegisterResponse>

    @POST("/login")
    suspend fun login(@Body userRequest: UserRequest): Response<LoginResponse>

    @POST("/refresh")
    suspend fun refresh(@Body tokenRequest: TokenRequest): Response<TokenResponse>

    @Multipart
    @Headers("Accept: application/json")
    @POST("/profile")
    fun addProfileInfo(
        @Part userName: MultipartBody.Part,
        @Part userImage: MultipartBody.Part?,
    ): Call<ProfileResponse>

    @POST("/products")
    suspend fun products(
        @Query("search") search: String?,
        @Query("brand") brand: String?,
        @Query("sort") sort: SortCategory?,
        @Query("highest") highest: Int?,
        @Query("lowest") lowest: Int?,
        @Query("page") page: Int?,
        @Query("limit") limit: Int?
    ): Response<ProductResponse>

    @POST("/search")
    fun search(
        @Query("query") query: String
    ): Call<SearchResponse>

    @GET("/products/{id}")
    suspend fun getProductDetail(
        @Path("id") id: String
    ): Response<ProductDetailResponse>

    @GET("/review/{id}")
    suspend fun getProductReview(
        @Path("id") id: String
    ): Response<ProductReviewResponse>

    @GET("/payment")
    fun getPayment(): Call<PaymentResponse>

    @POST("/fulfillment")
    fun getFulfillment(
        @Body fulfillmentRequest: FulfillmentRequest
    ): Call<FulfillmentResponse>

    @GET("/transaction")
    suspend fun getTransaction(): Response<TransactionResponse>

    @POST("/rating")
    fun rating(
        @Body ratingRequest: RatingRequest
    ): Call<RatingResponse>

    companion object {
        //        const val BASE_URL = "https://tokopaerbe.et.r.appspot.com/"
        const val BASE_URL = "http://192.168.1.20:5001/"

        const val API_KEY = "6f8856ed-9189-488f-9011-0ff4b6c08edc"
    }
}

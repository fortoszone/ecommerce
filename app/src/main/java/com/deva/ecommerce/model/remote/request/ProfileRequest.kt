package com.deva.ecommerce.model.remote.request

import com.google.gson.annotations.SerializedName
import java.io.File

data class ProfileRequest(
    @SerializedName("userName")
    val userName: String,
    @SerializedName("userImage")
    val userImage: File?
)

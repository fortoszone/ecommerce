package com.deva.ecommerce.model.local.room

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.deva.ecommerce.model.local.entity.Product

@Dao
interface WishlistDao {
    @Query("SELECT * FROM product_table")
    fun getWishlistedProduct(): LiveData<List<Product>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun addToWishlist(product: Product)

    @Delete
    suspend fun removeFromWishlist(product: Product)

    @Query("SELECT count(*) FROM product_table WHERE id = :id")
    fun checkWishlistIsFavorite(id: String): Boolean

    @Query("DELETE FROM product_table")
    suspend fun deleteAllWishlist()
}

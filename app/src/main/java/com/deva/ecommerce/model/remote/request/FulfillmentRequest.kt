package com.deva.ecommerce.model.remote.request

import android.os.Parcelable
import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

data class FulfillmentRequest(
    @SerializedName("payment")
    val payment: String,

    @SerializedName("items")
    val items: ArrayList<FulfillmentProduct>?
)

@Parcelize
@Keep
data class FulfillmentProduct(
    @SerializedName("productId")
    val id: String,

    @SerializedName("variantName")
    val variant: String,

    @SerializedName("quantity")
    val quantity: Int
) : Parcelable

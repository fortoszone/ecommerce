package com.deva.ecommerce.model.local.room

import androidx.room.Database
import androidx.room.RoomDatabase
import com.deva.ecommerce.model.local.entity.CartItem

@Database(entities = [CartItem::class], version = 5, exportSchema = true)
abstract class CartDatabase : RoomDatabase() {
    abstract fun cartDao(): CartDao
}

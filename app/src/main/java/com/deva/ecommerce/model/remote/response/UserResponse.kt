package com.deva.ecommerce.model.remote.response

import com.google.gson.annotations.SerializedName

data class UserResponse(

    @SerializedName("code")
    val code: Int,

    @SerializedName("data")
    val data: RegisterData,

    @SerializedName("message")
    val message: String
)

data class UserData(
    @SerializedName("userName")
    val name: String = "",

    @SerializedName("userImage")
    val image: Int = 0
)

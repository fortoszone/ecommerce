package com.deva.ecommerce.model.local.entity

data class Transaction(
    val date: String,
    val name: String,
    val price: Int,
    val quantity: Int,
    val review: String,
    val rating: Int
)

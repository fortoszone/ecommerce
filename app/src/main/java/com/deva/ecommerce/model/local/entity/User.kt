package com.deva.ecommerce.model.local.entity

import android.net.Uri

data class User(
    // val userID: String = UUID.randomUUID().toString(),
    var userName: String? = null,
    var userImage: Uri? = null,
    var refreshToken: String? = null,
    var accessToken: String? = null,
    var firebaseToken: String? = null,
)

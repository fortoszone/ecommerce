package com.deva.ecommerce.model.remote.response

import com.google.gson.annotations.SerializedName

data class RegisterResponse(

    @SerializedName("code")
    val code: Int,

    @SerializedName("data")
    val data: RegisterData?,

    @SerializedName("message")
    val message: String
)

data class RegisterData(

    @SerializedName("accessToken")
    val accessToken: String,

    @SerializedName("expiresAt")
    val expiresAt: Int,

    @SerializedName("refreshToken")
    val refreshToken: String
)

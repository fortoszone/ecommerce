package com.deva.ecommerce.model.remote.response

import com.google.gson.annotations.SerializedName

data class ProductResponse(
    @SerializedName("code")
    val code: Int,

    @SerializedName("data")
    val data: ProductData?,

    @SerializedName("message")
    val message: String
)

data class ProductData(
    @SerializedName("itemsPerPage")
    val itemsPerPage: Int,

    @SerializedName("currentItemCount")
    val currentItemCount: Int,

    @SerializedName("pageIndex")
    val pageIndex: Int,

    @SerializedName("totalPages")
    val totalPages: Int,

    @SerializedName("items")
    val items: List<ProductItems>
)

data class ProductItems(
    @SerializedName("productId")
    val id: String,

    @SerializedName("productName")
    val productName: String,

    @SerializedName("productPrice")
    val productPrice: Int,

    @SerializedName("image")
    val image: String,

    @SerializedName("brand")
    val brand: String,

    @SerializedName("store")
    val store: String,

    @SerializedName("sale")
    val sale: Int,

    @SerializedName("productRating")
    val productRating: Float,
)

package com.deva.ecommerce.model.remote.response

import com.google.gson.annotations.SerializedName

data class RatingResponse(
    @SerializedName("code")
    var code: Int,
    @SerializedName("message")
    val message: String
)

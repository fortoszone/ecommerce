package com.deva.ecommerce.model.remote.response

import com.google.gson.annotations.SerializedName

data class SearchResponse(
    @SerializedName("code")
    val code: Int,

    @SerializedName("data")
    val data: List<String>,

    @SerializedName("message")
    val message: String
)

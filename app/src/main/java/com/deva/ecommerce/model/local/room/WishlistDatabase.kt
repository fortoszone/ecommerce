package com.deva.ecommerce.model.local.room

import androidx.room.Database
import androidx.room.RoomDatabase
import com.deva.ecommerce.model.local.entity.Product

@Database(entities = [Product::class], version = 4, exportSchema = true)
abstract class WishlistDatabase : RoomDatabase() {
    abstract fun wishlistDao(): WishlistDao
}

package com.deva.ecommerce.model.remote.response

import com.google.gson.annotations.SerializedName

data class ProductReviewResponse(
    @SerializedName("code")
    val code: Int,

    @SerializedName("data")
    val data: List<ProductReviewData>,

    @SerializedName("message")
    val message: String
)

data class ProductReviewData(
    @SerializedName("userName")
    val userName: String,

    @SerializedName("userImage")
    val userImage: String,

    @SerializedName("userRating")
    val userRating: Int,

    @SerializedName("userReview")
    val userReview: String
)

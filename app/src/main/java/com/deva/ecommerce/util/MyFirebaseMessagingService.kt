package com.deva.ecommerce.util

import android.app.NotificationChannel
import android.app.NotificationManager
import android.media.RingtoneManager
import android.os.Build
import androidx.core.app.NotificationCompat
import androidx.navigation.NavDeepLinkBuilder
import com.deva.ecommerce.R
import com.deva.ecommerce.model.local.entity.Notification
import com.deva.ecommerce.model.local.room.NotificationDao
import com.deva.ecommerce.preference.AuthPreference
import com.google.android.material.badge.ExperimentalBadgeUtils
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import dagger.hilt.android.AndroidEntryPoint
import java.util.UUID
import javax.inject.Inject

@ExperimentalBadgeUtils
@AndroidEntryPoint
class MyFirebaseMessagingService :
    FirebaseMessagingService() {

    @Inject
    lateinit var notificationDao: NotificationDao

    private val authPreference: AuthPreference by lazy {
        AuthPreference(this)
    }

    override fun onMessageReceived(message: RemoteMessage) {
        super.onMessageReceived(message)
        sendNotification(message)
    }

    private fun sendNotification(message: RemoteMessage) {
        val title = message.data["title"] ?: ""
        val body = message.data["description"] ?: ""

        val pendingIntent = NavDeepLinkBuilder(this)
            .setGraph(R.navigation.mobile_navigation)
            .setDestination(R.id.navigation_notification)
            .createPendingIntent()

        val channelId = getString(R.string.transaction)
        val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        val notificationBuilder = NotificationCompat.Builder(this, channelId)
            .setSmallIcon(R.drawable.ic_launcher_foreground)
            .setContentTitle(title)
            .setContentText(body)
            .setAutoCancel(true)
            .setSound(defaultSoundUri)
            .setContentIntent(pendingIntent)
            .setStyle(
                NotificationCompat.BigTextStyle()
                    .bigText(body)
            )

        val notificationManager =
            getSystemService(NOTIFICATION_SERVICE) as NotificationManager

        // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(
                channelId,
                getString(R.string.transaction),
                NotificationManager.IMPORTANCE_DEFAULT,
            )
            notificationManager.createNotificationChannel(channel)
        }

        val notificationId = System.currentTimeMillis().toInt()
        notificationManager.notify(notificationId, notificationBuilder.build())

        val notifEntity = Notification(
            title = title,
            body = message.data["description"] ?: "",
            date = "${message.data["date"]}, ${message.data["time"]}",
            id = UUID.randomUUID().toString(),
            image = message.data["image"] ?: "",
            type = message.data["type"] ?: "",
            isRead = false
        )

        notificationDao.addToNotification(notifEntity)
    }

    override fun onNewToken(token: String) {
        super.onNewToken(token)
        authPreference.saveFirebaseToken(token)
    }
}

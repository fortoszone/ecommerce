package com.deva.ecommerce.network.interceptor

import com.chuckerteam.chucker.api.ChuckerInterceptor
import com.deva.ecommerce.model.remote.ApiService
import com.deva.ecommerce.model.remote.ApiService.Companion.API_KEY
import com.deva.ecommerce.model.remote.ApiService.Companion.BASE_URL
import com.deva.ecommerce.model.remote.request.TokenRequest
import com.deva.ecommerce.model.remote.response.LoginResponse
import com.deva.ecommerce.model.remote.response.TokenResponse
import com.deva.ecommerce.preference.SessionManager
import com.google.gson.Gson
import kotlinx.coroutines.runBlocking
import okhttp3.Authenticator
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response
import okhttp3.Route
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Inject

class TokenAuthenticator @Inject constructor(
    private val sessionManager: SessionManager,
    private val chuckInterceptor: ChuckerInterceptor,

) : Authenticator {
    override fun authenticate(route: Route?, response: Response): Request? {
        val refreshToken = sessionManager.fetchRefreshToken()

        synchronized(this) {
            return runBlocking {
                try {
                    val newToken = refreshToken(TokenRequest(refreshToken!!))
                    sessionManager.saveRefreshToken(newToken.data.refreshToken)
                    sessionManager.saveAccessToken(newToken.data.accessToken)
                    response.request
                        .newBuilder()
                        .header("Authorization", "Bearer ${newToken.data.accessToken}")
                        .build()
                } catch (error: Throwable) {
                    response.close()
                    null
                }
            }
        }
    }

    private suspend fun refreshToken(tokenRequest: TokenRequest): TokenResponse {
        val interceptor = Interceptor.invoke { chain ->
            val request = chain
                .request()
                .newBuilder()
                .addHeader("API_KEY", API_KEY)
                .build()
            chain.proceed(request)
        }

        val okHttpClient = OkHttpClient.Builder()
            .addInterceptor(chuckInterceptor)
            .addInterceptor(interceptor)
            .build()

        val apiService = Retrofit.Builder()
            .baseUrl(BASE_URL)
            .client(okHttpClient)
            .addConverterFactory(GsonConverterFactory.create())
            .build().create(ApiService::class.java)

        val newRequest = apiService.refresh(tokenRequest)

        if (newRequest.isSuccessful) {
            sessionManager.saveAccessToken(newRequest.body()!!.data.accessToken)
            sessionManager.saveRefreshToken(newRequest.body()!!.data.refreshToken)
        } else {
            val error =
                Gson().fromJson(newRequest.errorBody()!!.string(), LoginResponse::class.java)
            throw Exception(error.message)
        }

        return newRequest.body() ?: throw Exception("Data Empty")
    }
}

package com.deva.ecommerce.network.interceptor

import com.deva.ecommerce.model.remote.ApiService
import com.deva.ecommerce.preference.SessionManager
import okhttp3.Interceptor
import okhttp3.Response
import javax.inject.Inject

class AuthInterceptor @Inject constructor(
    private val sessionManager: SessionManager
) : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()

        val modifiedRequest = when (request.url.encodedPath) {
            "/login", "/register", "/refresh" -> {
                request
                    .newBuilder()
                    .addHeader("API_KEY", ApiService.API_KEY)
                    .build()
            }

            else -> {
                val accessToken = sessionManager.fetchAccessToken()
                request
                    .newBuilder()
                    .addHeader("Authorization", "Bearer $accessToken")
                    .build()
            }
        }
        return chain.proceed(modifiedRequest)
    }
}

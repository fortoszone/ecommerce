package com.deva.ecommerce.network.di

import android.app.Application
import androidx.room.Room
import com.deva.ecommerce.model.local.room.CartDao
import com.deva.ecommerce.model.local.room.CartDatabase
import com.deva.ecommerce.model.local.room.NotificationDao
import com.deva.ecommerce.model.local.room.NotificationDatabase
import com.deva.ecommerce.model.local.room.WishlistDao
import com.deva.ecommerce.model.local.room.WishlistDatabase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class RoomModule {
    @Provides
    @Singleton
    fun provideWishlistDatabase(application: Application): WishlistDatabase {
        return Room.databaseBuilder(
            application,
            WishlistDatabase::class.java,
            "wishlist_db"
        ).fallbackToDestructiveMigration().build()
    }

    @Provides
    fun provideWishlistDao(database: WishlistDatabase): WishlistDao {
        return database.wishlistDao()
    }

    @Provides
    @Singleton
    fun provideCartDatabase(application: Application): CartDatabase {
        return Room.databaseBuilder(
            application,
            CartDatabase::class.java,
            "cart_db"
        ).fallbackToDestructiveMigration().build()
    }

    @Provides
    fun provideCartDao(database: CartDatabase): CartDao {
        return database.cartDao()
    }

    @Provides
    @Singleton
    fun provideNotificationDatabase(application: Application): NotificationDatabase {
        return Room.databaseBuilder(
            application,
            NotificationDatabase::class.java,
            "notification_db"
        ).fallbackToDestructiveMigration().build()
    }

    @Provides
    fun provideNotificationDao(database: NotificationDatabase): NotificationDao {
        return database.notificationDao()
    }
}

package com.deva.ecommerce.network.interceptor

import android.content.Context
import android.content.Intent
import com.deva.ecommerce.preference.AuthPreference
import com.deva.ecommerce.preference.SessionManager
import com.deva.ecommerce.ui.prelogin.login.LoginActivity
import com.google.android.material.badge.ExperimentalBadgeUtils
import dagger.hilt.android.qualifiers.ApplicationContext
import okhttp3.Interceptor
import okhttp3.Response
import javax.inject.Inject

@ExperimentalBadgeUtils
class ErrorInterceptor @Inject constructor(
    private val sessionManager: SessionManager,
    private val authPreference: AuthPreference,
    @ApplicationContext private val context: Context
) :
    Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()

        val response = chain.proceed(request)

        if (response.code == 401) {
            sessionManager.clearSession()
            authPreference.deleteStatus()

            val intent = Intent(context, LoginActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
            context.startActivity(intent)

            return response
        }

        return response
    }
}

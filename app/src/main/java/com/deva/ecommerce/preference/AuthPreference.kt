package com.deva.ecommerce.preference

import android.content.Context
import android.content.SharedPreferences
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Inject

class AuthPreference @Inject constructor(@ApplicationContext context: Context) {
    private val prefs: SharedPreferences =
        context.getSharedPreferences("authPrefs", Context.MODE_PRIVATE)

    private val onboardingPrefs: SharedPreferences =
        context.getSharedPreferences("onboardingPrefs", Context.MODE_PRIVATE)

    companion object {
        const val IS_LOGIN = "isLogin"
        const val IS_ONBOARDED = "isOnboarded"
        const val IS_NAME_AND_IMAGE_FILLED = "isNameAndImageFilled"
    }

    fun deleteStatus() {
        val sp = prefs.edit()
        sp.clear()
        sp.apply()
    }

    fun saveLoginStatus(isLoggedIn: Boolean) {
        val sp = prefs.edit()
        sp.putBoolean(IS_LOGIN, isLoggedIn)
        sp.apply()
    }

    fun isLoggedIn(): Boolean {
        return prefs.getBoolean(IS_LOGIN, false)
    }

    fun saveNameAndImageFilled(nameAndImageFilled: Boolean) {
        val sp = prefs.edit()
        sp.putBoolean(IS_NAME_AND_IMAGE_FILLED, nameAndImageFilled)
        sp.apply()
    }

    fun isNameAndImageFilled(): Boolean {
        return prefs.getBoolean(IS_NAME_AND_IMAGE_FILLED, false)
    }

    fun saveOnboardedStatus(isOnboarded: Boolean) {
        val sp = onboardingPrefs.edit()
        sp.putBoolean(IS_ONBOARDED, isOnboarded)
        sp.apply()
    }

    fun isOnboarded(): Boolean {
        return onboardingPrefs.getBoolean(IS_ONBOARDED, false)
    }

    fun saveFirebaseToken(token: String) {
        val sp = prefs.edit()
        sp.putString("firebase_token", token)
        sp.apply()
    }

    fun fetchFirebaseToken(): String? {
        return prefs.getString("firebase_token", null)
    }
}

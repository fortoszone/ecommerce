package com.deva.ecommerce.preference

import android.content.Context
import android.content.SharedPreferences
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Inject

class SessionManager @Inject constructor(@ApplicationContext context: Context) {
    private val prefs: SharedPreferences =
        context.getSharedPreferences("userPrefs", Context.MODE_PRIVATE)

    companion object {
        const val ACCESS_TOKEN = "access_token"
        const val REFRESH_TOKEN = "refresh_token"
        const val USER_NAME = "user_name"
    }

    fun saveUserName(userName: String?) {
        val sp = prefs.edit()
        sp.putString(USER_NAME, userName)
        sp.apply()
    }

    fun fetchUserName(): String? {
        return prefs.getString(USER_NAME, null)
    }

    fun saveAccessToken(token: String?) {
        val sp = prefs.edit()
        sp.putString(ACCESS_TOKEN, token)
        sp.apply()
    }

    fun saveRefreshToken(token: String?) {
        val sp = prefs.edit()
        sp.putString(REFRESH_TOKEN, token)
        sp.apply()
    }

    fun fetchAccessToken(): String? {
        return prefs.getString(ACCESS_TOKEN, null)
    }

    fun fetchRefreshToken(): String? {
        return prefs.getString(REFRESH_TOKEN, null)
    }

    fun saveTheme(isDarkMode: Boolean) {
        val sp = prefs.edit()
        sp.putBoolean("isDarkMode", isDarkMode)
        sp.apply()
    }

    fun isDarkMode(): Boolean {
        return prefs.getBoolean("isDarkMode", false)
    }

    fun clearSession() {
        val sp = prefs.edit()
        sp.clear()
        sp.apply()
    }
}

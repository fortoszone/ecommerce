package com.deva.ecommerce.ui.main.cart

import android.content.Context
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.deva.ecommerce.model.local.entity.CartItem
import com.deva.ecommerce.model.local.room.CartDao
import com.deva.ecommerce.model.local.room.CartDatabase
import com.deva.ecommerce.util.MainDispatcherRule
import com.deva.ecommerce.util.getOrAwaitValue
import junit.framework.TestCase.assertEquals
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import javax.inject.Inject


@RunWith(AndroidJUnit4::class)
class CartViewModelTest {
    @Inject
    private lateinit var cartViewModel: CartViewModel

    @Inject
    lateinit var cartDao: CartDao

    @Inject
    lateinit var cartDatabase: CartDatabase

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    val dispatcherRule = MainDispatcherRule()

    private val cartItem = mutableListOf(
        CartItem(
            "1",
            "image.png",
            "Lenovo Thinkpad X280",
            "32GB",
            10,
            4000000,
            70,
            false,
        ),
        CartItem(
            "2",
            "image.png",
            "Lenovo Thinkpad X280",
            "32GB",
            10,
            4000000,
            70,
            false,
        ),
        CartItem(
            "3",
            "image.png",
            "Lenovo Thinkpad X280",
            "32GB",
            10,
            4000000,
            70,
            false,
        )
    )

    @Before
    fun setUp() {
        val context = ApplicationProvider.getApplicationContext<Context>()
        cartDatabase = Room.inMemoryDatabaseBuilder(
            context, CartDatabase::class.java
        ).build()
        cartDao = cartDatabase.cartDao()

        cartViewModel = CartViewModel(cartDao)
    }

    @Test
    fun isEveryProductSelected() = runTest {
        cartItem.forEach {
            cartDao.insertCartItem(it)
            it.isSelected = true
            cartDao.updateCartItem(it)
        }

        cartDao.updateAllSelectedProduct(true)
        assertEquals(true, cartViewModel.isAnyProductSelected.getOrAwaitValue())
    }

    @Test
    fun getCartItem() = runTest {
        for (item in cartItem) {
            cartDao.insertCartItem(item)
        }

        assertEquals(3, cartViewModel.getCartItem().getOrAwaitValue().size)
    }

    @Test
    fun removeCartItem() = runTest {
        cartItem.forEach {
            cartDao.insertCartItem(it)
        }

        cartDao.deleteCartItem(cartItem[0])
        cartDao.deleteCartItem(cartItem[1])
        assertEquals(1, cartViewModel.getCartItem().getOrAwaitValue().size)

    }

    @Test
    fun addCartItem() = runTest {
        cartItem.forEach {
            cartDao.insertCartItem(it)
        }

        cartDao.insertCartItem(
            CartItem(
                "4",
                "image.png",
                "Lenovo Thinkpad X280",
                "32GB",
                10,
                4000000,
                70,
                false,
            )
        )

        assertEquals(4, cartViewModel.getCartItem().getOrAwaitValue().size)
    }

    @Test
    fun updateSelectedAllProduct() = runTest {
        cartItem.forEach {
            cartDao.insertCartItem(it)
        }

        cartDao.updateAllSelectedProduct(true)

        for (item in cartViewModel.getCartItem().getOrAwaitValue()) {
            assertEquals(true, item.isSelected)
        }
    }

    @Test
    fun updateCart() = runTest {
        cartItem.forEach {
            cartDao.insertCartItem(it)
        }
        cartItem[0].isSelected = true
        cartDao.updateCartItem(cartItem[0])
        assertEquals(true, cartViewModel.getCartItem().getOrAwaitValue()[0].isSelected)
    }

    @Test
    fun deleteAllSelectedProduct() = runTest {
        cartItem.forEach {
            cartDao.insertCartItem(it)
        }

        cartItem[1].isSelected = true
        cartDao.updateCartItem(cartItem[1])
        cartDao.deleteAllSelectedProduct()
        assertEquals(2, cartViewModel.getCartItem().getOrAwaitValue().size)
    }
}
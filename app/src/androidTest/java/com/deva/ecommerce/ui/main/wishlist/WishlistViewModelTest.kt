package com.deva.ecommerce.ui.main.wishlist

import android.content.Context
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import com.deva.ecommerce.model.local.entity.Product
import com.deva.ecommerce.model.local.room.CartDao
import com.deva.ecommerce.model.local.room.CartDatabase
import com.deva.ecommerce.model.local.room.WishlistDao
import com.deva.ecommerce.model.local.room.WishlistDatabase
import com.deva.ecommerce.util.MainDispatcherRule
import com.deva.ecommerce.util.getOrAwaitValue
import junit.framework.TestCase.assertEquals
import kotlinx.coroutines.test.runTest
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class WishlistViewModelTest {
    private lateinit var wishlistViewModel: WishlistViewModel
    private lateinit var wishlistDao: WishlistDao
    private lateinit var wishlistDatabase: WishlistDatabase

    private lateinit var cartDatabase: CartDatabase
    private lateinit var cartDao: CartDao


    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    val dispatcherRule = MainDispatcherRule()

    @Before
    fun setUp() {
        val context = ApplicationProvider.getApplicationContext<Context>()
        wishlistDatabase = Room.inMemoryDatabaseBuilder(
            context, WishlistDatabase::class.java
        ).build()
        wishlistDao = wishlistDatabase.wishlistDao()

        cartDatabase = Room.inMemoryDatabaseBuilder(
            context, CartDatabase::class.java
        ).build()
        cartDao = cartDatabase.cartDao()

        wishlistViewModel = WishlistViewModel(wishlistDao, cartDao)
    }

    @After
    fun tearDown() {
        wishlistDatabase.close()
        cartDatabase.close()
    }

    private val wishlist = mutableListOf(
        Product(
            "1",
            "image1",
            "Lenovo Legion 3",
            10000000,
            "LenovoStore",
            4f,
            7,
            12,
            "Lenovo Legion 3",
            true,
            5,
            100,
            "RAM 16GB"
        ), Product(
            "2",
            "image2",
            "Lenovo Legion 5",
            10000000,
            "LenovoStore",
            4f,
            7,
            12,
            "Lenovo Legion 5",
            true,
            5,
            100,
            "RAM 16GB"
        ), Product(
            "3",
            "image3",
            "Lenovo Legion 7",
            10000000,
            "LenovoStore",
            4f,
            7,
            12,
            "Lenovo Legion 7",
            true,
            5,
            100,
            "RAM 16GB"
        )
    )

    @Test
    fun getWishlistedProduct() = runTest {
        wishlist.forEach {
            wishlistDao.addToWishlist(it)
        }

        val wishlistedProduct = wishlistViewModel.getWishlistedProduct().getOrAwaitValue()

        assertEquals(wishlist.size, wishlistedProduct.size)

    }

    @Test
    fun deleteFromWishlist() = runTest {
        wishlist.forEach {
            wishlistDao.addToWishlist(it)
        }

        wishlistViewModel.deleteFromWishlist(wishlist[0])

        val wishlistedProduct = wishlistViewModel.getWishlistedProduct().getOrAwaitValue()

        assertEquals(wishlist.size - 1, wishlistedProduct.size)
    }

    @Test
    fun addToCart() = runTest {
        wishlist.forEach {
            wishlistDao.addToWishlist(it)
        }

        wishlistViewModel.addToCart(wishlist[0])

        cartDao.getAllCartItems().getOrAwaitValue().forEach {
            assertEquals(wishlist[0].id, it.id)
        }
    }
}
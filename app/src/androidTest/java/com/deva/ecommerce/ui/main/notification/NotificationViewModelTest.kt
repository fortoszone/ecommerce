package com.deva.ecommerce.ui.main.notification

import android.content.Context
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.deva.ecommerce.model.local.entity.Notification
import com.deva.ecommerce.model.local.room.NotificationDao
import com.deva.ecommerce.model.local.room.NotificationDatabase
import com.deva.ecommerce.util.MainDispatcherRule
import com.deva.ecommerce.util.getOrAwaitValue
import junit.framework.TestCase.assertEquals
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class NotificationViewModelTest {
    private lateinit var notificationViewModel: NotificationViewModel
    private lateinit var notificationDao: NotificationDao
    private lateinit var notificationDatabase: NotificationDatabase

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    val dispatcherRule = MainDispatcherRule()

    @Before
    fun setUp() {
        val context = ApplicationProvider.getApplicationContext<Context>()
        notificationDatabase = Room.inMemoryDatabaseBuilder(
            context, NotificationDatabase::class.java
        ).build()
        notificationDao = notificationDatabase.notificationDao()

        notificationViewModel = NotificationViewModel(notificationDao)
    }

    @After
    fun tearDown() {
        notificationDatabase.close()
    }

    private val notificationList = mutableListOf(
        Notification(
            "1",
            "Info",
            "22 June",
            "Transaksi Berhasil",
            "Transaksi berhasil dilakukan, silahkan cek riwayat transaksi untuk melihat detail transaksi",
            "image.png",
            false,
        ),
        Notification(
            "2",
            "Info",
            "22 June",
            "Transaksi Berhasil",
            "Transaksi berhasil dilakukan, silahkan cek riwayat transaksi untuk melihat detail transaksi",
            "image.png",
            false,
        ),
        Notification(
            "3",
            "Info",
            "22 June",
            "Transaksi Berhasil",
            "Transaksi berhasil dilakukan, silahkan cek riwayat transaksi untuk melihat detail transaksi",
            "image.png",
            false,
        )
    )

    @Test
    fun getNotificationList() {
        notificationList.forEach {
            notificationDao.addToNotification(it)
        }

        assertEquals(
            notificationList.size,
            notificationViewModel.getNotificationList().getOrAwaitValue().size
        )

    }

    @Test
    fun updateNotification() {
        notificationList.forEach {
            notificationDao.addToNotification(it)
        }

        notificationViewModel.updateNotification(notificationList[0])

        assertEquals(
            notificationList[0].isRead,
            notificationViewModel.getNotificationList().getOrAwaitValue()[0].isRead
        )
    }
}
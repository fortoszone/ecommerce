package com.deva.ecommerce.preference

import android.content.Context
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import junit.framework.TestCase.assertTrue
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import javax.inject.Inject

@RunWith(AndroidJUnit4::class)
class SessionManagerTest {

    @Inject
    lateinit var sessionManager: SessionManager

    @Before
    fun setUp() {
        val context: Context = ApplicationProvider.getApplicationContext()
        sessionManager = SessionManager(context)
    }

    @After
    fun tearDown() {
        sessionManager.clearSession()
    }

    @Test
    fun testLoginStatus() {
        sessionManager.saveUserName("deva")
        assertTrue(sessionManager.fetchUserName() == "deva")
    }

    @Test
    fun testSaveAccessToken() {
        sessionManager.saveAccessToken("token")
        assertTrue(sessionManager.fetchAccessToken() == "token")
    }

    @Test
    fun testSaveRefreshToken() {
        sessionManager.saveRefreshToken("token")
        assertTrue(sessionManager.fetchRefreshToken() == "token")
    }

    @Test
    fun testDeleteUser() {
        sessionManager.clearSession()
        assertTrue(sessionManager.fetchUserName().isNullOrEmpty())
        assertTrue(sessionManager.fetchAccessToken().isNullOrEmpty())
        assertTrue(sessionManager.fetchRefreshToken().isNullOrEmpty())
    }
}
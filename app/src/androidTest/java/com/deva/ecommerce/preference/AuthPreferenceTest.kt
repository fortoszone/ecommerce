package com.deva.ecommerce.preference

import android.content.Context
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.deva.ecommerce.preference.AuthPreference
import junit.framework.TestCase
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import javax.inject.Inject

@RunWith(AndroidJUnit4::class)
class AuthPreferenceTest(
) {
    @Inject
    lateinit var authPreference: AuthPreference

    @Before
    fun setUp() {
        val context: Context = ApplicationProvider.getApplicationContext()
        authPreference = AuthPreference(context)
    }

    // Robolectric

    @After
    fun tearDown() {
        authPreference.deleteStatus()
    }

    @Test
    fun testLoginStatus() {
        authPreference.saveLoginStatus(true)
        TestCase.assertTrue(authPreference.isLoggedIn())
    }

    @Test
    fun testOnboardedStatus() {
        authPreference.saveOnboardedStatus(true)
        TestCase.assertTrue(authPreference.isOnboarded())
    }

    @Test
    fun testSaveNameAndImageFilled() {
        authPreference.saveNameAndImageFilled(true)
        TestCase.assertTrue(authPreference.isNameAndImageFilled())
    }

    @Test
    fun testSaveFirebaseToken() {
        authPreference.saveFirebaseToken("token")
        TestCase.assertEquals("token", authPreference.fetchFirebaseToken())
    }

    @Test
    fun deleteStatus() {
        authPreference.deleteStatus()
        TestCase.assertFalse(authPreference.isLoggedIn())
        TestCase.assertFalse(authPreference.isNameAndImageFilled())
    }
}
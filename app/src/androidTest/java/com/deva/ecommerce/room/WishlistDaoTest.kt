package com.deva.ecommerce.room

import android.content.Context
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.deva.ecommerce.model.local.entity.Product
import com.deva.ecommerce.model.local.room.WishlistDao
import com.deva.ecommerce.model.local.room.WishlistDatabase
import com.deva.ecommerce.util.getOrAwaitValue
import junit.framework.TestCase
import kotlinx.coroutines.test.runTest
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import java.io.IOException

@RunWith(AndroidJUnit4::class)
class WishlistTest {
    private lateinit var wishlistDao: WishlistDao
    private lateinit var wishlistDatabase: WishlistDatabase

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    @Before
    fun createDb() {
        val context = ApplicationProvider.getApplicationContext<Context>()
        wishlistDatabase = Room.inMemoryDatabaseBuilder(
            context, WishlistDatabase::class.java
        ).build()
        wishlistDao = wishlistDatabase.wishlistDao()
    }

    @After
    @Throws(IOException::class)
    fun closeDb() {
        wishlistDatabase.close()
    }

    @Test
    fun insertWishlist() = runTest {
        val wishlist = Product(
            "1",
            "image.png",
            "Lenovo Thinkpad X280",
            4000000,
            "Lenovo Official Store",
            4f,
            70,
            20,
            "Lenovo Thinkpad X280",
            false,
            16,
            100,
            "32GB"
        )

        wishlistDao.addToWishlist(wishlist)
        TestCase.assertEquals(1, wishlistDao.getWishlistedProduct().getOrAwaitValue().size)

    }

    @Test
    @Throws(Exception::class)
    fun deleteWishlist() = runTest {
        val wishlist = Product(
            "1",
            "image.png",
            "Lenovo Thinkpad X280",
            4000000,
            "Lenovo Official Store",
            4f,
            70,
            20,
            "Lenovo Thinkpad X280",
            false,
            16,
            100,
            "32GB"
        )

        wishlistDao.addToWishlist(wishlist)
        wishlistDao.removeFromWishlist(wishlist)
        TestCase.assertEquals(0, wishlistDao.getWishlistedProduct().getOrAwaitValue().size)
    }

    @Test
    @Throws(Exception::class)
    fun getWishlist() = runTest {
        val wishlist = Product(
            "1",
            "image.png",
            "Lenovo Thinkpad X280",
            4000000,
            "Lenovo Official Store",
            4f,
            70,
            20,
            "Lenovo Thinkpad X280",
            false,
            16,
            100,
            "32GB"
        )

        wishlistDao.addToWishlist(wishlist)
        TestCase.assertEquals(1, wishlistDao.getWishlistedProduct().getOrAwaitValue().size)

    }

    @Test
    @Throws(Exception::class)
    fun deleteAllWishlist() = runTest {
        val wishlist = Product(
            "1",
            "image.png",
            "Lenovo Thinkpad X280",
            4000000,
            "Lenovo Official Store",
            4f,
            70,
            20,
            "Lenovo Thinkpad X280",
            false,
            16,
            100,
            "32GB"
        )

        wishlistDao.addToWishlist(wishlist)
        wishlistDao.deleteAllWishlist()
        TestCase.assertEquals(0, wishlistDao.getWishlistedProduct().getOrAwaitValue().size)
    }

    @Test
    @Throws(Exception::class)
    fun checkWishlist() = runTest {
        val wishlist = Product(
            "1",
            "image.png",
            "Lenovo Thinkpad X280",
            4000000,
            "Lenovo Official Store",
            4f,
            70,
            20,
            "Lenovo Thinkpad X280",
            false,
            16,
            100,
            "32GB"
        )

        wishlistDao.addToWishlist(wishlist)
        TestCase.assertEquals(true, wishlistDao.checkWishlistIsFavorite(wishlist.id))
    }
}
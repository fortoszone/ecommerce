package com.deva.ecommerce.room

import android.content.Context
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.deva.ecommerce.model.local.entity.Notification
import com.deva.ecommerce.model.local.room.NotificationDao
import com.deva.ecommerce.model.local.room.NotificationDatabase
import com.deva.ecommerce.util.getOrAwaitValue
import junit.framework.TestCase
import kotlinx.coroutines.test.runTest
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class NotificationDaoTest {
    private lateinit var notificationDao: NotificationDao
    private lateinit var notificationDatabase: NotificationDatabase

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    @Before
    fun createDb() {
        val context = ApplicationProvider.getApplicationContext<Context>()
        notificationDatabase = Room.inMemoryDatabaseBuilder(
            context, NotificationDatabase::class.java
        ).build()
        notificationDao = notificationDatabase.notificationDao()
    }

    @After
    fun closeDb() {
        notificationDatabase.close()
    }

    @Test
    fun insertNotification() = runTest {
        val notification = Notification(
            "1",
            "info",
            "23 June, 15:00",
            "Transaksi berhasil",
            "Transaksi berhasil dilakukan, silahkan cek riwayat transaksi untuk melihat detail transaksi",
            "image.png",
            false,
        )

        notificationDao.addToNotification(notification)
        TestCase.assertEquals(1, notificationDao.getNotification().getOrAwaitValue().size)

    }

    @Test
    fun deleteAllNotification() = runTest {
        val notification = mutableListOf(
            Notification(
                "1",
                "info",
                "23 June, 15:00",
                "Transaksi berhasil",
                "Transaksi berhasil dilakukan, silahkan cek riwayat transaksi untuk melihat detail transaksi",
                "image.png",
                false
            ),
            Notification(
                "2",
                "info",
                "23 June, 16:00",
                "Transaksi berhasil",
                "Transaksi berhasil dilakukan, silahkan cek riwayat transaksi untuk melihat detail transaksi",
                "image.png",
                false
            )
        )

        notificationDao.addToNotification(notification[0])
        notificationDao.addToNotification(notification[1])
        notificationDao.deleteAllNotification()
        TestCase.assertEquals(0, notificationDao.getNotification().getOrAwaitValue().size)
    }

    @Test
    fun getUnreadNotification() = runTest {
        val notification = mutableListOf(
            Notification(
                "1",
                "info",
                "23 June, 15:00",
                "Transaksi berhasil",
                "Transaksi berhasil dilakukan, silahkan cek riwayat transaksi untuk melihat detail transaksi",
                "image.png",
                false
            ),
            Notification(
                "2",
                "info",
                "23 June, 16:00",
                "Transaksi berhasil",
                "Transaksi berhasil dilakukan, silahkan cek riwayat transaksi untuk melihat detail transaksi",
                "image.png",
                false
            )
        )

        notificationDao.addToNotification(notification[0])
        notificationDao.addToNotification(notification[1])
        notification[1].isRead = true
        notificationDao.updateNotification(notification[1])
        TestCase.assertEquals(1, notificationDao.getUnreadNotification().getOrAwaitValue().size)
    }

    @Test
    fun markedNotificationAsRead() = runTest {
        val notification = Notification(
            "1",
            "info",
            "23 June, 15:00",
            "Transaksi berhasil",
            "Transaksi berhasil dilakukan, silahkan cek riwayat transaksi untuk melihat detail transaksi",
            "image.png",
            false,
        )

        notificationDao.addToNotification(notification)
        notification.isRead = true
        notificationDao.updateNotification(notification)

        TestCase.assertEquals(true, notificationDao.getNotification().getOrAwaitValue()[0].isRead)
    }

    @Test
    fun getNotification() = runTest {
        val notification = Notification(
            "1",
            "info",
            "23 June, 15:00",
            "Transaksi berhasil",
            "Transaksi berhasil dilakukan, silahkan cek riwayat transaksi untuk melihat detail transaksi",
            "image.png",
            false,
        )

        notificationDao.addToNotification(notification)

        TestCase.assertEquals(1, notificationDao.getNotification().getOrAwaitValue().size)
    }
}
package com.deva.ecommerce.room

import android.content.Context
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.deva.ecommerce.model.local.entity.CartItem
import com.deva.ecommerce.model.local.room.CartDao
import com.deva.ecommerce.model.local.room.CartDatabase
import com.deva.ecommerce.util.getOrAwaitValue
import junit.framework.TestCase
import kotlinx.coroutines.test.runTest
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class CartDaoTest {
    private lateinit var cartDao: CartDao
    private lateinit var cartDatabase: CartDatabase

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    @Before
    fun createDb() {
        val context = ApplicationProvider.getApplicationContext<Context>()
        cartDatabase = Room.inMemoryDatabaseBuilder(
            context, CartDatabase::class.java
        ).build()
        cartDao = cartDatabase.cartDao()

    }

    @After
    fun closeDb() {
        cartDatabase.close()

    }

    @Test
    fun insertCart() = runTest {
        val item = CartItem(
            "1",
            "image.png",
            "Lenovo Thinkpad X280",
            "32GB",
            10,
            4000000,
            70,
            false,
        )

        cartDao.insertCartItem(item)
        TestCase.assertEquals(1, cartDao.getAllCartItems().getOrAwaitValue().size)
    }

    @Test
    fun removeCart() = runTest {
        val item = CartItem(
            "1",
            "image.png",
            "Lenovo Thinkpad X280",
            "32GB",
            10,
            4000000,
            70,
            false,
        )

        cartDao.deleteCartItem(item)
        TestCase.assertEquals(0, cartDao.getAllCartItems().getOrAwaitValue().size)
    }

    @Test
    fun getCartItem() = runTest {
        val item = CartItem(
            "1",
            "image.png",
            "Lenovo Thinkpad X280",
            "32GB",
            10,
            4000000,
            70,
            false,
        )

        cartDao.insertCartItem(item)
        TestCase.assertEquals(1, cartDao.getAllCartItems().getOrAwaitValue().size)
    }

    @Test
    fun deleteCartItemById() = runTest {
        val item = mutableListOf(
            CartItem(
                "1",
                "image.png",
                "Lenovo Thinkpad X280",
                "32GB",
                10,
                4000000,
                70,
                false,
            ),
            CartItem(
                "2",
                "image.png",
                "Lenovo Thinkpad X260",
                "32GB",
                10,
                4000000,
                70,
                false,
            )
        )

        cartDao.insertCartItem(item[0])
        cartDao.insertCartItem(item[1])
        cartDao.deleteCartById(item[0].id)
        TestCase.assertEquals(1, cartDao.getAllCartItems().getOrAwaitValue().size)
    }

    @Test
    fun deleteAllCartItem() = runTest {
        val item = mutableListOf(
            CartItem(
                "1",
                "image.png",
                "Lenovo Thinkpad X280",
                "32GB",
                10,
                4000000,
                70,
                false,
            ),
            CartItem(
                "2",
                "image.png",
                "Lenovo Thinkpad X260",
                "32GB",
                10,
                4000000,
                70,
                false,
            )
        )

        cartDao.insertCartItem(item[0])
        cartDao.insertCartItem(item[1])
        cartDao.deleteAllCart()
        TestCase.assertEquals(0, cartDao.getAllCartItems().getOrAwaitValue().size)
    }

    @Test
    fun updateCartItemQty() = runTest {
        val item = CartItem(
            "1",
            "image.png",
            "Lenovo Thinkpad X280",
            "32GB",
            10,
            4000000,
            70,
            false,
        )

        cartDao.insertCartItem(item)
        item.quantity = 20
        cartDao.updateCartItem(item)
        TestCase.assertEquals(20, cartDao.getAllCartItems().getOrAwaitValue()[0].quantity)
    }

    @Test
    fun selectCartItem() = runTest {
        val item = CartItem(
            "1",
            "image.png",
            "Lenovo Thinkpad X280",
            "32GB",
            10,
            4000000,
            70,
            false,
        )

        cartDao.insertCartItem(item)
        item.isSelected = true
        cartDao.updateCartItem(item)
        TestCase.assertEquals(true, cartDao.getAllCartItems().getOrAwaitValue()[0].isSelected)
    }

    @Test
    fun getSelectedCartItem() = runTest {
        val item = mutableListOf(
            CartItem(
                "1",
                "image.png",
                "Lenovo Thinkpad X280",
                "32GB",
                10,
                4000000,
                70,
                false,
            ),
            CartItem(
                "2",
                "image.png",
                "Lenovo Thinkpad X260",
                "32GB",
                10,
                4000000,
                70,
                false,
            )
        )

        cartDao.insertCartItem(item[0])
        cartDao.insertCartItem(item[1])
        item[0].isSelected = true
        cartDao.updateCartItem(item[0])
        TestCase.assertEquals(true, cartDao.checkIsAnyProductSelected().getOrAwaitValue())
    }

    @Test
    fun deleteSelectedCartItem() = runTest {
        val item = mutableListOf(
            CartItem(
                "1",
                "image.png",
                "Lenovo Thinkpad X280",
                "32GB",
                10,
                4000000,
                70,
                false,
            ),
            CartItem(
                "2",
                "image.png",
                "Lenovo Thinkpad X260",
                "32GB",
                10,
                4000000,
                70,
                false,
            )
        )

        cartDao.insertCartItem(item[0])
        cartDao.insertCartItem(item[1])
        item[0].isSelected = true
        cartDao.updateCartItem(item[0])
        cartDao.deleteAllSelectedProduct()
        TestCase.assertEquals(1, cartDao.getAllCartItems().getOrAwaitValue().size)
    }

    @Test
    fun deleteCartItem() = runTest {
        val item = mutableListOf(
            CartItem(
                "1",
                "image.png",
                "Lenovo Thinkpad X280",
                "32GB",
                10,
                4000000,
                70,
                false,
            ),
            CartItem(
                "2",
                "image.png",
                "Lenovo Thinkpad X260",
                "32GB",
                10,
                4000000,
                70,
                false,
            )
        )

        cartDao.insertCartItem(item[0])
        cartDao.insertCartItem(item[1])
        cartDao.deleteCartItem(item[0])
        TestCase.assertEquals(1, cartDao.getAllCartItems().getOrAwaitValue().size)
    }
}
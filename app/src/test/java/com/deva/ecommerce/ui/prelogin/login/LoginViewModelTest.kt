package com.deva.ecommerce.ui.prelogin.login

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.deva.ecommerce.model.remote.ApiService
import com.deva.ecommerce.model.remote.request.UserRequest
import com.deva.ecommerce.model.remote.response.LoginResponse
import com.deva.ecommerce.preference.AuthPreference
import com.deva.ecommerce.preference.SessionManager
import com.deva.ecommerce.util.MainDispatcherRule
import com.deva.ecommerce.util.getOrAwaitValue
import junit.framework.TestCase.assertEquals
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.advanceUntilIdle
import kotlinx.coroutines.test.runTest
import okhttp3.mockwebserver.MockWebServer
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.kotlin.mock
import org.mockito.kotlin.whenever
import retrofit2.Response

@ExperimentalCoroutinesApi
class LoginViewModelTest {
    private lateinit var loginViewModel: LoginViewModel
    private lateinit var sessionManager: SessionManager
    private lateinit var authPreference: AuthPreference
    private lateinit var mockWebServer: MockWebServer

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    val dispatcherRule = MainDispatcherRule()

    @Mock
    private lateinit var apiService: ApiService

    private val request = UserRequest(
        "test@gmail.com",
        "12345678",
        "token"
    )

    @Before
    fun setUp() {
        mockWebServer = MockWebServer()
        mockWebServer.start()
        sessionManager = mock()
        authPreference = mock()

        apiService = Mockito.mock(ApiService::class.java)

        loginViewModel = LoginViewModel(sessionManager, authPreference, apiService)
    }

    @After
    fun tearDown() {
        mockWebServer.shutdown()
    }

    @Test
    fun `login test`() = runTest {
        val expectedResponse = LoginResponse(
            code = 200,
            data = null,
            message = "Success"
        )

        val successResponse = Response.success(expectedResponse)

        whenever(apiService.login(request)).thenReturn(successResponse)

        loginViewModel.login(
            "test@gmail.com",
            "12345678",
            "token"
        )

        advanceUntilIdle()

        assertEquals(
            200,
            loginViewModel.data.getOrAwaitValue().code
        )
    }

    @Test
    fun `login with error`() = runTest {
        val expectedResponse = LoginResponse(
            code = 400,
            data = null,
            message = "User already exists"
        )

        val successResponse = Response.success(expectedResponse)

        whenever(apiService.login(request)).thenReturn(successResponse)

        loginViewModel.login(
            "test@gmail.com",
            "12345678",
            "token"
        )

        advanceUntilIdle()

        assertEquals(
            400,
            loginViewModel.data.getOrAwaitValue().code
        )
    }
}

package com.deva.ecommerce.ui.main.transaction

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.deva.ecommerce.model.remote.ApiService
import com.deva.ecommerce.model.remote.response.TransactionResponse
import com.deva.ecommerce.util.MainDispatcherRule
import com.deva.ecommerce.util.getOrAwaitValue
import junit.framework.TestCase.assertEquals
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.advanceUntilIdle
import kotlinx.coroutines.test.runTest
import okhttp3.mockwebserver.MockWebServer
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.mockito.kotlin.mock
import org.mockito.kotlin.whenever
import retrofit2.Response

@ExperimentalCoroutinesApi
class TransactionViewModelTest {
    private lateinit var transactionViewModel: TransactionViewModel

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    val dispatcherRule = MainDispatcherRule()

    @Mock
    private lateinit var apiService: ApiService

    private lateinit var mockWebServer: MockWebServer

    @Before
    fun setUp() {
        mockWebServer = MockWebServer()
        mockWebServer.start()
        apiService = mock()
        transactionViewModel = TransactionViewModel(apiService)
    }

    @After
    fun tearDown() {
        mockWebServer.shutdown()
    }

    @Test
    fun getTransactionList() = runTest {
        val expectedResponse = TransactionResponse(
            code = 200,
            data = listOf(),
            message = "Success"
        )

        val successResponse = Response.success(expectedResponse)

        whenever(apiService.getTransaction()).thenReturn(successResponse)

        transactionViewModel.getTransactionList()

        advanceUntilIdle()

        assertEquals(expectedResponse, transactionViewModel.data.getOrAwaitValue())
    }
}

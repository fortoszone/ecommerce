package com.deva.ecommerce.ui.prelogin.profile

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.deva.ecommerce.model.remote.ApiService
import com.deva.ecommerce.preference.AuthPreference
import com.deva.ecommerce.preference.SessionManager
import com.deva.ecommerce.util.MainDispatcherRule
import kotlinx.coroutines.ExperimentalCoroutinesApi
import okhttp3.mockwebserver.MockWebServer
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.kotlin.mock

@ExperimentalCoroutinesApi
class ProfileViewModelTest {
    private lateinit var profileViewModel: ProfileViewModel
    private lateinit var sessionManager: SessionManager
    private lateinit var authPreference: AuthPreference
    private lateinit var mockWebServer: MockWebServer

    @Mock
    private lateinit var apiService: ApiService

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    val dispatcherRule = MainDispatcherRule()

    @Before
    fun setUp() {
        mockWebServer = MockWebServer()
        sessionManager = mock()
        authPreference = mock()
        mockWebServer.start()
        apiService = Mockito.mock(ApiService::class.java)

        profileViewModel = ProfileViewModel(apiService, authPreference)
    }

    @After
    fun tearDown() {
        mockWebServer.shutdown()
    }

    /*@Test
    fun addProfile() {
        val name = MultipartBody.Part.createFormData("userName", "name")
        val expectedResponse = ProfileResponse(
            200,
            ProfileData(
                "deva",
                "1d32ba79-e879-4425-a011-2da4281f1c1b-test.png"
            ),
            "OK"
        )

        val successResponse = Calls.response(expectedResponse)

        whenever(apiService.addProfileInfo(name, null)).thenReturn(successResponse)

        profileViewModel.addProfile(
            "deva",
            null
        )

        assertEquals(expectedResponse.code, profileViewModel.data.getOrAwaitValue().code)
    }*/
}

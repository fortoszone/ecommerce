package com.deva.ecommerce.ui.main.fulfillment

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.deva.ecommerce.model.remote.ApiService
import com.deva.ecommerce.model.remote.request.RatingRequest
import com.deva.ecommerce.model.remote.response.RatingResponse
import com.deva.ecommerce.util.MainDispatcherRule
import com.deva.ecommerce.util.getOrAwaitValue
import junit.framework.TestCase.assertEquals
import okhttp3.mockwebserver.MockWebServer
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.mockito.kotlin.mock
import org.mockito.kotlin.whenever
import retrofit2.mock.Calls

class FulfillmentViewModelTest {
    private lateinit var fulfillmentViewModel: FulfillmentViewModel

    @Mock
    private lateinit var apiService: ApiService

    private lateinit var mockWebServer: MockWebServer

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    val dispatcherRule = MainDispatcherRule()

    @Before
    fun setUp() {
        mockWebServer = MockWebServer()
        mockWebServer.start()
        apiService = mock()
        fulfillmentViewModel = FulfillmentViewModel(apiService)
    }

    @After
    fun tearDown() {
        mockWebServer.shutdown()
    }

    private val request = RatingRequest(
        id = "1",
        rating = 5,
        review = "Good"
    )

    @Test
    fun rating() {
        val expectedResponse = RatingResponse(
            code = 200,
            message = "Success"
        )

        val successResponse = Calls.response(expectedResponse)

        whenever(apiService.rating(request)).thenReturn(successResponse)

        fulfillmentViewModel.rating(
            id = "1",
            rating = 5,
            review = "Good"
        )

        assertEquals(expectedResponse.code, fulfillmentViewModel.message.getOrAwaitValue().code)
    }
}

package com.deva.ecommerce.ui.main.product.detail

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.deva.ecommerce.model.local.room.CartDao
import com.deva.ecommerce.model.local.room.WishlistDao
import com.deva.ecommerce.model.remote.ApiService
import com.deva.ecommerce.model.remote.response.ProductDetailData
import com.deva.ecommerce.model.remote.response.ProductDetailResponse
import com.deva.ecommerce.model.remote.response.ProductVariant
import com.deva.ecommerce.util.MainDispatcherRule
import com.deva.ecommerce.util.getOrAwaitValue
import junit.framework.TestCase.assertEquals
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.advanceUntilIdle
import kotlinx.coroutines.test.runTest
import okhttp3.mockwebserver.MockWebServer
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.mockito.kotlin.mock
import org.mockito.kotlin.whenever
import retrofit2.Response

@ExperimentalCoroutinesApi
class ProductDetailViewModelTest {
    private lateinit var productDetailViewModel: ProductDetailViewModel

    private lateinit var mockWebServer: MockWebServer

    @Rule
    @JvmField
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    @Rule
    @JvmField
    val dispatcherRule = MainDispatcherRule()

    @Mock
    private lateinit var apiService: ApiService

    @Mock
    private lateinit var cartDao: CartDao

    @Mock
    private lateinit var wishlistDao: WishlistDao

    @Before
    fun setUp() {
        mockWebServer = MockWebServer()
        mockWebServer.start()
        apiService = mock()
        wishlistDao = mock()
        cartDao = mock()
        productDetailViewModel = ProductDetailViewModel(apiService, wishlistDao, cartDao)
    }

    @Test
    fun getProduct() = runTest {
        val expectedResponse = ProductDetailResponse(
            code = 200,
            data = ProductDetailData(
                "17b4714d-527a-4be2-84e2-e4c37c2b3292",
                "ASUS ROG Strix G17 G713RM-R736H6G-O - Eclipse Gray",
                24499000,
                listOf(
                    "https://images.tokopedia.net/img/cache/900/VqbcmM/2022/4/6/0a49c399-cf6b-47f5-91c9-8cbd0b86462d.jpg",
                    "https://images.tokopedia.net/img/cache/900/VqbcmM/2022/3/25/0cc3d06c-b09d-4294-8c3f-1c37e60631a6.jpg",
                    "https://images.tokopedia.net/img/cache/900/VqbcmM/2022/3/25/33a06657-9f88-4108-8676-7adafaa94921.jpg"
                ),
                "Asus",
                "ROG Strix G17 G713RM-R736H6G-O - Eclipse Gray",
                "AsusStore",
                12,
                2,
                7,
                5,
                100,
                5.0f,
                listOf(
                    ProductVariant(
                        "16GB",
                        0,
                    ),
                    ProductVariant(
                        "32GB",
                        1000000,
                    )
                )
            ),
            message = "OK"
        )

        val successResponse = Response.success(expectedResponse)

        whenever(apiService.getProductDetail("id")).thenReturn(successResponse)

        productDetailViewModel.getProduct("id")

        advanceUntilIdle()

        assertEquals(
            expectedResponse.data.productId,
            productDetailViewModel.data.getOrAwaitValue().data.productId
        )
    }
}

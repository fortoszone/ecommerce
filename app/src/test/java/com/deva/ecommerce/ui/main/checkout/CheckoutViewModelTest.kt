package com.deva.ecommerce.ui.main.checkout

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.deva.ecommerce.model.local.room.CartDao
import com.deva.ecommerce.model.remote.ApiService
import com.deva.ecommerce.model.remote.request.FulfillmentProduct
import com.deva.ecommerce.model.remote.request.FulfillmentRequest
import com.deva.ecommerce.model.remote.response.FulfillmentDetail
import com.deva.ecommerce.model.remote.response.FulfillmentResponse
import com.deva.ecommerce.util.MainDispatcherRule
import com.deva.ecommerce.util.getOrAwaitValue
import junit.framework.TestCase.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.kotlin.mock
import org.mockito.kotlin.whenever
import retrofit2.mock.Calls

class CheckoutViewModelTest {
    private lateinit var cartDao: CartDao
    private lateinit var checkoutViewModel: CheckoutViewModel
    private lateinit var apiService: ApiService

    private val request = FulfillmentRequest(
        "Bank BCA",
        arrayListOf(
            FulfillmentProduct(
                "1",
                "32GB",
                10
            )
        )
    )

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    val mainDispatcherRule = MainDispatcherRule()

    @Before
    fun setUp() {
        cartDao = mock()
        apiService = mock()
        checkoutViewModel = CheckoutViewModel(apiService, cartDao)
    }

    @Test
    fun getFulfillment() {
        val expectedResponse = FulfillmentResponse(
            200,
            FulfillmentDetail(
                "ba47402c-d263-49d3-a1f8-759ae59fa4a1",
                true,
                "09 Jun 2023",
                "08:53",
                "Bank BCA",
                48998000,
                "",
                0
            ),
            "OK"
        )

        val successRequest = Calls.response(expectedResponse)

        whenever(apiService.getFulfillment(request)).thenReturn(successRequest)

        checkoutViewModel.getFulfillment(request)

        val actualResponse = checkoutViewModel.data.getOrAwaitValue()

        assertEquals(expectedResponse.code, actualResponse.code)
    }
}

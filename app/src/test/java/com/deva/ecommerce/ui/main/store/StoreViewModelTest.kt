package com.deva.ecommerce.ui.main.store

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import androidx.paging.PagingSource
import com.deva.ecommerce.model.remote.ApiService
import com.deva.ecommerce.model.remote.response.ProductItems
import com.deva.ecommerce.util.MainDispatcherRule
import junit.framework.TestCase.assertEquals
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.launch
import kotlinx.coroutines.test.advanceUntilIdle
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.mockito.kotlin.any
import org.mockito.kotlin.mock
import org.mockito.kotlin.whenever

@ExperimentalCoroutinesApi
class StoreViewModelTest {
    private lateinit var storeViewModel: StoreViewModel

    private lateinit var pager: Pager<Int, ProductItems>

    @Mock
    lateinit var apiService: ApiService

    @Mock
    private lateinit var mockPagingSource: PagingSource<Int, ProductItems>

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    val dispatcherRule = MainDispatcherRule()

    private val productItems = listOf(
        ProductItems(
            id = "1",
            productName = "Lenovo Legion 3",
            productPrice = 10000000,
            image = "image",
            brand = "Lenovo",
            store = "Lenovo Official Store",
            sale = 10,
            productRating = 4f
        ),
        ProductItems(
            id = "2",
            productName = "Lenovo Legion 5",
            productPrice = 12000000,
            image = "image",
            brand = "Lenovo",
            store = "Lenovo Official Store",
            sale = 3,
            productRating = 4f
        ),
        ProductItems(
            id = "3",
            productName = "Lenovo Thinkpad X280",
            productPrice = 5000000,
            image = "image",
            brand = "Lenovo",
            store = "Lenovo Official Store",
            sale = 20,
            productRating = 4f
        )
    )

    @Before
    fun setUp() {
        mockPagingSource = mock()

        pager = Pager(
            PagingConfig(
                pageSize = 10,
                initialLoadSize = 10,
                prefetchDistance = 1
            )
        ) {
            mockPagingSource
        }

        apiService = mock()

        storeViewModel = StoreViewModel(apiService)
    }

    @Test
    fun pagerTest() = runTest {
        val mockPagingData: PagingData<ProductItems> = PagingData.from(productItems)

        whenever(mockPagingSource.load(any())).thenReturn(
            PagingSource.LoadResult.Page(
                data = productItems,
                prevKey = null,
                nextKey = null
            )
        )

        val dataStream: Flow<PagingData<ProductItems>> = pager.flow
        val collectedData = mutableListOf<PagingData<ProductItems>>()
        val job = launch {
            dataStream.collect {
                collectedData.add(it)
            }
        }

        advanceUntilIdle()

        assertEquals(1, collectedData.size)

        job.cancel()
    }
}

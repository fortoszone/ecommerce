package com.deva.ecommerce.ui.main.product.review

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.deva.ecommerce.model.remote.ApiService
import com.deva.ecommerce.model.remote.response.ProductReviewResponse
import com.deva.ecommerce.util.MainDispatcherRule
import com.deva.ecommerce.util.getOrAwaitValue
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.advanceUntilIdle
import kotlinx.coroutines.test.runTest
import okhttp3.mockwebserver.MockWebServer
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.mockito.kotlin.mock
import org.mockito.kotlin.whenever
import retrofit2.Response

@ExperimentalCoroutinesApi
class ProductReviewViewModelTest {
    private lateinit var productReviewViewModel: ProductReviewViewModel

    private lateinit var mockWebServer: MockWebServer

    @Mock
    private lateinit var apiService: ApiService

    @Rule
    @JvmField
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    @Rule
    @JvmField
    val dispatcherRule = MainDispatcherRule()

    @Before
    fun setUp() {
        mockWebServer = MockWebServer()
        mockWebServer.start()
        apiService = mock()
        productReviewViewModel = ProductReviewViewModel(apiService)
    }

    @After
    fun tearDown() {
        mockWebServer.shutdown()
    }

    @Test
    fun getProductReview() = runTest {
        val expectedResponse = ProductReviewResponse(
            code = 200,
            data = listOf(),
            message = "Success"
        )

        val successResponse = Response.success(expectedResponse)

        whenever(apiService.getProductReview("id")).thenReturn(successResponse)

        productReviewViewModel.getProductReview("id")

        advanceUntilIdle()

        assertEquals(expectedResponse.data, productReviewViewModel.data.getOrAwaitValue().data)
    }
}

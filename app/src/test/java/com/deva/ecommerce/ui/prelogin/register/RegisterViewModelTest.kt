package com.deva.ecommerce.ui.prelogin.register

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.deva.ecommerce.model.remote.ApiService
import com.deva.ecommerce.model.remote.request.UserRequest
import com.deva.ecommerce.model.remote.response.RegisterResponse
import com.deva.ecommerce.preference.AuthPreference
import com.deva.ecommerce.preference.SessionManager
import com.deva.ecommerce.util.MainDispatcherRule
import com.deva.ecommerce.util.getOrAwaitValue
import junit.framework.TestCase.assertEquals
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.advanceUntilIdle
import kotlinx.coroutines.test.runTest
import okhttp3.mockwebserver.MockWebServer
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.kotlin.mock
import org.mockito.kotlin.whenever
import retrofit2.Response

@ExperimentalCoroutinesApi
class RegisterViewModelTest {

    private lateinit var registerViewModel: RegisterViewModel
    private lateinit var sessionManager: SessionManager
    private lateinit var authPreference: AuthPreference

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    val dispatcherRule = MainDispatcherRule()

    @Mock
    private lateinit var apiService: ApiService

    private lateinit var mockWebServer: MockWebServer

    private val request = UserRequest(
        "test@gmail.com",
        "12345678",
        "token"
    )

    @Before
    fun setUp() {
        mockWebServer = MockWebServer()
        mockWebServer.start()
        sessionManager = mock()
        authPreference = mock()

        apiService = Mockito.mock(ApiService::class.java)

        registerViewModel = RegisterViewModel(sessionManager, authPreference, apiService)
    }

    @After
    fun tearDown() {
        mockWebServer.shutdown()
    }

    @Test
    fun `register test`() = runTest {
        val expectedResponse = RegisterResponse(
            code = 200,
            data = null,
            message = "Success"
        )

        val successResponse = Response.success(expectedResponse)

        whenever(apiService.register(request)).thenReturn(successResponse)

        registerViewModel.register(
            "test@gmail.com",
            "12345678",
            "token"
        )

        advanceUntilIdle()

        assertEquals(
            200,
            registerViewModel.data.getOrAwaitValue().code
        )
    }

    @Test
    fun `register with error`() = runTest {
        val expectedResponse = RegisterResponse(
            code = 400,
            data = null,
            message = "User already exists"
        )

        val successResponse = Response.success(expectedResponse)

        whenever(apiService.register(request)).thenReturn(successResponse)

        registerViewModel.register(
            "test@gmail.com",
            "12345678",
            "token"
        )

        advanceUntilIdle()

        assertEquals(
            400,
            registerViewModel.data.getOrAwaitValue().code
        )
    }
}

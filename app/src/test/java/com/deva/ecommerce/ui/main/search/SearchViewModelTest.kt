package com.deva.ecommerce.ui.main.search

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.deva.ecommerce.model.remote.ApiService
import com.deva.ecommerce.model.remote.response.SearchResponse
import com.deva.ecommerce.util.MainDispatcherRule
import com.deva.ecommerce.util.getOrAwaitValue
import junit.framework.TestCase.assertEquals
import okhttp3.mockwebserver.MockWebServer
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.mockito.kotlin.mock
import org.mockito.kotlin.whenever
import retrofit2.mock.Calls

class SearchViewModelTest {
    private lateinit var searchViewModel: SearchViewModel

    @Mock
    private lateinit var apiService: ApiService

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    val dispatcherRule = MainDispatcherRule()

    private lateinit var mockWebServer: MockWebServer

    @Before
    fun setUp() {
        mockWebServer = MockWebServer()
        mockWebServer.start()
        apiService = mock()
        searchViewModel = SearchViewModel(apiService)
    }

    @After
    fun tearDown() {
        mockWebServer.shutdown()
    }

    @Test
    fun `get query test`() {
        val expectedResponse = SearchResponse(
            data = listOf(
                "Lenovo Legion 3",
                "Lenovo Legion 5",
                "Lenovo Legion 7",
                "Lenovo Ideapad 3",
                "Lenovo Ideapad 5",
                "Lenovo Ideapad 7"
            ),
            message = "Success",
            code = 200
        )

        val successResponse = Calls.response(expectedResponse)

        whenever(apiService.search("Lenovo")).thenReturn(successResponse)

        searchViewModel.getQueries("Lenovo")

        val actualValue = searchViewModel.data.getOrAwaitValue()

        assertEquals(expectedResponse.data.size, actualValue.size)
    }
}

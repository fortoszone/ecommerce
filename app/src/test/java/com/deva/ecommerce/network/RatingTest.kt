package com.deva.ecommerce.network

import com.deva.ecommerce.model.remote.ApiService
import com.deva.ecommerce.model.remote.request.RatingRequest
import com.deva.ecommerce.model.remote.response.RatingResponse
import com.deva.ecommerce.util.MainDispatcherRule
import com.google.gson.Gson
import junit.framework.TestCase.assertEquals
import okhttp3.OkHttpClient
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.net.HttpURLConnection

class RatingTest {
    private lateinit var apiService: ApiService

    private var mockWebServer: MockWebServer = MockWebServer()

    @get:Rule
    val mainDispatcherRule = MainDispatcherRule()

    val json = """
            {
                "code": "200",
                "message": "Success",
                "data": {
                
                }
            }
    """.trimIndent()

    @Before
    fun setUp() {
        mockWebServer.start()
        val retrofit = Retrofit.Builder()
            .baseUrl(mockWebServer.url("/"))
            .client(OkHttpClient.Builder().build())
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        apiService = retrofit.create(ApiService::class.java)
    }

    @After
    fun tearDown() {
        mockWebServer.shutdown()
    }

    @Test
    fun `test rating`() {
        val mockedResponse = MockResponse()
        mockedResponse.setResponseCode(HttpURLConnection.HTTP_OK)
        mockedResponse.setBody(json)

        mockWebServer.enqueue(mockedResponse)
        val expectedResponse = Gson().fromJson(json, RatingResponse::class.java)

        val response = apiService.rating(RatingRequest("1", 5, "Good")).execute()

        assertEquals(response.code(), expectedResponse.code)
    }

    @Test
    fun `test rating with error`() {
        val mockedResponse = MockResponse()
        mockedResponse.setResponseCode(HttpURLConnection.HTTP_BAD_REQUEST)
        mockedResponse.setBody(json)

        mockWebServer.enqueue(mockedResponse)
        val expectedResponse = Gson().fromJson(json, RatingResponse::class.java)
        expectedResponse.code = 400

        val response = apiService.rating(RatingRequest("1", 5, "Good")).execute()

        assertEquals(response.code(), expectedResponse.code)
    }

    @Test
    fun `test rating with exception`() {
        val mockedResponse = MockResponse()
        mockedResponse.setResponseCode(HttpURLConnection.HTTP_BAD_REQUEST)
        mockedResponse.setBody(json)

        mockWebServer.enqueue(mockedResponse)
        val expectedResponse = Gson().fromJson(json, RatingResponse::class.java)
        expectedResponse.code = 400

        val response = apiService.rating(RatingRequest("1", 5, "Good")).execute()

        assertEquals(response.code(), expectedResponse.code)
    }
}

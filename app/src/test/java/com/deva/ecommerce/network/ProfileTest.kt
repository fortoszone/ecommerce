package com.deva.ecommerce.network

import com.deva.ecommerce.model.remote.ApiService
import com.deva.ecommerce.model.remote.response.ProfileResponse
import com.deva.ecommerce.util.MainDispatcherRule
import com.google.gson.Gson
import junit.framework.TestCase.assertEquals
import kotlinx.coroutines.test.runTest
import okhttp3.MultipartBody
import okhttp3.OkHttpClient
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.net.HttpURLConnection

class ProfileTest {
    private lateinit var apiService: ApiService
    private var mockWebServer = MockWebServer()

    @get:Rule
    val mainDispatcherRule = MainDispatcherRule()

    val json = """
        {
            "code": 200,
            "message": "OK",
            "data": {
                "userName": "Test",
                "userImage": "1d32ba79-e879-4425-a011-2da4281f1c1b-test.png"
            }
        }

    """.trimIndent()

    @Before
    fun setUp() {
        mockWebServer.start()
        val retrofit = Retrofit.Builder()
            .baseUrl(mockWebServer.url("/"))
            .client(OkHttpClient.Builder().build())
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        apiService = retrofit.create(ApiService::class.java)
    }

    @After
    fun tearDown() {
        mockWebServer.shutdown()
    }

    @Test
    fun `test get profile`() {
        val mockedResponse = MockResponse()
        mockedResponse.setResponseCode(HttpURLConnection.HTTP_OK)
        mockedResponse.setBody(json)

        mockWebServer.enqueue(mockedResponse)

        val expectedResponse = Gson().fromJson(json, ProfileResponse::class.java)

        val name = MultipartBody.Part.createFormData("userName", "name")
        val actualResponse = apiService.addProfileInfo(name, null).execute()

        assertEquals(200, expectedResponse.code)
    }

    @Test
    fun `test profile username cannot be null`() = runTest {
        val mockedResponse = MockResponse()
        mockedResponse.setResponseCode(HttpURLConnection.HTTP_BAD_REQUEST)
        mockedResponse.setBody(json)

        mockWebServer.enqueue(mockedResponse)

        val expectedResponse = Gson().fromJson(json, ProfileResponse::class.java)
        expectedResponse.code = 400

        val name = MultipartBody.Part.createFormData("userName", "")
        val actualResponse = apiService.addProfileInfo(name, null).execute()

        assertEquals(expectedResponse.code, actualResponse.code())
    }
}

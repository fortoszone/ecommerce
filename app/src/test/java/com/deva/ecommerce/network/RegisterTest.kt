package com.deva.ecommerce.network

import com.deva.ecommerce.model.remote.ApiService
import com.deva.ecommerce.model.remote.request.UserRequest
import com.deva.ecommerce.util.MainDispatcherRule
import junit.framework.TestCase.assertEquals
import kotlinx.coroutines.test.runTest
import okhttp3.OkHttpClient
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.net.HttpURLConnection

class RegisterTest {
    private lateinit var apiService: ApiService
    private var mockWebServer: MockWebServer = MockWebServer()

    @get:Rule
    val mainDispatcherRule = MainDispatcherRule()

    @Before
    fun setUp() {
        mockWebServer.start()
        val retrofit = Retrofit.Builder()
            .baseUrl(mockWebServer.url("/"))
            .client(OkHttpClient.Builder().build())
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        apiService = retrofit.create(ApiService::class.java)
    }

    @After
    fun tearDown() {
        mockWebServer.shutdown()
    }

    @Test
    fun `test register`() = runTest {
        val mockedResponse = MockResponse()
        mockedResponse.setResponseCode(HttpURLConnection.HTTP_OK)
        mockedResponse.setBody(
            """
            {
                "code": "200",
                "message": "Success",
                "data": {
                
                }
            }
            """.trimIndent()
        )

        mockWebServer.enqueue(mockedResponse)

        val actualResponse =
            apiService.register(UserRequest("test@gmail.com", "12345678", "token"))

        assertEquals(200, actualResponse.body()?.code)
    }

    @Test
    fun `test register with error`() = runTest {
        val mockedResponse = MockResponse()
        mockedResponse.setResponseCode(HttpURLConnection.HTTP_BAD_REQUEST)
        mockedResponse.setBody(
            """
            {
                "code": 400,
                "message": "Email is already taken",
                "data": {
                }
            }
            """.trimIndent()
        )

        mockWebServer.enqueue(mockedResponse)

        val actualResponse =
            apiService.register(UserRequest("test@gmail.com", "12345678", "token"))

        assertEquals(400, actualResponse.code())
    }
}

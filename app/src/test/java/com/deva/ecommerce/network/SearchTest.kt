package com.deva.ecommerce.network

import com.deva.ecommerce.model.remote.ApiService
import com.deva.ecommerce.util.MainDispatcherRule
import junit.framework.TestCase.assertEquals
import okhttp3.OkHttpClient
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.net.HttpURLConnection

class SearchTest {
    private lateinit var apiService: ApiService
    private val mockWebServer = MockWebServer()

    @get:Rule
    val mainDispatcherRule = MainDispatcherRule()

    @Before
    fun setUp() {
        mockWebServer.start()
        val retrofit = Retrofit.Builder()
            .baseUrl(mockWebServer.url("/"))
            .client(OkHttpClient.Builder().build())
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        apiService = retrofit.create(ApiService::class.java)
    }

    @After
    fun tearDown() {
        mockWebServer.shutdown()
    }

    @Test
    fun `test search`() {
        val mockedResponse = MockResponse()
        mockedResponse.setResponseCode(HttpURLConnection.HTTP_OK)
        mockedResponse.setBody(
            """
            {
                "code": "200",
                "message": "Success",
                "data": []
            }
            """.trimIndent()
        )

        mockWebServer.enqueue(mockedResponse)

        val actualResponse = apiService.search("test").execute()
        assertEquals(actualResponse.code(), HttpURLConnection.HTTP_OK)
    }
}

package com.deva.ecommerce.network

import com.deva.ecommerce.model.remote.ApiService
import com.deva.ecommerce.model.remote.request.FulfillmentRequest
import com.deva.ecommerce.model.remote.response.FulfillmentResponse
import com.deva.ecommerce.util.MainDispatcherRule
import com.google.gson.Gson
import junit.framework.TestCase.assertEquals
import kotlinx.coroutines.test.runTest
import okhttp3.OkHttpClient
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.net.HttpURLConnection

class FulfillmentTest {
    private lateinit var apiService: ApiService

    private var mockWebServer: MockWebServer = MockWebServer()

    @get:Rule
    val mainDispatcherRule = MainDispatcherRule()

    val json = """
        {
            "code": 200,
            "message": "OK",
            "data": {
                "invoiceId": "ba47402c-d263-49d3-a1f8-759ae59fa4a1",
                "status": true,
                "date": "09 Jun 2023",
                "time": "08:53",
                "payment": "Bank BCA",
                "total": 48998000
            }
        }
    """.trimIndent()

    @Before
    fun setUp() {
        mockWebServer.start()
        val retrofit = Retrofit.Builder()
            .baseUrl(mockWebServer.url("/"))
            .client(OkHttpClient.Builder().build())
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        apiService = retrofit.create(ApiService::class.java)
    }

    @After
    fun tearDown() {
        mockWebServer.shutdown()
    }

    @Test
    fun `test checkout`() = runTest {
        val mockedResponse = MockResponse()
        mockedResponse.setResponseCode(HttpURLConnection.HTTP_OK)
        mockedResponse.setBody(json)

        val expectedResponse = Gson().fromJson(
            json,
            FulfillmentResponse::class.java
        )

        mockWebServer.enqueue(mockedResponse)

        val actualResponse =
            apiService.getFulfillment(FulfillmentRequest("Bank BCA", null)).execute()

        assertEquals(expectedResponse.code, actualResponse.code())
    }

    @Test
    fun `test checkout with error`() = runTest {
        val mockedResponse = MockResponse()
        mockedResponse.setResponseCode(HttpURLConnection.HTTP_BAD_REQUEST)
        mockedResponse.setBody(json)

        mockWebServer.enqueue(mockedResponse)

        val expectedResponse = Gson().fromJson(
            json,
            FulfillmentResponse::class.java
        )

        expectedResponse.code = HttpURLConnection.HTTP_BAD_REQUEST

        val actualResponse =
            apiService.getFulfillment(FulfillmentRequest("Bank BCA", null)).execute()

        assertEquals(expectedResponse.code, actualResponse.code())
    }
}

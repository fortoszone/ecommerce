package com.deva.ecommerce.network

import com.deva.ecommerce.model.remote.ApiService
import com.deva.ecommerce.model.remote.request.UserRequest
import com.deva.ecommerce.model.remote.response.FulfillmentResponse
import com.deva.ecommerce.util.MainDispatcherRule
import com.google.gson.Gson
import junit.framework.TestCase.assertEquals
import kotlinx.coroutines.test.runTest
import okhttp3.OkHttpClient
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.net.HttpURLConnection

class LoginTest {
    private lateinit var apiService: ApiService

    private var mockWebServer: MockWebServer = MockWebServer()

    @get:Rule
    val mainDispatcherRule = MainDispatcherRule()

    val json = """
        {
                "code": 200,
                "message": "OK",
                "data": {
                    "accessToken": "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJlY29tbWVyY2UtYXVkaWVuY2UiLCJpc3MiOiJodHRwOi8vMTkyLjE2OC4yMzAuMTI5OjgwODAvIiwidXNlcklkIjoiMzczNTNkMzAtMWIzZC00ZGJlLThmODQtYWZjMjdjNGU5MWJhIiwidHlwZVRva2VuIjoiYWNjZXNzVG9rZW4iLCJleHAiOjE2ODUzNDE1MjB9.ldL_6Qoo-MfMmwHrhxXUv670Uz6j0CCF9t9I8uOmW_LuAUTzCWhjMcQelP8MjfnVDqKSZj2LaqHv3TY08AB7TQ",
                    "refreshToken": "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJlY29tbWVyY2UtYXVkaWVuY2UiLCJpc3MiOiJodHRwOi8vMTkyLjE2OC4yMzAuMTI5OjgwODAvIiwidXNlcklkIjoiMzczNTNkMzAtMWIzZC00ZGJlLThmODQtYWZjMjdjNGU5MWJhIiwidHlwZVRva2VuIjoiYWNjZXNzVG9rZW4iLCJleHAiOjE2ODUzNDQ1MjB9.HeeNuQww-w2tb3pffNC43BCmMCcE3rOj-yL7-pTGOEcIcoFCv2n9IEWS0gqxNnDaNf3sXBm7JHCxFexB5FGRgQ",
                "expiresAt": 600
            }
        }
    """.trimIndent()

    @Before
    fun setUp() {
        mockWebServer.start()
        val retrofit = Retrofit.Builder()
            .baseUrl(mockWebServer.url("/"))
            .client(OkHttpClient.Builder().build())
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        apiService = retrofit.create(ApiService::class.java)
    }

    @After
    fun tearDown() {
        mockWebServer.shutdown()
    }

    @Test
    fun `test login`() = runTest {
        val mockedResponse = MockResponse()
        mockedResponse.setResponseCode(HttpURLConnection.HTTP_OK)
        mockedResponse.setBody(json)

        mockWebServer.enqueue(mockedResponse)

        val expectedResponse = Gson().fromJson(
            json,
            FulfillmentResponse::class.java
        )

        val actualResponse =
            apiService.login(UserRequest("test@gmail.com", "12345678", "token"))

        assertEquals(expectedResponse.code, actualResponse.code())
    }

    @Test
    fun `test login with error`() = runTest {
        val mockedResponse = MockResponse()
        mockedResponse.setResponseCode(HttpURLConnection.HTTP_BAD_REQUEST)
        mockedResponse.setBody(json)
        mockWebServer.enqueue(mockedResponse)

        val expectedResponse = Gson().fromJson(
            json,
            FulfillmentResponse::class.java
        )

        expectedResponse.code = HttpURLConnection.HTTP_BAD_REQUEST

        val actualResponse =
            apiService.login(UserRequest("test@gmail.com", "12345678", "token"))

        assertEquals(expectedResponse.code, actualResponse.code())
    }
}

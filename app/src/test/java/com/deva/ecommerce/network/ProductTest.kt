package com.deva.ecommerce.network

import com.deva.ecommerce.model.remote.ApiService
import com.deva.ecommerce.model.remote.response.FulfillmentResponse
import com.deva.ecommerce.model.remote.response.ProductReviewResponse
import com.deva.ecommerce.ui.main.store.SortCategory
import com.deva.ecommerce.util.MainDispatcherRule
import com.google.gson.Gson
import junit.framework.TestCase.assertEquals
import kotlinx.coroutines.test.runTest
import okhttp3.OkHttpClient
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.net.HttpURLConnection

class ProductTest {
    private lateinit var apiService: ApiService

    private var mockWebServer: MockWebServer = MockWebServer()

    @get:Rule
    val mainDispatcherRule = MainDispatcherRule()

    @Before
    fun setUp() {
        mockWebServer.start()
        val retrofit = Retrofit.Builder()
            .baseUrl(mockWebServer.url("/"))
            .client(OkHttpClient.Builder().build())
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        apiService = retrofit.create(ApiService::class.java)
    }

    private val json = """
        {
            "code": 200,
            "message": "OK",
            "data": {
                "itemsPerPage": 10,
                "currentItemCount": 10,
                "pageIndex": 1,
                "totalPages": 3,
                "items": []
            }
        }
    """.trimIndent()

    @After
    fun tearDown() {
        mockWebServer.shutdown()
    }

    @Test
    fun `test get product`() = runTest {
        val mockedResponse = MockResponse()
        mockedResponse.setResponseCode(HttpURLConnection.HTTP_OK)
        mockedResponse.setBody(json)

        val expectedResponse = Gson().fromJson(
            json,
            FulfillmentResponse::class.java
        )

        mockWebServer.enqueue(mockedResponse)
        val actualResponse = apiService.products(
            sort = SortCategory.HighestPrice,
            search = null,
            brand = null,
            highest = null,
            lowest = null,
            page = 1,
            limit = 10
        )
        assertEquals(actualResponse.code(), expectedResponse.code)
    }

    @Test
    fun `test get product detail`() = runTest {
        val mockedResponse = MockResponse()
        mockedResponse.setResponseCode(HttpURLConnection.HTTP_OK)
        mockedResponse.setBody(json)

        val expectedResponse = Gson().fromJson(
            json,
            FulfillmentResponse::class.java
        )

        mockWebServer.enqueue(mockedResponse)
        val actualResponse = apiService.getProductDetail("17b4714d-527a-4be2-84e2-e4c37c2b3292")
        assertEquals(actualResponse.code(), expectedResponse.code)
    }

    @Test
    fun `test get product review`() = runTest {
        val reviewJson = """
            {
                "code": 200,
                "message": "OK",
                "data": [
                    {
                        "userName": "John",
                        "userImage": "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTQM4VpzpVw8mR2j9_gDajEthwY3KCOWJ1tOhcv47-H9o1a-s9GRPxdb_6G9YZdGfv0HIg&usqp=CAU",
                        "userRating": 4,
                        "userReview": "Lorem Ipsum is simply dummy text of the printing and typesetting industry."
                    },
                    {
                        "userName": "Doe",
                        "userImage": "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTR3Z6PN8QNVhH0e7rEINu_XJS0qHIFpDT3nwF5WSkcYmr3znhY7LOTkc8puJ68Bts-TMc&usqp=CAU",
                        "userRating": 5,
                        "userReview": "Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book."
                    }
                ]
            }

        """.trimIndent()

        val mockedResponse = MockResponse()
        mockedResponse.setResponseCode(HttpURLConnection.HTTP_OK)
        mockedResponse.setBody(reviewJson)

        val expectedResponse = Gson().fromJson(
            reviewJson,
            ProductReviewResponse::class.java
        )

        mockWebServer.enqueue(mockedResponse)
        val actualResponse =
            apiService.getProductReview("17b4714d-527a-4be2-84e2-e4c37c2b3292")
        assertEquals(actualResponse.code(), expectedResponse.code)
    }
}
